import matplotlib.pyplot as plt


y = [-1.4287691, -1.4393806, -1.4507284, -1.4853802, -1.5275908, -1.5497236, -1.5492592, -1.5459194]
x = [1, 2, 3, 4, 5, 6, 7, 8]

fontsize = 20
labelpad = 10
fig = plt.figure()
ax = fig.add_subplot()#121, projection='3d')
ax.set_xlabel('$cellsize$ in m/pixel', fontsize=fontsize, labelpad=labelpad)
ax.set_ylabel('$r_y(t_{end})$ in m', fontsize=fontsize, labelpad=labelpad)
#ax.set_zlabel("Final $L^2$ norm (to be read dimensionless)", fontsize=fontsize)#"$\mu_r$")

ax.scatter(x, y)



ax.spines.right.set_visible(False)
ax.spines.top.set_visible(False)
#ax.set_ylim(bottom=0)
#ax.set_xlim(left=0)
#ax.set_yticks(range(0, 450, 50))

#ax.set_xscale('log')

plt.xticks(fontsize=15)
plt.yticks(fontsize=15)


ax.grid(alpha=0.5)


#hf.set_axes_equal(ax)
fig.tight_layout()
plt.show()
