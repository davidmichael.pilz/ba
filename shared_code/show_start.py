"""
# TODO:

"""


#!/usr/bin/env python

#
# MAKESHIFT HACKED TOGETHER START POSITION PLOTTER!
#

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from siconos.mechanics.collision.convexhull import ConvexHull
from siconos.mechanics.collision.tools import Contactor
from siconos.mechanics.collision.bullet import SiconosBulletOptions
from siconos.io.mechanics_run import MechanicsHdf5Runner
import siconos.numerics as sn
import siconos.kernel as sk
import numpy as np
import math
#import function_gdal_shapefile_to_array as fungdal

import random

import csv

import h5py

import pyhull.convex_hull

np.set_printoptions(threshold=sys.maxsize)

import helperfunctions as hf

##################################################################################

                 


########################################################################################



cmdOptions = hf.get_CMD_options("hdg:b:s:m:n:", ["help", "debug", "grid=", "blocks=", "soil=", "blocksParams=", "soilParams="], 
        ["help", "Print all debug messages",
        "Path to heightmap (ESRI ASCII (.asc))", 
        "Path to soil grid (ESRI ASCII (.asc))", 
        "Path to blocks starting position grid (ESRI ASCII (.asc))", 
        "Path to soil parameters file (.csv); Expected: (e, mu, mu_r)", 
        "Path to blocks parameters file (.csv); Expected: (number_blocks, height_fall_min, height_fall_max, number_points_hull, v_min, v_max)"])    



# check IO interaction
for j in range(2, len(cmdOptions)):
    if cmdOptions[j] is None:
        print("Not enough file paths given!")
        exit(-1)
    else:
        cmdOptions[j] = cmdOptions[j].strip()


        # Load heightmap
        
mX, mY, mZ, misc  = hf.read_ESRI_ASCII(cmdOptions[2])

# initial_resolution
init_res = float(misc[0]["cellsize"])


        # Block departure zones

dep = hf.read_ESRI_ASCII(cmdOptions[3])[2]

# ONE type of rock expected (gets mixed up anyway with rnd generation)
blocksParams = hf.read_CSV(cmdOptions[5])
        # Soil zones
        
zones = hf.read_ESRI_ASCII(cmdOptions[4])[2]


        # Soil data
        
soilParams = hf.read_CSV(cmdOptions[6])
#print(soilParams, blocksParams)

import matplotlib.pyplot as plt
fig, ax = hf.plot_start_setting(mX, mY, mZ, (blocksParams, dep), (soilParams, zones))
plt.show()
