"""
# TODO:
    - DOCU
    - multiple (start) positions per ID?
    - ID input as well via .csv! (not reeealy needed, but nicer?)
"""


#!/usr/bin/env python

#
# Simulation of experimental data aquired with LROC / LOLA ; boulder positions manually extracted
#

from siconos.mechanics.collision.convexhull import ConvexHull
from siconos.mechanics.collision.tools import Contactor
from siconos.mechanics.collision.bullet import SiconosBulletOptions
from siconos.io.mechanics_run import MechanicsHdf5Runner
import siconos.numerics as sn
import siconos.kernel as sk
import numpy as np
#np.set_printoptions(threshold=sys.maxsize)

# Set random number generator and seed
## PCG64 provides guarantee that a fixed ssed will always reproduce the same random integer stream (current numpy default generator)
## https://numpy.org/doc/stable/reference/random/bit_generators/pcg64.html

seed = 11235813213455
rng = np.random.Generator(np.random.PCG64(seed))


import math
#import function_gdal_shapefile_to_array as fungdal

import random

import csv

import h5py

import pyhull.convex_hull


import sys, os
# needed for easy access to helperfunctions in different directory (../shared_code)
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))
import helperfunctions as hf

##################################################################################

        
def soil_setup(io_hdf5, soil_heightmap, soil_parameters, resolution):
    """
    #TODO:
    """
    
    ####
    # Modify siconos simulation adding one soil type with corresponding heightmap (Default collision_group1=100, collision_group2=1)

    # -IN- #    
    # io_hdf5; <siconos.MechanicsHdf5Runner>:   Simulation to modify
    # soil_heightmap; <np.array(<float>)>:      Corresponding heightmap to soil (m)
    # soil_parameters; <dict(<str>:<float>)>:   Dict containing values for 'e', 'mu', 'mu_r' in [0, 1]
    # resolution; <float>:                      Resolution of soil_heightmap grid to infer x,y position of br_corner (m/pix)
    
    # -OUT- #
    # 0 on success
    ####

    
            # Add map and soil params to simulation
    ## X [Y] extension only (nX [nY] -1)*resolution as each grid point corresponds to one terrain junction ! (i.e. tl_corner == 0, 0 ; tr_corner == 0, -1 !)
    io_hdf5.add_height_map('Height_map', soil_heightmap, (resolution*(soil_heightmap.shape[0]-1), resolution*(soil_heightmap.shape[1]-1)), outsideMargin=0.01, insideMargin=0.01)
    io_hdf5.add_object('Ground_object', [Contactor('Height_map', collision_group=1)],translation=[0, 0, 0])        
    
    # Add contact law
    io_hdf5.add_Newton_impact_rolling_friction_nsl('Contact_law', e=float(soil_parameters["e"]), mu=float(soil_parameters["mu"]), mu_r=float(soil_parameters["mu_r"]), collision_group1=100, collision_group2=1)
                
                
    return 0
    
    
    
def create_rock_properties(L1, L2, L3, number_of_points, density=2475, random_number_generator=rng):

    """
    #TODO:
        - implement only one dimension random as well? !!
        
        - L2, L3 better None than 0 ?
    """

    ####
    # Create randomly shaped cuboidal rocks with given principal axis and return vertices of convex shape along with computed physical properties from density (assuming homogeneous mass distribution/density)
    # Set L2 AND L3 to 0 to generate randomly sized shapes inside the diameter bound given by L1

    # -IN- #    
    # L1, L2, L3; <float>:      Principal axis of convex shape along x,y,z direction (normally >0, m) , The created shape fits within a cuboid of lengths L1, L2, L3 touching each face
    # number_of_points; <int>:  Number of points to build convex hull around via pyhull
    
    # density; <float>:         Density of rock material (kg m-3); Default: 2475 kg m-3
    ##  The density and porosity of lunar rocks, Kiefer, W., 2012: https://doi.org/10.1029/2012GL051319 
    ### Impact Breccias: bulk density is the important parameter, and the combined range of the results summarized here is 2350–2600 kg m−3
    
    # random_number_generator; <np.random.Generator>:       Numpy generator to get random numbers from ; Default: rng (globally defined at imports section)
    
    
    # -OUT- #
    # vertices; <np.array([<float>, <float>, <float>])>:    Vertices of created shape from convex hull around random points with 0, 0, 0 at center of mass
    # inertia; <np.array(<float>)>:                         Moments of inertia corresponding to the shape created
    # mass, <float>:                                        Mass of the shape created assuming homogeneous mass distribution / density
    ####
    
    
        # Create archetype random convex hull -> modify size later
    # Pick random 3D points uniformly in [0, 1)
    points = random_number_generator.random((number_of_points, 3))
    
    # Create convex hull using some points as vertices for simplizes
    hull = pyhull.convex_hull.ConvexHull(points)
        
    # Get all the points used as vertices (their index got returned) for the convex shape
    vertices = points[np.unique(np.array(hull.vertices).flatten())]
    
    
        # Modify archetype shape
        
    # If not all axes given -> draw missing values randomly
    if L3 == 0:
        # Sometimes, we only know about the approx. diameter of the rock (given by L1) -> generate randomly sized shapes inside this diameter bound
        if L2 == 0:
            diameter = L1
            ## TODO other distribution ideas from papers?? (e.g. INFERRED LUNAR BOULDER DISTRIBUTIONS AT DECIMETER SCALES) ; further narrowing down of axis ration with crater age and location?
            ## We assume a normal distribution centered around [diameter, 1.5*diameter] with a very small standard deviation to avoid overly large and overly small shapes
            ## 1.5, because we know more about the axial ration (L1/L2) ! (DOI: 10.1016/j.pss.2017.07.014) -> 'Most (~90%) of the boulders have the axial ratio in the range of 1-2' with mean of 1.5
            random_shape = rng.multivariate_normal([1, 1.5], [[0.01, 0], [0, 0.01]], 10000)
            L1 = diameter*random_shape[0]
            L2 = diameter*random_shape[1]
          
        # Generally believed to be the shortest axis (DOI: 10.1016/j.pss.2017.07.014) --> 'It is obvious that in most cases a rock ejected to the lunar surface should lie with its short axis upward. So, when we observe rocks from above, as in the case of the present study, most probably we see the long a and medium b dimensions of it (e.g., Demidov and Basilevsky, 2014)'
        ## We set a lower bound (0.5) to get reasonable results from random [draws from [0, 1)]
        L3 = min(L1, L2) * (0.5 + 0.5*rng.random())
        
        
        
        
    # Get at least one vertex' coordinate in each dimension to 0 , We later want to span min(dim_j) = 0 -> L_j !
    ## We only work with positive values here
    vertices[:,0] = vertices[:,0] - np.min(vertices[:,0])
    vertices[:,1] = vertices[:,1] - np.min(vertices[:,1])
    vertices[:,2] = vertices[:,2] - np.min(vertices[:,2])
    
    # And 'normalize' them before stretching -> afterwards max(dim_j) is L_j
    ## Does not preserve the shape
    vertices[:,0] = L1 * vertices[:,0]/np.max(vertices[:,0]) 
    vertices[:,1] = L2 * vertices[:,1]/np.max(vertices[:,1]) 
    vertices[:,2] = L3 * vertices[:,2]/np.max(vertices[:,2]) 
    
    
        # Get physical properties of this new convex hull
    ## siconos' build-in functions!
    convex_hull = ConvexHull(vertices)
    center_mass = convex_hull.centroid()
    
    # 'Normalized' inertia, and volume
    inertia, volume = convex_hull.inertia(center_mass)
    mass = volume*density
    
    # Get 'real' moments of inertia
    inertia = inertia*mass
    
    # Move vertices such that center of mass at 0, 0, 0
    ## siconos spawns objects at their local 0, 0, 0 (sure?, or simply center of mass?)
    vertices = vertices - center_mass
    
    
    return vertices, inertia, mass
    
    
    
    
def blocks_setup(io_hdf5, blocks_parameters, repetitions, mZ, start_positions, resolution, random_number_generator=rng):

    """
    #TODO:
        - DOCU! 
        - startin_uncertainty and z position (not at grid points anymore !!!) ? - difficult to get surface_normals ? -- clipping (orientation)!
        - check default values siconos add stuff (especially inertia!)
    """
    
    ####
    # 
    ## ONE parameter set <-> ONE starting index
    ## shape random, but in bounds (0.5 +) AND orientation always such that longest dimension points upwards (+z) [otherwise complicated intersection checking necessary..]

    # -IN- #    
    # L1, L2, L3; <float>:      Principal axis of convex shape along x,y,z direction (m) , The created shape fits within a cuboid of lengths L1, L2, L3
    # number_of_points; <int>:  Number of points to build convex hull around via pyhull
    
    # random_number_generator; <numpy.random.Generator>:    Numpy generator to get random numbers from ; Default: rng (globally defined at imports section)
    
    
    # -OUT- #
    # vertices; <np.array([<float>, <float>, <float>])>:    Vertices of created shape from convex hull around random points with 0, 0, 0 at center of mass
    # inertia; <np.array(<float>)>:                         Moments of inertia corresponding to the shape created
    # mass, <float>:                                        Mass of the shape created assuming homogeneous mass distribution / density
    ####


    for site_ID in start_positions.keys():
        
        # Get corresponding parameter_set
        parameter_set = [parameter_set for parameter_set in blocks_parameters if int(parameter_set['ID']) == site_ID][0]
            
            # Retrieve defining properties of this rock type
        L1, L2, L3 = float(parameter_set['L1']), float(parameter_set['L2']), float(parameter_set['L3']) #  x, y, z ; in m
        number_of_points = int(parameter_set['points'])
        bulk_density = float(parameter_set['bulk_density'])                                             # in kg m-3
        starting_uncertainty = float(parameter_set['starting_uncertainty'])                             # in m
        v0 = float(parameter_set['v0'])                                                                 # in m/s


        # X [Y] extension only (nX [nY] -1)*resolution as each grid point corresponds to one terrain junction ! (i.e. tl_corner == 0, 0 ; tr_corner == 0, -1 !)   
        tl_row_coordinate = -resolution*0.5*(mZ.shape[0]-1)
        tl_column_coordinate = -resolution*0.5*(mZ.shape[1]-1)
        
        # Simultaneous evaluation of multiple runs with the same underlying parameters -> check impact of randomness
        for j in range(repetitions):
        
                # Create shape and compute physical properties
            vertices, inertia, mass = create_rock_properties(L1, L2, L3, number_of_points, density=bulk_density, random_number_generator=rng)
                        
                        
                # Compute starting position
                
            # Extracted XY starting position estimation
            start_position = start_positions[site_ID]
            
            # Uncertainty and all errors which factor in the extraction
            ## We assume a normal distribution centered around 0 with a standard deviation of starting_uncertainty/np.sqrt(5) (such that ~67.25% inside error/uncertainty margin/circle [eq. 20: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4358977/]) (our error; no other dependency between directions)
            starting_deviation = rng.multivariate_normal([0, 0], [[starting_uncertainty/np.sqrt(5), 0], [0, starting_uncertainty/np.sqrt(5)]])
            
            Xpos = start_position[0] + starting_deviation[0]
            Ypos = start_position[1] + starting_deviation[1]
                                        
            # Interpolate z    
            Zpos = hf.get_interpolated_height([Xpos, Ypos], mZ, tl_row_coordinate, tl_column_coordinate, 1/resolution)
        
            
            posInit = np.array([Xpos,Ypos,Zpos])
            
            
                # Avoid clipping through surface (rocks have space filling shapes !)
               
                # (1) Orientation
                
                # Rotate shape, such that longest shape_dimension in z direction (BEST EFFORT avoiding clipping through surface)
            # Find longest dimension (we assume one of xyz is longest as we constructed the shapes like this)
            dimension_index = np.argmax(np.amax(abs(vertices), axis=0))
            
            # Corresponding axis vector
            axis = np.zeros(3)
            axis[dimension_index] = 1
            
            
            # We want the default orientation (0, 0, 1) to go to the axis with the longest dimension
            default_orientation = np.array([0, 0, 1])
            rotation_1 = hf.calculate_rotation_quaternion(default_orientation, axis)
                       
                # Rotate to surface_normal
                       
            # Get the unit normal to the surface at the starting point -> that's the desired orientation as well
            surface_normal = hf.coordinates_calculate_normal(mZ, posInit, tl_row_coordinate, tl_column_coordinate, 1/resolution)
                       
            # We want the rock to start flush on the surface; Default orientation is [0, 0, 1]
            rotation_2 = hf.calculate_rotation_quaternion(default_orientation, surface_normal) 
               
            initial_orientation = hf.composite_rotation_quaternion(rotation_1, rotation_2)
               
               
                # (2) Offset
               
            # Get max length beneath center of mass (siconos spawns objects at their center of mass)
            ## vertices were adjusted such that center of mass at their 0, 0, 0 -> we need the biggest (absolute) value of the dimension_index component
            ## Might be too much as com could not be in the middle of dimension (? sure) and smaller length is facing down?
            length_below_com = np.max(abs(vertices[:, dimension_index]))
            
            # Offset the position (shape's center / center of mass) to avoid clipping through the surface
            ## Well this is best effort; Better would be to check intersections between shape and triangles directly and change offset/orientation accordingly!
            posInit = posInit + surface_normal*length_below_com
            posInit = posInit.tolist()
            
            
            
     
                # Get starting velocity (only translational (sliding), no torque applied to body)
                
            # Uncertainty of starting velocity (the rock lay at rest on the surface - until it decided to rumble down ; therefor some_ starting velocity != 0 had to occur, maybe due to some impluse transmission [we assume no torque - rotational velocity though!])
            ## We assume a normal distribution centered around 0 with a standard deviation of v0 (our 'error' / estimation; no other dependency between directions)
            ## ~67.5% inside sigma = ||v0_deviation|| = v0 ; ARBITRARY (eq 22: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4358977/)
            v0_deviation = rng.multivariate_normal([0, 0, 0], [[v0/np.sqrt(12), 0, 0], [0, v0/np.sqrt(12), 0], [0, 0, v0/np.sqrt(12)]])
            
            # Choose v random inside bound
            v_x = v0_deviation[0]
            v_y = v0_deviation[1]
            v_z = v0_deviation[2]
            
            ## TODO necessary?
            # Rocks should not lift off on their own
            if v_z < 0:
                v_z *= -1 
            
            # L2 norm (length) of resulting velocity vector
            length = np.sqrt(v_x*v_x + v_y*v_y + v_z*v_z) 
            
            
            ## CHECK DEFAULT VALUES!!
            shape_name = f'shape_parameter_{site_ID}_rock_{j}'
            io_hdf5.add_convex_shape(shape_name, vertices, outsideMargin=0.0)
            io_hdf5.add_object(f'parameter_{site_ID}_rock_{j}', [Contactor(shape_name, collision_group=100)], translation=posInit, velocity=[v_x, v_y, v_z, 0, 0, 0], orientation=initial_orientation, mass=mass)
            
            
            #break
        # TODO test
        #break
    return 0    
            
     

######################################################################
############################ MAIN ####################################
######################################################################



if __name__ == "__main__":

    cmdOptions = hf.get_CMD_options("hdg:b:s:m:n:", ["help", "debug", "grid=", "blocks=", "soil=", "blocksParams=", "soilParams="], 
            ["help", "Print all debug messages",
            "Path to heightmap (ESRI ASCII (.asc))", 
            "Path to blocks starting coordinates (.GEOJSON)", 
            "Path to soil grid (ESRI ASCII (.asc))", 
            "Path to soil parameters file (.csv); Expected: (e, mu, mu_r)", 
            "Path to blocks parameters file (.csv); Expected: (L1, L2, L3, points, bulk_density, starting_uncertainty, v0_max)"])



    # check IO interaction
    for j in range(2, len(cmdOptions)):
        if cmdOptions[j] is None:
            print("Not enough file paths given!")
            exit(-1)
        else:
            cmdOptions[j] = cmdOptions[j].strip()


            # Load heightmap
            
    _, _, mZ, misc  = hf.read_ESRI_ASCII(cmdOptions[2])

    # initial_resolution
    resolution = float(misc[0]["cellsize"])


            # Rock departure positions
    
    # Alternatively access the transformed coordinates directly
    extracted_paths = hf.load_extracted_paths(cmdOptions[3])
    
    start_positions = {}
    
    site_IDs = []
    
    # Access xy coordinates 
    for s, site in enumerate(extracted_paths.keys()): 
        if s < 10:
            site_ID = int(site.rsplit('_')[-1])
            site_IDs.append(site_ID)
            start_positions[site_ID] = extracted_paths[site][-1]


            # Rock parameters
                    
    blocksParams = hf.read_CSV(cmdOptions[5])


            # Soil zones
            
    zones = hf.read_ESRI_ASCII(cmdOptions[4])[2]

            # Soil parameters
            
    soilParams = hf.read_CSV(cmdOptions[6])[0]
           
           
    """
    # TODO: automatic adjustment as option!; hard break! --> new commit? where already implemented?? 
    """
    T = 50
    ## TODO TOADJUST !! ##
    ## ref.: Predictive Capabilities of 2D and 3D Block Propagation Models Integrating Block Shape Assessed from Field Experiments: 'n = 50 were considered sufficient to provide representative numerical results' for 50 block releases from the same site!!
    repetitions = 20

    with MechanicsHdf5Runner() as io:


             # Setup soil
        soil_setup(io, mZ, soilParams, resolution=resolution)
        
            # Setup rocks
        blocks_setup(io, blocksParams, repetitions, mZ, start_positions, resolution, random_number_generator=rng)
                          
                          
        # By default earth gravity is applied and the units are those
        # of the International System of Units.

        # We run the simulation with relatively low precision because we
        # are interested mostly just in the location of contacts with the
        # height field, but we are not evaluating the performance of
        # individual contacts here.


        ## TODO: Magic Settings

            # Setup siconos solver
            
        # Create solver options
        ## What do they mean?
        options = sk.solver_options_create(sn.SICONOS_ROLLING_FRICTION_3D_NSGS)
        options.iparam[sn.SICONOS_IPARAM_MAX_ITER] = 1000
        # If an object has not moved after 20 timesteps it will be disregarded from further propagation
        options.iparam[sn.SICONOS_FRICTION_3D_NSGS_FREEZING_CONTACT] = 20
        options.dparam[sn.SICONOS_DPARAM_TOL] = 1e-4


        # Start the simulation
        io.run(
            with_timer=False,               # ?
            multipoints_iterations=True,    # ?
            gravity_scale=9.81/1.62,        # Divisor to 9.81 , i.e. if e.g. g = 1.62 is desired: gravity_scale=9.81/1.62 ; https://nssdc.gsfc.nasa.gov/planetary/factsheet/moonfact.html
            t0=0,                           # Start time
            T=T,                            # End time
            h=1e-3,                         # Size of time step
            theta=0.50001,                  # ?
            Newton_max_iter=1,              # ?
            set_external_forces=None,       # ?
            solver_options=options,         # Specify the solver ?
            violation_verbose=False,        # ?
            numerics_verbose=False,         # ?
            output_frequency=None,            # What datapoints (time points) to save to .hdf5 file
            output_contact_index_set=0      # ?
        )
        


