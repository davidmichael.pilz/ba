########
# TODO:
#   - output points of distribution_areas and distribution_center as well?
#
#   - maybe extra plot function for convex_distribution including centroid and points???
#   - maybe extra function for json.dump ?

#   - extracted <-> observation naming better!
#
##########


##
##  Analyse results of one .hdf5 with simulations for all detected boulders run simultaneously
##



import numpy as np

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import sys, os

# only needed if helperfunctions one directory above this file
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))
import helperfunctions as hf

#np.set_printoptions(threshold=sys.maxsize)

import json


def input_file(hdf5_file, sites=1):
    """
    # TODO:
    """
    
    ####
    # Retrieve trajectories and velocities from given .hdf5 file
    
    # -IN- #    
    # hdf5_file; <PATH>:    List of PATHs to .hdf5 files (containing trajectories, velocities)
    # sites, <int>:         Number of different rockfalls simulated; Default: 1
    
    
    # -OUT- #    
    # blockTrajectories; <list(<np.array(<float>)>)>:       List of 2D arrays (sorted by ID) containing the trajectories (time, ID, x, y, z) of each detected rockfall as saved in "data"/"dynamic"
    # blockVelocities; <list(<np.array(<float>)>)>:         List of 2D arrays (sorted by ID) containing the velocities (time, ID, v_x, v_y, v_z, omega_x, omega_y, omega_z) of each detected rockfall as saved in "data"/"velocities"
    ####
    
    
        # Block trajectories (x, y, z)

    blockTrajectories = []
    blockVelocities = []

    try:
        with h5py.File(hdf5_file, "r") as hdf5File:
            
            # t, ID, x, y, z, orientation (quaternion)
            ## x ^, y ->
            trajectories = hdf5File["data"]["dynamic"][:, :5]
                                        
            # t, ID, vx, vy, vz, omega
            velocities = hdf5File["data"]["velocities"][:, :]
            
            
            # Get number of simulated rocks
            ## Look for number of unique IDs
            total_rocks = len(np.unique(trajectories[:, 1]))
            
            rocks_per_site = int(total_rocks/sites)
            
            # Sort arrays by ID
            trajectories = hf.sort_array(trajectories)
            velocities = hf.sort_array(velocities)
            
            
            # Save trajectories/velocites for each site separately
            for j in range(sites):
                blockTrajectories.append(np.array(trajectories[j*rocks_per_site:(j+1)*rocks_per_site, :, :]))
                blockVelocities.append(np.array(velocities[j*rocks_per_site:(j+1)*rocks_per_site, :, :]))
                
    except:
        print("  - An error occured while trying to access the input block trajectories' filepath:")
        print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")  
        
      
    return blockTrajectories, blockVelocities



def analyse_rockfalls(trajectory_data, velocity_data):
    """
    # TODO:
    """
    
    ####
    # Retrieve detailed information about the generated trajectories and velocities for each rockfall
    ## We assume t to be the same for each rockfall ! 
    
    # -IN- #    
    # blockTrajectories; <list(<np.array(<float>)>)>:       List of 2D arrays containing the trajectories (time, ID, x, y, z) of each rockfall site as saved in "data"/"dynamic"
    # blockVelocities; <list(<np.array(<float>)>)>:         List of 2D arrays containing the velocities (time, ID, v_x, v_y, v_z), omega_x, omega_y, omega_z of each rockfall site as saved in "data"/"velocities"
    
    
    # -OUT- #    
    # r_data, v_data, omega_data, traj_analysis_data; <list(<list(..)>)>)>: List of data for each rockfall site as returned by hf.trajectory_analysis() ordered for each rockfall
    
    ## just _ONE_ 'run' (first run)!!
    # t: (t; <np.array(<float>)>:                                           Time corresponding to each datapoint (same for r, velocity, .. !)
    # tmax; <int> ):                                                        Max t in dataset converted to array index; Default conversion: 1e-3 (look in siconos simulation for io.run-options!!! might not always be 1e-3 !)
    ####
    
        
    r_data = []
    v_data = []
    omega_data = []
    traj_analysis_data = []
        
        # Get details of trajectories
        
    # Analyse trajectories for each site individually
    for j, trajectory_site in enumerate(trajectory_data):
        r_data.append([])
        v_data.append([])
        omega_data.append([])
        traj_analysis_data.append([])
        for traj, vel in zip(trajectory_site, velocity_data[j]):
            
            ## t assumed to be the same for every run
            r, v, omega, traj_analysis, t = hf.trajectory_analysis((traj, vel))
            
            r_data[j].append(r)
            v_data[j].append(v)
            omega_data[j].append(omega)
            traj_analysis_data[j].append(traj_analysis)
        
    return r_data, v_data, omega_data, traj_analysis_data, t
    
    
    
## maybe with soil files? --> change input_files()!
def get_data(hdf5_files, sites=1):
    """
    # TODO:
        - sites as param!!
        - multi hdf5 file support!!
    """
    
    ####
    # Extract raw data from files
    ## We only check one rockfall (first rockfall) for t
    
    # -IN- #    
    # hdf5_files; <list(<str)>: Paths to all .hdf5 files
    # sites, <int>:         Number of different rockfalls simulated; Default: 1
    
    
    # -OUT- #  
    # r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs;   as returned by analyse_runs()
    # parameters;           Soil parameters as returned by input_files()
    ####
    
    
    # Extract raw data from files
    trajectories, velocities = input_file(hdf5_files, sites=sites)

    # Analyse results
    r_runs, v_runs, omega_runs, traj_analysis_rockfalls, t_runs = analyse_rockfalls(trajectories, velocities)
    
    return r_runs, v_runs, omega_runs, traj_analysis_rockfalls, t_runs





def evaluation(trajectories_simulated, trajectory_extracted):#, heightmap, tl_row_coordinate, tl_column_coordinate, conversion_factor):
    """
    # TODO:
        - DOCU!!
    
        - errors/uncertainties?! --> sci [USING t-DISTRIBUTION!] / comparison: start_error as a min_error for the extracted end position ! ---> momentarily only needed for Validation of Calibration (here relative scores interesting, errors should be the same for each soil parameter set)
        
        - z component for extracted points ?!
        
        - # bounces --> extract extracted by distance between consecutive points
        - distance between bounces (?)
        
        - maybe mean vector - extracted distance as well? --> well this would assume distribution to be equal (?) --> all vectors weighted equally at least ; not really what we expect from transforming a multidimensional normal distribution 
    """
    
    ####
    # Compare properties of simulated trajectory vs. extracted trajectory for one site
    ## Like a meta analysis: compare all simulations at once
    
    # -IN- #    
    # trajectories_simulated; <list(dict(<str>:np.array(<float>)))>:  All trajectories (r_x, r_y, r_z, r, r0) corresponding to one rockfall site
    # trajectory_extracted; <dict(<str>:np.array(<float>))>:        Extracted path and start/end points' coordiantes (x, y ; _NO_ z!!) ; Same coordinate system as trajectories_simulated !
    
    # heightmap; <np.array(<float>)>:       Grid containing z values, trajectories lie inside the area covered
    # tl_row_coordinate; <int>:             Coordinate in original coordinate system of the top left corner's row to adjust center (0, 0) to top left corner of array [0, 0].
    # tl_column_coordinate; <int>:          Coordinate in original coordinate system of the top left corner's column to adjust center (0, 0) to top left corner of array [0, 0].
    # conversion_factor; <float>:           Conversion between grid (pixel) and coordinate system: COORDINATES * CONVERSION_FACTOR = PIXEL
    
    
    # -OUT- #  
    # distance; <float>:                                                            Distance of extracted end position to 'centroid' of simulated endposition cluster - only in 2D xy-plane !
    # distribution_areas; <list(<dict(<str>:<shapely.Polygon>, <str>:<float>)>)>:   All extracted distribution areas ['hull'] as shapely Polygon objects as well as the corresponding percentile ['percentile']
    # distribution_center; <shapely.Point>:                                 The centroid of the analysed cluster
    # end_simulations; <np.array(<float>, <float>, <float>)>:               Points of analysed cluster
    ####
    
    
    from shapely.geometry import LineString, Point
    
        # Input values
        
    # Simulation values
    
    path_simulations = []
    start_simulations = []
    end_simulations = []
    
    for trajectory in trajectories_simulated:
        ## All vectors
        #path_simulation = LineString(np.array([trajectory[0], trajectory[1], trajectory[2]]))#trajectory['r_x'], trajectory['r_y'], trajectory['r_z']]))
        
        #paths_simulation.append(path_simulation)
                
        start_simulation, end_simulation = Point([trajectory[0][0][0], trajectory[1][0][0], trajectory[2][0][0]]), Point([trajectory[0][0][-1], trajectory[1][0][-1], trajectory[2][0][-1]]) #path_simulation.boundary
        
        # We save xyz coordinates here for easier analysis later
        start_simulations.append([start_simulation.x, start_simulation.y, start_simulation.z])
        end_simulations.append([end_simulation.x, end_simulation.y, end_simulation.z])
        
    
    start_simulations = np.array(start_simulations)
    end_simulations = np.array(end_simulations)
    
    
    # Extracted values
    ## All vectors
    path_extracted = trajectory_extracted
    start_extracted = path_extracted[-1] 
    end_extracted = path_extracted[0] 
    
    end_extracted = Point(*end_extracted)
    
    
        # [A] Analysis (Single experiment idea)
        
        # (1) Mean distance to observation
    
    distances = []
        
    # Get all distances individually
    for point in end_simulations:
        distance = Point(*point[:2]).distance(end_extracted)
        distances.append(distance)
        
    # Calculate mean etc. 
    ## We have one parameter (column) with multiple observations (row)
    individual_distance_statistics = hf.describe_dataset(np.atleast_2d(distances).T)
        
        # (2) bounces
        ## TODO
    
    
    
    
        # [B] Analysis (Distribution transformation idea)
    
    # Analyse distribution of simulated end positions
    distribution_analysis = hf.convex_distribution(end_simulations)
    distribution_areas = distribution_analysis['distribution']
    distribution_center = distribution_analysis['centroid']
    
    
        # (1) Distance extracted end to centroid
        
    # Get z component of distribution_center
    #### would it not be reasonable as well to just look at the xy projection??? if points are close together in that plane, the simulation kinda does it's job - only slight xy deviations account for drastic z changes ? (e.g. cliffs)
    
    #center_z = hf.get_interpolated_height(distribution_center.xy, heightmap, tl_row_coordinate, tl_column_coordinate, conversion_factor)
    
    # Compute distance (xy)
    #distance_centroid_vector = end_extracted - np.array([*distribution_center.xy]) #np.array([*distribution_center.xy, center_z])
    distance_centroid = end_extracted.distance(distribution_center) #np.linalg.norm(distance_centroid_vector)
    
    
        # (2) Percentile
        # (3) Distance to closest point of next area or centroid (_AGAIN_)
        
    end_extracted_percentile_location = None
    
    # We start looking for intersections of the extracted position with our distribution_areas from the outside of the cluster
    for d, distribution_area in enumerate(distribution_areas):
        
        # (2) Percentile
    
        # Case 1: failed before center area
        if not end_extracted.intersects(distribution_area['hull']):
            # Case 1a: first area
            if d == 0:
                end_extracted_percentile_location = [0, 0]
            # Case 1b: at least one check succeeded
            else:
                end_extracted_percentile_location = [distribution_areas[d-1]['percentile'], distribution_area['percentile']]
                
                # (3) Distance to next area

            distance_next_area = end_extracted.distance(distribution_area['hull'])
            break
                
        # Case 2: all checks succeeded, i.e. reached end of distribution_areas
        elif d == len(distribution_areas)-1:
            end_extracted_percentile_location = [distribution_areas[d-1]['percentile'], 1]
            
                # (3) Distance to next area: _HERE_ centroid (_AGAIN_)
                
            distance_next_area = distance_centroid
            break
   
   
    return distribution_areas, distribution_center, end_simulations, distance_centroid, end_extracted_percentile_location, distance_next_area, individual_distance_statistics
    
    

######################################################################
############################ MAIN ####################################
######################################################################



if __name__ == "__main__":

    # TODO Toggle for plotting of convex_distributions
    plot = False


    cmdOptions = hf.get_CMD_options("hdr:f:m:n:", ["help", "debug", 'simulation=', 'features=', 'blocksParams=', "soilParams="], 
            ["help", "Print all debug messages",
            'Path to simulation results to analyse (.hdf5)',
            'Path to extracted features in _siconos_ coordinate system (.GEOJSON); Expected only paths from end to start position', 
            "Path to blocks parameters file (.csv); Expected: (L1, L2, L3, points, bulk_density, starting_uncertainty, v0_max)", 
            "Path to blocks parameters file (.csv); Expected: (L1, L2, L3, points, bulk_density, starting_uncertainty, v0_max)"])

    # check IO interaction
    for j in range(2, len(cmdOptions)):
        if cmdOptions[j] is None:
            print('Not enough file paths given!')
            exit(-1)
        else:
            cmdOptions[j] = cmdOptions[j].strip()




    simulationFile = cmdOptions[2]
    # The file name is the string inbetween the last '/' of the path and the last '.' separating the file type
    simulation_name = simulationFile.rsplit('/')[-1].rsplit('.', maxsplit=1)[0]
    featuresPath = cmdOptions[3]
    soilFile = cmdOptions[4]

    # Load extracted features / paths (especially end positions)
    paths = hf.load_extracted_paths(featuresPath)
    
        # Extract all paths
        ## We assume there are as many different sites simulated as paths extracted in .GEOJSON
    extracted_paths = {}
    
    IDs = []
    
    for label in paths:
        label_ID = int(label.split('_')[-1])
        extracted_paths[label_ID] = paths[label]
        IDs.append(label_ID)
                
    number_of_sites = len(extracted_paths)
    
                    
    # Analyse the simulated trajectories
    ## We don't care at all about v and omega_v !
    r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs = get_data(simulationFile, sites=number_of_sites)
    
    
    trajectory_areas = []
    trajectory_centers = []
    trajectory_centroid_distances = []
    end_extracted_percentile_location = []
    trajectory_distances_next_area = []
    trajectory_individual_distance_statistics = []
    
    
    # (1) Evaluate each site individually
    for s, site_trajectories in enumerate(r_runs):
        
        extracted_path = extracted_paths[IDs[s]]

        areas, center, points, centroid_distance, percentile_location, distance_next_area, individual_distance_statistics = evaluation(site_trajectories, extracted_path)#, heightmap, tl_row_coordinate, tl_column_coordinate, conversion_factor)
        
        trajectory_centroid_distances.append(centroid_distance)
        trajectory_areas.append(areas)
        trajectory_centers.append(center)
        end_extracted_percentile_location.append(percentile_location)
        trajectory_distances_next_area.append(distance_next_area)
        trajectory_individual_distance_statistics.append(individual_distance_statistics)
        
        
        ## maybe extra function?
        if plot:
            fig, ax = hf.convex_distribution_plot(areas, center, points, dimension=3, equal_axes=False)
            
            ax.scatter(*extracted_path[0], color='green', label='Extracted end position')
            
            # Adjust axis to equal spacing between ticks
            hf.set_axes_equal(ax)
            
            ax.legend()
            
            plt.show()
    
    
    print('Distances of extracted end position to \'centroid\' of simulated end positions: ', trajectory_centroid_distances)
    print('Location (percentile-wise) of extracted end position in cluster: ', end_extracted_percentile_location)
    print('Distances of extracted end position to next more centered distribution area of simulated end positions: ', trajectory_distances_next_area)
    print('Mean distance between extracted and simulated end position(s): ', trajectory_individual_distance_statistics)
    
    
    
    
        # Output analysis outcome
    ## extra function??
    ## output distribution_areas and distribution_center as well? ## maybe .wtk or so?
    
    # Convert shapely objects to WKT format
    for c, centroid in enumerate(trajectory_centers):
        trajectory_centers[c] = centroid.wkt
    
    for site in trajectory_areas:
        for a, area in enumerate(site):
            site[a]['hull'] = area['hull'].wkt
       
        
    parameters = ['centroid_distance', 'percentile_location', 'distances_next_area', 'individual_distance_statistics', 'distribution_areas', 'distribution_center']
    values = [trajectory_centroid_distances, end_extracted_percentile_location, trajectory_distances_next_area, trajectory_individual_distance_statistics, trajectory_areas, trajectory_centers]
    
    json_output = {}
    
    # Assemble correct json_output object for [site1: [parameter1: value1, parameter2: value2, ...], site2: [parameter1: value1, parameter2: value2, ...], ...]
    for j, ID in enumerate(IDs):#[site for site in range(1, number_of_sites+1)]:
    
        # Get values for each parameter corresponding to the specific site
        site_parameter_values = [parameter_value[j] for parameter_value in values]
    
        # Assemble the parameter dict for this site
        site_values = {parameter: value for parameter, value in zip(parameters, site_parameter_values)}
        
        json_output[ID] = site_values
        
    json_output = [json_output]
    
    with open(f'{simulation_name}_analysis.json', 'w') as json_file:
        json.dump(json_output, json_file)
    
    
    
    
    
    
    
