########
# TODO:
#   - 
##########


##
##  Slice huge DTMs to extract workable and interesting portion
##



import numpy as np
#np.set_printoptions(threshold=sys.maxsize)

import sys, os
# only needed if helperfunctions one directory above this file
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code/'))

import helperfunctions as hf

import json


    
def load_LBL_data(filePath, data_names=['LINE_LAST_PIXEL', 'SAMPLE_LAST_PIXEL', 'MAP_RESOLUTION', 'MAP_SCALE', 'MAXIMUM_LATITUDE', 'WESTERNMOST_LONGITUDE', 'A_AXIS_RADIUS', 'CENTER_LONGITUDE', 'CENTER_LATITUDE', 'LINE_PROJECTION_OFFSET', 'SAMPLE_PROJECTION_OFFSET', 'LINE_FIRST_PIXEL', 'SAMPLE_FIRST_PIXEL']):
    """
    #TODO:
        - to be universally usable: some work needed... (units etc..)
    """

    ####
    # Crude extraction of relevant data from .LBL file accompanying .IMG files

    # -IN- #    
    # filePath; <str>:              PATH to file containing a binary table
    
    # data_names; <list(<str>)>:    List of all data identifiers as listed in .LBL files by convention ; Default: ['LINE_LAST_PIXEL', 'SAMPLE_LAST_PIXEL', 'MAP_RESOLUTION', 'MAP_SCALE' (in m/px !!), 'MAXIMUM_LATITUDE', 'WESTERNMOST_LONGITUDE'] (optional)
    
    
    # -OUT- #
    # data; <dict(<str>:<str>)>:  Dictionary with all values corresponding to data_names
    ####
       
    content = []
    # Load file contents and split by whitespace into separate entries
    with open(filePath) as f:
        for line in f.readlines():
            content += line.split()
        
    data = {}
    
    # True if MAP_SCALE (in thus apparently all height values as well) in km ; if False we assume m
    data['km'] = False
    
    # Retrieve the corresponding values
    ## We assume that all data is listed in the file by name following the convention --> otherwise this will throw an exception!
    for data_name in data_names:
        try:
        
            # MAP_SCALE needs extra attention - no convention here... :(
            ## Unit is saved in the next segment following the value - for values with units
            if data_name == 'MAP_SCALE' and 'km' in content[content.index(data_name) + 3]:
                ## We want m/pix...
                data[data_name] = str(float(content[content.index(data_name) + 2])*1000)
                data['km'] = True
            else:
                # By convention data_name and value are separate by ' = '
                data[data_name] = content[content.index(data_name) + 2]
            
        except ValueError:
            print(f'Check the provided .LBL file, whether it includes all relevant information spelled exactly the same way : {data_names} !')
            print(f'\t Error: {sys.exc_info()[0]} : {sys.exc_info()[1]} \n')
            sys.exit(1)
    
    # Sometimes a NoDATA value is specified
    if 'NoDATA' in content:
        ## The last character is a '.' for end of line in those .LBLs we are working with...
        data['NoDATA'] = content[content.index('NoDATA') + 3][:-1]
    else:
        data['NoDATA'] = None
    
    return data
  
   
def load_binary_table(filePath, data_format=np.float32, rows=1, columns=-1):
    """
    #TODO:
    """

    ####
    # Read and return content of a file containing a binary table

    # -IN- #    
    # filePath; <str>:      PATH to file containing a binary table
    
    # data_format; <dtype>: Expected dtype of each entry ; Default: np.float32
    # rows; <int>:          Number of rows the table is expected to hold ; Default: 1
    # columns; <int>:       Number of columns the table is expected to hold ;  Default: -1 (adjust for 1 row)
    
    # -OUT- #
    # content; <np.array(np.float32)>: Array containing each pixel-height
    ####
       
    
    # Load file contents
    content = np.fromfile(filePath,  dtype=np.float32).reshape((rows, columns))
    
    return content
    
    
    
    
def load_features(filePath):
    """
    #TODO:
    """

    ####
    # Read and return content of a .GEOJSON file containing extracted features from map
    ## Coordinates (lon, lat) adjusted to match the map coordinates (lon: map: 180->360/0->180 ; feature: -180->180) and flipped to (lat, lon) !
    ## Works even with shape paths included in features

    # -IN- #    
    # filePath; <str>:      PATH to .GEOJSON file containing extracted features from map
    
    
    # -OUT- #
    # content; <dict()>:            Full content of .GEOJSON file        
    # boundaries; <list(<float>)>:  List containing the minimum and maximum lat/lon coordinates in map coordinate system (map: 180->360/0->180 ; feature: -180->180)
    ####
    
    # Load file contents
    with open(filePath) as f:
        content = json.load(f)
    
    latitude = []
    longitude = []
    
    # Typically multiple extracted features stored in one .GEOJSON
    for feature in content["features"]:
        coordinates = feature["geometry"]["coordinates"]
        
        # Multiple coordinates stored (e.g. for extracted path)
        if isinstance(coordinates[0], list):
            for coords in coordinates:
                # .GEOJSON feature coordinates listed as (lon, lat)
                # different coordinate systems (map: 180->360/0->180 ; feature: -180->180)
                lon = coords[0]
                lat = coords[1]
                if lon < 0:
                    lon = lon + 360
                    longitude.append(lon)
                else:
                    longitude.append(lon)
                latitude.append(lat)
                
                # Flip (lon, lat) to match (lat, lon) ^= (row, column)
                coords[0] = lat
                coords[1] = lon
                
        # Only one coordinate stored (e.g. only single extracted point)
        else:
            # .GEOJSON feature coordinates listed as (lon, lat)
            # different coordinate systems (map: 180->360/0->180 ; feature: -180->180)
            lon = coordinates[0]
            lat = coordinates[1]
            if lon < 0:
                lon = lon + 360
                longitude.append(lon)
            else:
                longitude.append(lon)
            latitude.append(lat)
                 
            # Flip (lon, lat) to match (lat, lon) ^= (row, column)
            coordinates[0] = lat
            coordinates[1] = lon
            
    
    min_lat = min(latitude)
    max_lat = max(latitude)
    min_lon = min(longitude)
    max_lon = max(longitude)
    
    return content, [min_lat, max_lat, min_lon, max_lon]
    
    
    
    
def adjust_table(table, min_row=0, max_row=-1, min_column=0, max_column=-1, offset=None, noData=None, km=False):
    """
    #TODO:
        - NODATA inerference needed?
    """

    ####
    # Return a slice/tile of table in given area and adjust values to be all positive in meters

    # -IN- #    
    # table; <np.array(np.float32)>: Array containing each pixel-height in km
    
    # min_row; <int>:       Start row index
    # max_row; <int>:       Stop row index
    # min_column; <int>:    Start column index
    # max_column; <int>:    Stop column index
    
    # offset; <float>:      Offset to apply to all values ; Default: None -> look at smallest internal value and take that as a reference point to push to 0
    
    # -OUT- #
    # table_slice; <np.array(<>)>: Sliced array
    ####
          
                      
    # Slice array and adjust values
    table_slice = table[min_row:max_row, min_column:max_column]


    # We have to factor out possible interferences with default NoDATA values
    if not noData is None:  
        ## Assuming noData is a abs() big number!  
        noData_indizes = np.argwhere(abs(table_slice - noData) < abs(noData) - np.median(table_slice))
        table_slice[noData_indizes[:, 0], noData_indizes[:, 1]] = np.nan
        
        
            # But we actually want no holes --> have to interpolate the height
        for index in noData_indizes:
            test_index = index.copy()
            
            # While we don't reach a border and are still surrounded by Nones -> try diagonal neighbors in the back
            while all(test_index > 0) and np.isnan(table_slice[test_index[0], test_index[1]]):
                test_index -= 1
                
            # If we failed finding a suitable neighbor -> try going forward
            if any(test_index < 0):
                test_index = index
                # While we don't reach a border and are still surrounded by Nones -> try diagonal neighbors in the front
                while all(test_index - np.asarray(table_slice.shape)-1 < 0) and np.isnan(table_slice[test_index[0], test_index[1]]):
                    test_index += 1
        
            table_slice[index[0], index[1]] = table_slice[test_index[0], test_index[1]]
    
    # Per default: push smallest value to 0
    if offset is None:
        offset = -np.nanmin(table_slice)
    
    
    table_slice = (table_slice + offset)
    
    # If values saved in km
    if km: table_slice *= 1000
    
    return table_slice
    
    

def get_nearest_index(coordinate, tl_row_coordinate, tl_column_coordinate, conversion_factor):
    """
    #TODO:
    """

    ####
    # Return nearest (rounded) corresponding index in array to given coordinate (lat, lon)

    # -IN- #    
    # coordinate; <list(<float>, <float>)>: Coordinate in original coordinate system (lat, lon)
    # tl_row_coordinate; <int>:             Coordinate in original coordinate system of the top left corner's row to adjust center (0, 0) to top left corner of array [0, 0].
    # tl_column_coordinate; <int>:          Coordinate in original coordinate system of the top left corner's column to adjust center (0, 0) to top left corner of array [0, 0].
    # conversion_factor; <float>:           Conversion between pixel and coordinate system: COORDINATES * CONVERSION_FACTOR = PIXEL ; equals MAP_RESOLUTION
    
    
    # -OUT- #
    # coordinate; <list(<int>, <int>)>: Nearest (rounded) corresponding index pair to given coordinates
    ####
    
    row = round(abs((coordinate[0] - tl_row_coordinate) * conversion_factor))
    column = round(abs((coordinate[1] - tl_column_coordinate) * conversion_factor))
   
    return [row, column]
   
   

    

def map_features_to_siconos(features, tl_corner, LonP, LatP, S0, L0, Scale, R, meters_per_pix, grid_shape, crater_name=''):
    """
    #TODO:
        - match other map function??
    """

    ####
    # Return coordinates of features in siconos' coordinate system (origin (0, 0) in center of grid)

    # -IN- #    
    # features; <dict()>:   Full content of .GEOJSON file load previously via load_features() with coordinates (lat, lon) adjusted to be in map coordinate system (map: lon in 180->360/0->180 ; original feature: lon in -180->180)
    
    ## all coordinates expected in map coordinate system (map: 180->360/0->180 ; feature: -180->180)
    # tl_row_coordinate; <int>:     Coordinate in original coordinate system of the top left corner's row to adjust center (0, 0) to top left corner of array [0, 0].
    # tl_column_coordinate; <int>:  Coordinate in original coordinate system of the top left corner's column to adjust center (0, 0) to top left corner of array [0, 0].
    
    #    LonP  | CENTER_LONGITUDE                                        (in DEG)
    #    LatP  | CENTER_LATITUDE                                         (in DEG)
    #    S0    | SAMPLE_PROJECTION_OFFSET                                (unitless)
    #    L0    | LINE_PROJECTION_OFFSET                                  (in DEG)
    #    Scale | MAP_SCALE                                               (in km/px!)
    #    R     | A_AXIS_RADIUS (same as B_AXIS_RADIUS and C_AXIS_RADIUS) (in km)
    
    
    # -OUT- #
    # features; <dict()>:   Full content of input .GEOJSON file modified to contain coordinates in siconos' coordinate system instead of global coordinates 
    ####

    import copy

    features_copy = copy.deepcopy(features)
    
    # Statistics
    path_lengths = []
    
    # Typically multiple extracted features stored in one .GEOJSON
    for feature in features_copy['features']:
    
        coordinates = feature['geometry']['coordinates']
                
        # Multiple coordinates stored (e.g. for extracted path)
        if isinstance(coordinates[0], list):
            for c, coords in enumerate(coordinates):
                
                # Adjust lon,lat -> meter, meter with center (0, 0) in tl_corner (lat, lon)
                ## lat 90 -> -90 !! siconos: (-row) 0 -> row !!
                coords[0] = abs(meters_per_pix * (lat_to_line(coords[0], L0, Scale, R) - tl_corner[0]))
                coords[1] = meters_per_pix * (lon_to_sample(coords[1], LonP, LatP, S0, Scale, R) - tl_corner[1])
                
                # Adjust to center (0, 0) in middle of height map grid
                coords[0] = coords[0] - meters_per_pix*0.5*(grid_shape[0]-1)
                coords[1] = coords[1] - meters_per_pix*0.5*(grid_shape[1]-1)
                
                coordinates[c] = coords
                
                
                # Statistics
            # Ignore the shape paths (e.g. 'path_1s')
            if not any(char in feature['properties']['label'] for char in ('s', 'l')):
                # Transform 2D xy coordinates to 3D xyz coordinates to match calculate_runoutlength() parameter signature 
                coordinates_3D = np.hstack((np.asarray(coordinates), np.zeros((len(coordinates), 1))))
                path_lengths.append(hf.calculate_runoutlength(coordinates_3D)[-1])
            
                
        # Only one coordinate stored (e.g. only single extracted point)
        else:
        
            # Adjust lat,lon -> meter, meter with center (0, 0) in tl_corner
                ## lat 90 -> -90 !! siconos: (-row) 0 -> row !!
            coordinates[0] = abs(meters_per_pix * (lat_to_line(coordinates[0], L0, Scale, R) - tl_corner[0]))
            coordinates[1] = meters_per_pix * (lon_to_sample(coordinates[1], LonP, LatP, S0, Scale, R) - tl_corner[1])
            
            # Adjust to center (0, 0) in middle of height map grid
            coordinates[0] = coordinates[0] - meters_per_pix*0.5*(grid_shape[0]-1)
            coordinates[1] = coordinates[1] - meters_per_pix*0.5*(grid_shape[1]-1)
            
            feature['geometry']['coordinates'] = coordinates
            
            
       
        # Statistics
    # TODO toggle for plotting
    plot = False
    
    #path_lengths_stats = hf.describe_dataset(path_lengths)
    
    
    import matplotlib.pyplot as plt
    
    if plot:
        
        print(hf.describe_dataset(np.atleast_2d(path_lengths).T, whis=[5, 95]))
        fig = plt.figure()
    
        hf.plot_multiboxplot_subplot(fig, path_lengths, x_label=crater_name, y_label='Path lengths in m')
    
        plt.show()
        
    
    
    return features_copy
    
 
 
    
    
def create_rocks_parameters(content, LonP, LatP, R, crater_name=''):
    """
    #TODO:
        - maybe coordinate <-> meters trafo differently !!!
    """

    ####
    # Create set of parameters for each rockfall site
    ## A LOT of magic numbers!

    # -IN- #    
    # features; <dict()>:           Full content of .GEOJSON file load previously via load_features()
    
    #    LonP  | CENTER_LONGITUDE                                        (in DEG)
    #    LatP  | CENTER_LATITUDE                                         (in DEG)
    #    R     | A_AXIS_RADIUS (same as B_AXIS_RADIUS and C_AXIS_RADIUS) (in km)
    
    
    # -OUT- #
    # output; <list(list(<>))>: All necessary rock parameters (including extracted shape_lengths) for each rockfall site
    ####
    
    
        # Extract the shape paths
        
    shapes = {}
    
        
    # We expect pairs of paths for each site
    site_IDs = []#range(1, int(len(shapes.keys())/2)+1)
    
    # Typically multiple extracted features stored in one .GEOJSON
    for feature in content['features']:
        feature_name = feature['properties']['label']
                
        # Extract the shape paths (e.g. 'path_1l', 'path_1s')
        if any(char in feature_name for char in ('s', 'l')):
            shapes[feature_name] = feature['geometry']['coordinates']
            site_IDs.append(int(feature_name.rsplit('_')[-1][:-1]))
            
            
    site_IDs = np.unique(site_IDs)
    
        # Compute lengths
        
    L1 = []
    L2 = []
        
        
    for j in site_IDs:
            # Get coordiante difference -> length of path
        # Trafo to (x,y) in m coordinate system
        start_1 = np.array([lat_to_y(shapes[f'path_{j}l'][0][0], R), lon_to_x(shapes[f'path_{j}l'][0][1], LonP, LatP, R)])
        end_1 = np.array([lat_to_y(shapes[f'path_{j}l'][-1][0], R), lon_to_x(shapes[f'path_{j}l'][-1][1], LonP, LatP, R)])
        L1_j = np.linalg.norm(end_1 - start_1)
    
        # Trafo to (x,y) in m coordinate system
        start_2 = np.array([lat_to_y(shapes[f'path_{j}s'][0][0], R), lon_to_x(shapes[f'path_{j}s'][0][1], LonP, LatP, R)])
        end_2 = np.array([lat_to_y(shapes[f'path_{j}s'][-1][0], R), lon_to_x(shapes[f'path_{j}s'][-1][1], LonP, LatP, R)])
        L2_j = np.linalg.norm(end_2 - start_2)
        
        
        L1.append(L1_j)
        L2.append(L2_j)
        
        
        
        # Statistics
    # TODO toggle for plotting
    plot = False
    
    #L1_stats = hf.describe_dataset(np.atleast_2d(L1).T)
    #L2_stats = hf.describe_dataset(np.atleast_2d(L2).T)
    #A_stats = hf.describe_dataset((np.atleast_2d(L1)/np.atleast_2d(L2)).T)
    
    A = np.array(L1)/np.array(L2)
    
    import matplotlib.pyplot as plt
    
    if plot:
    
        print('a: ', hf.describe_dataset(np.atleast_2d(L1).T, whis=[5, 95]))
        print('b: ', hf.describe_dataset(np.atleast_2d(L2).T, whis=[5, 95]))
        print('c: ', hf.describe_dataset(np.atleast_2d(A).T, whis=[5, 95]))
        
        fig = plt.figure()
    
        hf.plot_multiboxplot_subplot(fig, L1, x_label=crater_name, y_label='Longest axis $a$ in m', columns=3, position=1)
        hf.plot_multiboxplot_subplot(fig, L2, x_label=crater_name, y_label='Second longest axis $b$ in m', columns=3, position=2)
        hf.plot_multiboxplot_subplot(fig, A, x_label=crater_name, y_label='Axis ratio $A = \\frac{a}{b}$', columns=3, position=3)
    
        plt.show()
    
    
    
        # Combine output
        
    output = []
    
    ## TODO MAGIC NUMBERS ##
    
    # Somewhat sharp edges (arbitrary), round polygon as mentioned in Predictive Capabilities of 2D and 3D Block Propagation Models Integrating Block Shape Assessed from Field Experiments
    number_of_points = 40
    # Assuming all boulder beeing impact basin ejacta https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2012GL051319 (mean)
    bulk_density = 2475
    # All errors combined (accuracy, precision, Nac-NAC_DTM [overlay acc, projection/angle deviation, own precision] for Atlas_East!, estimation of starting position)
    starting_uncertainty = 3.84 + 1.26 + 4.124 + 5
    # Estimation of impuls transmission
    v0 = 3
    
    
    for j, ID in enumerate(site_IDs):
        output.append([ID, L1[j], L2[j], 0, number_of_points, bulk_density, starting_uncertainty, v0])
        
        
    return output
    
    
    
    
def lon_to_sample(lon, LonP, LatP, S0, Scale, R):
    """Corrected Equirectengular coordinate transformation from https://pds.lroc.asu.edu/data/LRO-L-LROC-5-RDR-V1.0/LROLRC_2001/CATALOG/DSMAP.CAT

    Args:
        lon (float):    Longitude to convert to sample (column)
        
        LonP  | CENTER_LONGITUDE                                        (in DEG)
        LatP  | CENTER_LATITUDE                                         (in DEG)
        S0    | SAMPLE_PROJECTION_OFFSET                                (unitless)
        Scale | MAP_SCALE                                               (in km/px!)
        R     | A_AXIS_RADIUS (same as B_AXIS_RADIUS and C_AXIS_RADIUS) (in km)
    
    
    Returns:
        Converted sample (column) (float) unitless
    """

    return (lon - LonP)/(180/np.pi) / Scale *(R * np.cos(LatP*np.pi/180)) + S0 + 1
    
    
    
def lat_to_line(lat, L0, Scale, R):
    """Corrected Equirectengular coordinate transformation from https://pds.lroc.asu.edu/data/LRO-L-LROC-5-RDR-V1.0/LROLRC_2001/CATALOG/DSMAP.CAT

    Args:
        lat (float):    Latitude to convert to line (row)
        
        L0    | LINE_PROJECTION_OFFSET                                  (in DEG)
        Scale | MAP_SCALE                                               (in km/px)
        R     | A_AXIS_RADIUS (same as B_AXIS_RADIUS and C_AXIS_RADIUS) (in km)
    
    
    Returns:
        Converted line (row) (float) unitless
    """
    
    return (lat /(-180/np.pi) * R/Scale - 1 + L0)
    
    
    
def lon_to_x(lon, LonP, LatP, R):
    """Corrected Equirectengular coordinate transformation from https://pds.lroc.asu.edu/data/LRO-L-LROC-5-RDR-V1.0/LROLRC_2001/CATALOG/DSMAP.CAT

    Args:
        lon (float):    Longitude to convert to x
        
        LonP  | CENTER_LONGITUDE                                        (in DEG)
        LatP  | CENTER_LATITUDE                                         (in DEG)
        R     | A_AXIS_RADIUS (same as B_AXIS_RADIUS and C_AXIS_RADIUS) (in km)
    
    
    Returns:
        Converted x (float) in m
    """

    return R * (lon - LonP) * np.cos(LatP*np.pi/180) *1000 *np.pi/180
    
    
    
def lat_to_y(lat, R):
    """Corrected Equirectengular coordinate transformation from https://pds.lroc.asu.edu/data/LRO-L-LROC-5-RDR-V1.0/LROLRC_2001/CATALOG/DSMAP.CAT

    Args:
        lat (float):    Latitude to convert to y
        
        R     | A_AXIS_RADIUS (same as B_AXIS_RADIUS and C_AXIS_RADIUS) (in km)
    
    
    Returns:
        Converted y (float) in m
    """
    
    return R * lat *np.pi/180 *1000 
    
    
######################################################################
############################ MAIN ####################################
######################################################################

if __name__ == "__main__":
        
    '''
    # SLDEM2015_512_30S_00S_315_360_FLOAT.LBL
    ## Height in km from average GEOID_RADIUS at the equator
    #?
     LINE_PROJECTION_OFFSET       = -0.5 <pix>
     SAMPLE_PROJECTION_OFFSET     = -69120.5 <pix>
    #?
    '''

        # Input #
        
    cmdOptions = hf.get_CMD_options('hdi:l:f:n:', ['help', 'debug', 'image=', 'lbl=', 'feature=', 'name='], 
        ["help", "Print all debug messages",
        'Path to .IMG (binary table) representing a heightmap',
        'Path to .LBL containg accompanying information to the .IMG file - 0 to infer from .IMG path provided',
        'Path to .GEOJSON containing all extracted paths, as well as paths corresponding to the longest axis and perpendicular extension of each rock shape - if rock_params are supposed to be processed.',
        'Name of the region of interest (ROI) - will be used for the output files'])    

    # check IO interaction
    for j in range(2, len(cmdOptions)):
        if cmdOptions[j] is None:
            print('Not enough file paths given!')
            exit(-1)
        else:
            cmdOptions[j] = cmdOptions[j].strip()

    tablePath = cmdOptions[2]
    lblPath = cmdOptions[3]
    featurePath = cmdOptions[4]
    ROI_name = cmdOptions[5]


    # Infer the .LBL file path automatically from .IMG if set
    if lblPath == '0':
        lblPath = tablePath.split('.')[0] + '.LBL'


    # Load defining parameters for this .IMG
    data_dict = load_LBL_data(lblPath)

    # Load raw table
    ## WATCH OUT! CONVENTION FAILURE (rows+1) SOMETIMES! ##
    try:
        table = load_binary_table(tablePath, rows=int(data_dict['LINE_LAST_PIXEL']), columns=int(data_dict['SAMPLE_LAST_PIXEL']))
    except ValueError:
        table = load_binary_table(tablePath, rows=int(data_dict['LINE_LAST_PIXEL'])+1, columns=int(data_dict['SAMPLE_LAST_PIXEL']))
        

    # Load extracted features
    ## Coordinates: (lon, lat) with lon in [-180, 180] !!
    features, boundaries = load_features(featurePath)


        # Slice map #
        
        # Coordinate system transformation (lat, lon) -> (row, column) == (Line, Sample)
            
    R = float(data_dict['A_AXIS_RADIUS'])
    LonP = float(data_dict['CENTER_LONGITUDE'])
    LatP = float(data_dict['CENTER_LATITUDE'])
    L0 = float(data_dict['LINE_PROJECTION_OFFSET'])
    S0 = float(data_dict['SAMPLE_PROJECTION_OFFSET'])
    
    row_start = int(data_dict['LINE_FIRST_PIXEL'])
    column_start = int(data_dict['SAMPLE_FIRST_PIXEL'])
    
    
    # Resolutions
    pix_per_deg = float(data_dict['MAP_RESOLUTION'])
    meters_per_pix = float(data_dict['MAP_SCALE'])
    # Scale in km/px
    Scale = meters_per_pix/1000

    # Meaning of values
    noData_value = data_dict['NoDATA']
    if not noData_value is None:
        noData_value = float(data_dict['NoDATA'])
        
    km = data_dict['km']

    # Extra boundary of extra_meters/meters_per_pixel/resolution around desired area to avoid falling of edges during simulation
    ## TODO MAGIC NUMBER in m ## CAN BREAK THINGS !!!
    extra_boundary = 300/(meters_per_pix*pix_per_deg)

    new_min_lat = boundaries[0] - extra_boundary
    new_max_lat = boundaries[1] + extra_boundary
    new_min_lon = boundaries[2] - extra_boundary
    new_max_lon = boundaries[3] + extra_boundary

    
    ## min_row == max_lat (90 -> -90) ! BUT min_column == min_lon (0 -> 360) !
    min_row = int(lat_to_line(new_max_lat, L0, Scale, R)) -row_start
    max_row = int(np.ceil(lat_to_line(new_min_lat, L0, Scale, R))) -row_start
    min_column = int(lon_to_sample(new_min_lon, LonP, LatP, S0, Scale, R)) -column_start
    max_column = int(np.ceil(lon_to_sample(new_max_lon, LonP, LatP, S0, Scale, R))) -column_start
    
    # Adjust table to contain values >= 0 in meters in desired slice
    sliced_table = adjust_table(table, min_row=min_row, max_row=max_row, min_column=min_column, max_column=max_column, noData=noData_value, km=km)
    slice_shape = sliced_table.shape


        # Feature manipulation #
        
        # Coordinate transformation to siconos' coordinate system
    # Map (lon, lat) extracted coordinates to siconos' internal coordinate system for better accesss later
    ## tl_corner == (min_row, min_column) in original table coordinate system!
    new_features_siconos = map_features_to_siconos(features, (min_row, min_column), LonP, LatP, S0, L0, Scale, R, meters_per_pix, slice_shape, crater_name=ROI_name)

        # Assemble rock parameters [IF shape paths provided in .GEOJSON ; otherwise empty .csv file will be generated
    rock_parameters = create_rocks_parameters(features, LonP, LatP, R, crater_name=ROI_name)

        # Output #

    # Output heightmap to ESRI_ASCII, cellsize: resolution in meters
    hf.write_ESRI_ASCII(sliced_table, meters_per_pix, filename=f'{ROI_name}_map.asc')

    # Output soil to ESRI_ASCII (using 1 soil type)
    hf.write_ESRI_ASCII(np.ones(slice_shape), meters_per_pix, filename=f'{ROI_name}_soil.asc')


    # Output transformed coordinates in siconos' coordinate system
    with open(f'{ROI_name}_siconos.geojson', 'w') as output_features_file:
        json.dump(new_features_siconos, output_features_file)


    # ID: rock ID corresponding to one extracted boulder ; L1, L2, L3 in m ; points: number of points to wrap convex hull around ; bulk_density in kg m-3 ; starting_uncertainty in m ; v0 in m/s
    hf.write_CSV(['ID', 'L1', 'L2', 'L3', 'points', 'bulk_density', 'starting_uncertainty', 'v0'], rock_parameters, f'{ROI_name}_rock_parameters.csv')






