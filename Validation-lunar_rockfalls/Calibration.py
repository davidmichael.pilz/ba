########
# TODO:
#
##########



#
# Simple script to run all files/code necessary for preprocessing, simulation and analysis of rockfalls in one crater
#


import numpy as np

import sys, os

# Only needed if helperfunctions in different directory
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))
import helperfunctions as hf

import json
import copy


######################################################################

       
def load_statistics(filePath):
    """
    #TODO:
    """

    ####
    # Read and return selected content of a .JSON file containing the statistic over all analysis parameters
    
    
    # -IN- #    
    # filePath; <str>:  PATH to .JSON file containing all statistic findings
    
    
    # -OUT- #
    # analysis; <np.array(<float>)>:    Statistics for each parameter (mean/median and sample_variance) ordered as in parameters
    # parameters; <list(<str>)>:        All parameters labels responsible for two consecutive items in analysis
    ####
    
    # Load file contents
    with open(filePath) as f:
        content = json.load(f)
    
    statistics = {}
        
    # Load all relevant parameters for each site
    for p, parameter in enumerate(content[0].keys()):
        
        parameter_statistic = content[0][parameter]
        
            # centroid_distance
        if parameter == 'centroid_distance':
            # mean: we care about outliers!, bad (absolute) simulations - even only sometimes - should be treated as such
            statistics[parameter] = [parameter_statistic['mean'], parameter_statistic['sample_variance']]#, parameter_statistic['max']]
            
            # percentile_location
        elif parameter == 'percentile_location_lower':
            # median: we don't care about outliers as much! this is a relativ metric comparing/ranking the observed location relativ to the cluster of simulation positions ; outliers should not skew the overall relativ impression too much as those depend much on the variance/spread of the simulation without saying anything absolute about exactly those
            statistics[parameter] = [parameter_statistic['median'], parameter_statistic['sample_variance']]
        elif parameter == 'percentile_location_upper':
            statistics[parameter] = [parameter_statistic['median'], parameter_statistic['sample_variance']]
            
            # individual_distance_statistics['mean'] 
            ## not needed for distribution perspective ; extra metric  
        elif parameter == 'individual_distance_statistics':
            # mean: we care about outliers!, bad (absolute) simulations - even only sometimes - should be treated as such
            statistics[parameter] = [parameter_statistic['mean'], parameter_statistic['sample_variance']]
            
      
    
        # Assemble values for easy access (and keep them ordered!)
        ## switch from name selection to index selection
    parameters = ['centroid_distance', 'percentile_location_lower', 'percentile_location_upper', 'individual_distance_statistics']
    values = []
        
    for parameter in parameters:
        values.append(statistics[parameter])
           
    # Flatten nested list
    values = [value for statistic in values for value in statistic]
    
    return values, parameters
    
    
    
    
def rank_results(results):
    """
    #TODO:
        - catch errors (no outliers etc.)
        - maybe loop iteratively over extraordinary close levels -> update and loop over small set!
        - include multiple metrics?
    """

    ####
    # Rank the results to find best one(s), first by centroid_distance_m, then percentile_location_upper_m
    
    
    # -IN- #    
    # results; np.array(<float>):   All statistic values (column) for each soil_set (row) with fields defined: (ID, 'centroid_distance_m', 'centroid_distance_variance', ..) further including ('percentile_location_lower', 'percentile_location_upper', 'individual_distance_statistics') in the same manner (one mean/median, one variance)
    
    
    # -OUT- #
    #
    ####
    
    
    # 1. Simple sort (descending) by centroid_distance_m (ean)
    ## Alternatively: only individual_distance_mean    
    results = np.sort(results, order='centroid_distance_m')



        # Get consecutive differences   
             
    differences = []

    # Get definitions for structured array
    ## Exclude 'ID' from dtype
    dtype = results.dtype.names[1:]
    
    # Excluding the last one: there is no lower entry to compare to
    for l, level in enumerate(results[1:]):
        # Only l not l-1 because we already start with an index offset (slicing the array)!
        differences.append([abs(results[l][value] - level[value]) for value in dtype])


        # And find the low outliers 
        
        ## We give extraordinary close (difference in centroid_distance_m smaller than Q1 - 1.5*(Q3-Q1) i.e. outside low whisker in box plot when using Tukey's original definition) levels a second chance to compete in the next metric

    differences = np.array(differences)

    # Get outliers
    differences_statistics = hf.describe_dataset(differences, whis=1.5)
    outliers_centroid_distance_m = np.array(differences_statistics['fliers'][0])
    
    # Get _low_ outliers
    low_outliers = outliers_centroid_distance_m[np.argwhere(outliers_centroid_distance_m < differences_statistics['median'][0]).flatten()]
    
    
    # Second comparison if there are any extraordinary close levels 
    if len(low_outliers) > 0:
        
            # Second comparison (ascending) by percentile_location_upper_m
        
        # Backtrack levels (indizes) for every outlier found
        second_chance_levels_old_indices = []
        for low_outlier in low_outliers:
            # We need +1 as difference_indices started one to early for results_indices due to slicing
            second_chance_levels_old_indices += (np.argwhere(differences[:, 0] == low_outlier).flatten() +1).tolist()
            
        # and append the next lowest index for the upcoming comparison (differences where computed from the lower rank (high index) perspective)
        second_chance_levels_old_indices.append(np.amin(second_chance_levels_old_indices) - 1)
        
        # Sort indizes and get unique to make sorting results by (if appropriate) consecutive flipping pairs possible
        ## axis=None necessary?!
        second_chance_levels_old_indices = np.sort(np.unique(second_chance_levels_old_indices, axis=None)).tolist()
        
        
        # Get levels and sort again
        ## Sort optimistically ('this level could be pretty good') for upper bound
        ## We flip because we'd like the ascending order for percentile_location_upper_m
        second_chance_levels = np.flip(np.sort(results[second_chance_levels_old_indices], order='percentile_location_upper_m'))
            
            
            
            # Update ranking
        
        # Compare relative ranking for all consecutive pairs
        while not len(second_chance_levels_old_indices) == 0:
            
            index = second_chance_levels_old_indices.pop(0)
            next_index = index + 1
            # Check if consecutive index is part of second_chance_levels (if even comparable)
            if next_index in second_chance_levels_old_indices:
                level_high = results[index].copy()
                level_low = results[next_index].copy()
                
                # Get new index from second ranking
                new_index_high = np.argwhere(second_chance_levels['ID'] == level_high['ID'])
                new_index_low = np.argwhere(second_chance_levels['ID'] == level_low['ID'])
                
                # Flip if appropriate (if relativ ranking changed)
                if new_index_low < new_index_high:
                    results[index] = level_low
                    results[next_index] = level_high
                    
                    
        ## TODO This could go on include e.g. the variance of the distance etc..
                    
                    
    return results
    
    
    

######################################################################
############################ MAIN ####################################
######################################################################


if __name__ == "__main__":

        # Paths and input name
    crater_name = 'Atlas_East'
    heightmap_path = f'data/{crater_name}/0_raw_data/NAC_DTM_ATLAS1_E473N0448.IMG'
    LBL_path = f'data/{crater_name}/0_raw_data/NAC_DTM_ATLAS1.LBL'
    features_path =  f'data/{crater_name}/0_raw_data/paths_and_shapes.geojson'
    rocks_path = None #'data/Euclides_K/rocks_params.csv'
    soil_path = f'data/{crater_name}/soil_params.csv'
    LHS_path = ''
    
    #"""
    
        # Test-Train (Calibration-Validation) split features
        
    if not 'calibration' in features_path:
        # Load file contents
        with open(features_path) as f:
            content = json.load(f)
                
        ## We have to account for the shape paths as well!
        number_of_features = int(len(content['features'])/3)
        # Get all IDs (+1 as IDs start at 1)
        all_IDs = np.arange(number_of_features) + 1
        
        
        # TODO do not run all IDs due to limited computational resources !
        ## we simulate 20*13*0.8*38 calibration rocks ; 20*13*0.2 validation rocks
        number_of_features = 6
            
        seed = 42 #11235813213455
        rng = np.random.Generator(np.random.PCG64(seed))
        
        # Get calibration IDs 
        train_percentage = 0.8
        calibration_IDs = rng.choice(all_IDs, int(train_percentage*number_of_features), replace=False)
        validation_IDs = [ID for ID in all_IDs if ID not in calibration_IDs]
        # We might not want to validate _all_ other IDs
        validation_IDs = rng.choice(validation_IDs, int(np.ceil((1- train_percentage)*number_of_features)), replace=False)
            
        ## extra function would be nicer..
        
            # Modify original content and output calibration features [leave original dict intact though!]
        calibration_features_path = f'data/{crater_name}/calibration_features.geojson'
        calibration_content = copy.deepcopy(content)
        
        remove_indices_calibration = []
        remove_indices_validation = []
        
        # Filter the validation paths
        for f, feature in enumerate(content['features']):
            label = feature['properties']['label']
            # Account for the shape paths as well
            try:
                ID = int(label.rsplit('_')[-1])
            except ValueError:
                ID = int(label.rsplit('_')[-1][:-1])  
            if not ID in calibration_IDs:
                remove_indices_calibration.append(f)
            if not ID in validation_IDs:
                remove_indices_validation.append(f)
                
                
        calibration_content['features'] = [feature for f, feature in enumerate(calibration_content['features']) if f not in remove_indices_calibration]
                      
        with open(calibration_features_path, 'w') as output_features_file:
            json.dump(calibration_content, output_features_file)
        
        
        
            # Modify original content and output validation features [leave original dict intact though!]
        validation_features_path = f'data/{crater_name}/validation_features.geojson'
        validation_content = copy.deepcopy(content)
                
        validation_content['features'] = [feature for f, feature in enumerate(validation_content['features']) if f not in remove_indices_validation]
        
        
        with open(validation_features_path, 'w') as output_features_file:
            json.dump(validation_content, output_features_file)
        
        
        # Point to new features.geojson
        features_path = calibration_features_path
        
        
    
    
        # LHS for soil parameters
    
    ### when is space filled good enough??!????
    ## confidence intervals for uniform distribution?
    # -> Matala 2008 ?
    ## Space Filling Designs for Modeling & Simulation Validation - Heather Wojton ; Matala as well
    # -> rule of thumb: 10x param number

    ## Matala table: 38 =^ 90%, 90% (at least for random sampeling ?)
    
    # If no path provided -> create LHS
    if LHS_path == '':
        LHS_path = f'data/{crater_name}/LHS_soil.csv'
        LHS = hf.create_levels(3, 38)
        hf.write_CSV(['e', 'mu', 'mu_r'], LHS, LHS_path)
        
        
    LHS = hf.read_CSV(LHS_path)


              
          # Execute scripts
              
    for s, soil in enumerate(LHS):
        
        print(f'\nSoil set {s}:\n')
        
            # Write soil parameters to soil_params.csv
        parameters = soil.keys()
        values = [[float(soil[parameter]) for parameter in parameters]]
                
        hf.write_CSV(parameters, values, f'data/{crater_name}/soil_params.csv')
        
            # Run simulation and analyse results
        if s == 0:
            # With preprocessing
            hf.run_scripts(crater_name, heightmap_path, LBL_path, features_path, soil_path, rocks_parameters=rocks_path, simulation_name=f'level_{s}')
        else: 
            # _WITHOUT_ preprocessing
            hf.run_scripts(crater_name, heightmap_path, LBL_path, features_path, soil_path, rocks_parameters=rocks_path, run_preprocessing=False, simulation_name=f'level_{s}')
        
        
    #"""
    
        # Find best LHS level
    
    soil_statistics = []
    dtype = [('ID', 'U10')]
    
    for s in range(11):#len(LHS)):
    
        # Extract relevant parameters
        statistics, parameters = load_statistics(f'data/{crater_name}/4_statistics/level_{s}_statistics.json')
        
        soil_statistics.append((f'level_{s}', *statistics))
        
    # We need to double the dytpe for the parameters as we always include mean/median and variance through load_statistics()
    statistics_dtype = [[(parameter + '_m', float), (parameter + '_variance', float)] for parameter in parameters]
    
    # Flatten the nested list
    dtype += [partial_dtype for stats_dtype in statistics_dtype for partial_dtype in stats_dtype]
    
    
    soil_statistics = np.array(soil_statistics, dtype=dtype)
    
    # Rank results
    ranking = rank_results(soil_statistics)
    print([level for level in ranking[:10]])
    
    
    
        # Output ranking
    
    hf.write_CSV(ranking.dtype.names, ranking, f'{crater_name}_calibration_ranking.csv')
    
    
    
    
    
    
