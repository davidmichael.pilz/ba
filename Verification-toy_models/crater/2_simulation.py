"""
# TODO:
"""

#
# Example of how to use the heightmap object to interact with static terrain.
#


from siconos.mechanics.collision.convexhull import ConvexHull
from siconos.mechanics.collision.tools import Contactor
from siconos.io.mechanics_run import MechanicsHdf5Runner
import siconos.numerics as sn
import siconos.kernel as sk
import numpy as np
#np.set_printoptions(threshold=sys.maxsize)


import sys, os

# needed for easy access to helperfunctions in different directory (../shared_code)
sys.path.append(os.path.join(os.path.dirname(__file__), '../../shared_code'))
import helperfunctions as hf


##################################################################################
        
        
        
def soil_setup(io_hdf5, soil_heightmap, soil_parameters, resolution):
    """
    #TODO:
    """
    
    ####
    # Modify siconos simulation adding one soil type with corresponding heightmap (Default collision_group1=100, collision_group2=1)

    # -IN- #    
    # io_hdf5; <siconos.MechanicsHdf5Runner>:   Simulation to modify
    # soil_heightmap; <np.array(<float>)>:      Corresponding heightmap to soil (m)
    # soil_parameters; <dict(<str>:<float>)>:   Dict containing values for 'e', 'mu', 'mu_r' in [0, 1]
    # resolution; <float>:                      Resolution of soil_heightmap grid to infer x,y position of br_corner (m/pix)
    
    # -OUT- #
    # 0 on success
    ####

    
            # Add map and soil params to simulation
    ## X [Y] extension only (nX [nY] -1)*resolution as each grid point corresponds to one terrain junction ! (i.e. tl_corner == 0, 0 ; tr_corner == 0, -1 !)
    io_hdf5.add_height_map('Height_map', soil_heightmap, (resolution*(soil_heightmap.shape[0]-1), resolution*(soil_heightmap.shape[1]-1)), outsideMargin=0.01, insideMargin=0.01)
    io_hdf5.add_object('Ground_object', [Contactor('Height_map', collision_group=1)],translation=[0, 0, 0])        
    
    # Add contact law
    io_hdf5.add_Newton_impact_rolling_friction_nsl('Contact_law', e=float(soil_parameters["e"]), mu=float(soil_parameters["mu"]), mu_r=float(soil_parameters["mu_r"]), collision_group1=100, collision_group2=1)
                
                
    return 0
    
    
    
    
def spheres_setup(io_hdf5, mZ, indx_dep, v_min, v_max, resolution):

        # Shape (geometric primitive, sphere)

    radius = 1
    
    io_hdf5.add_primitive_shape('Ball', 'Sphere', [radius])

    
    
        # Compute starting position

    for start, start_index in enumerate(indx_dep):

        # x -> rows ; y -> cols (x ^ y ->) !!
        ## X [Y] extension only (nX [nY] -1)*resolution as each grid point corresponds to one terrain junction ! (i.e. tl_corner == 0, 0 ; tr_corner == 0, -1 !)
        Xpos = resolution* (start_index[0] - 0.5*(mZ.shape[0]-1))
        Ypos = resolution* (start_index[1] - 0.5*(mZ.shape[1]-1))
        
        # If analyse bouncing properties: +15
        Zpos = mZ[start_index[0], start_index[1]]
        
            
            
            # Avoid clipping through surface (we have space filling shapes !)
                
        # Get the unit normal to the surface at the starting point -> that's the desired orientation as well
        surface_normal = hf.indices_calculate_normal(mZ, start_index, cell_side_length=resolution)
        
        
        # Offset the position to avoid clipping through the surface
        posInit = np.array([Xpos,Ypos,Zpos]) + surface_normal*(radius)
        posInit = posInit.tolist()
        
        

            # Compute velocity direction
        nX, nY = mZ.shape

        #### TODO: generalize!!!! via xll, yll
        # Center assumed at (0, 0)
        maxX = nX/2 * resolution
        maxY = nY/2 * resolution

        # Coordinate axis (x,y)
        x_axis = np.linspace(-maxX, maxX, nX)    
        y_axis = np.linspace(-maxY, maxY, nY)
        
        # Coordinate grid (adjust shapes)
        mX, mY = np.meshgrid(x_axis, y_axis, indexing='ij')
            

        def f(x, y, s1, s2): return x**2/s1**2 + y**2/s2**2
        
        # Gradient of the underlying function used for computation of paraboloid
        ## We want the _decline_ not _incline_ ! -> -
        grad = [-2*mX/maxX, -2*mY/maxY]
        # (x, y, f(x, y)) -> (x + dz/dx, y + dz/dy, f(x+dx, y+dy)) <=> vec = r2-r1 = (dz/dx, dz/dy, f(x+dx, y+dy) - f(x, y)
        surfaceVec = [*grad, f(mX + grad[0], mY + grad[1], maxX, maxY) - f(mX, mY, maxX, maxY)]      

        for velocity, v in enumerate(np.linspace(v_min, v_max, 4)):
            # Get normalized gradient
            grad = np.array([surfaceVec[0][start_index[0], start_index[1]], surfaceVec[1][start_index[0], start_index[1]], surfaceVec[2][start_index[0], start_index[1]]])
            grad /= np.linalg.norm(grad)
            
            v_x = grad[0]*v
            v_y = grad[1]*v
            v_z = grad[2]*v
            
            
            
            io_hdf5.add_object(f'ball_{start}_{velocity}', [Contactor('Ball', collision_group=100)], translation=posInit, velocity=[v_x, v_y, v_z, 0, 0, 0], orientation=[1, 0, 0, 0], mass=1)
            
            
            
    return 0
    


########################################################################################


if __name__ == "__main__":

    cmdOptions = hf.get_CMD_options("hdg:b:s:m:n:", ["help", "debug", "grid=", "blocks=", "soil=", "blocksParams=", "soilParams="], 
            ["help", "Print all debug messages",
            "Path to heightmap (ESRI ASCII (.asc))", 
            "Path to blocks starting position grid (ESRI ASCII (.asc))",
            "Path to soil grid (ESRI ASCII (.asc))",  
            "Path to soil parameters file (.csv); Expected: (e, mu, mu_r)", 
            "Path to sphere parameters file (.csv); Expected: (v_min, v_max)"])    



    # check IO interaction
    for j in range(2, len(cmdOptions)):
        if cmdOptions[j] is None:
            print("Not enough file paths given!")
            exit(-1)
        else:
            cmdOptions[j] = cmdOptions[j].strip()


            # Load heightmap
    _, _, mZ, misc  = hf.read_ESRI_ASCII(cmdOptions[2])

    # initial_resolution
    init_res = float(misc[0]["cellsize"])


            # Block departure zones

    dep = hf.read_ESRI_ASCII(cmdOptions[3])[2]
    indx_dep = np.argwhere(dep>0)


            # Soil zones
            
    zones = hf.read_ESRI_ASCII(cmdOptions[4])[2]


            # Soil data
            
    soilParams = hf.read_CSV(cmdOptions[6])[0]
           
           
            # Sphere starting params           
            
    spheresParams = hf.read_CSV(cmdOptions[5])[0]
    
    v_min = float(spheresParams["v_min"]) #0.
    v_max = float(spheresParams["v_max"]) #5.


    # Max time to simulate in s
    T = 50.


    with MechanicsHdf5Runner() as io:


            # Setup soil
        soil_setup(io, mZ, soilParams, init_res)
        
            # Setup spheres
        spheres_setup(io, mZ, indx_dep, v_min, v_max, init_res)
                          

        # By default earth gravity is applied and the units are those
        # of the International System of Units.

        # We run the simulation with relatively low precision because we
        # are interested mostly just in the location of contacts with the
        # height field, but we are not evaluating the performance of
        # individual contacts here.


        ## TODO: Magic Settings

            # Setup siconos solver
            
        # Create solver options
        options = sk.solver_options_create(sn.SICONOS_ROLLING_FRICTION_3D_NSGS)
        options.iparam[sn.SICONOS_IPARAM_MAX_ITER] = 1000
        # If an object has not moved after 20 timesteps it will be disregarded from further propagation
        options.iparam[sn.SICONOS_FRICTION_3D_NSGS_FREEZING_CONTACT] = 20
        options.dparam[sn.SICONOS_DPARAM_TOL] = 1e-4


        io.run(
            with_timer=False,               # ?
            multipoints_iterations=True,    # ?
            gravity_scale=9.81/1.62,        # Divisor to 9.81 , i.e. if e.g. g = 1.62 is desired: gravity_scale=9.81/1.62 ; https://nssdc.gsfc.nasa.gov/planetary/factsheet/moonfact.html
            t0=0,                           # Start time
            T=T,                            # End time
            h=1e-3,                         # Size of time step
            theta=0.50001,                  # ?
            Newton_max_iter=1,              # ?
            set_external_forces=None,       # ?
            solver_options=options,         # Specify the solver ?
            violation_verbose=False,        # ?
            numerics_verbose=False,         # ?
            output_frequency=None,            # What datapoints (time points) to save to .hdf5 file
            output_contact_index_set=0      # ?
        )
        

