##########
# TODO: #
#
##########


##
##  Run all desired siconos simulations
##



import numpy as np
# Quasi-Monte_carlo
from scipy.stats import qmc

import sys, getopt, os
import subprocess

import matplotlib.pyplot as plt


import helperfunctions as hf
np.set_printoptions(threshold=sys.maxsize)


def read_cmd():

    """
    # TODO:
        - safety-check !?
    """


    ####
    # Load heightmap as well as soil and blocks parameters from files

    # -IN- #    
    # gridFilePath; <FILE_PATH>:    Path to heightmap (ESRI ASCII (.asc)) (CLA)
    # soilFilePath; <FILE_PATH>:    Path to soil parameters file (.csv); Expected: (e, mu, mu_r) (CLA)
    # blocksFilePath; <FILE_PATH>:  Path to blocks parameters file (.csv); Expected: (number_blocks, height_fall_min, height_fall_max, number_points_hull, v_min, v_max) (CLA)
    
    # plot; <bool>:                 Plot 3D soil separation and blocks starting position via matplotlib UI (optional CLA) 
    # debug; <bool>:                Display debug info on stdout (optional CLA) 
    
    # -OUT- #
    # mX; <np.array(<float>)>:                  X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>:                  Y-axis grid (2D matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>:                  Z-axis grid (2D matrix ; heightmap), shaped like mX; mY ; (only needed for plotting)
    # (maxX, maxY,                              Miscellaneous values: maxX/maxY: max(x/y_axis); 
    #   cellsize,                               'resolution' of heightmap;
    #   nSoilTypes); <tuple(<float>)>:          number of different soil types
    
    # soilParameters; <np.array(<float>)>:      All input soil parameters (only needed for plotting)
    # blocksParameters; <np.array(<float>)>:    All input blocks parameters (m-n relation, m<=n (<: for m->n: default to last parameter set) for n: numberSoilTypes) (mostly needed for plotting)
    
    # inPlot
    # inDebug
    ####
        
        
    cmdOptions = hf.get_CMD_options("hdpg:m:n:", ["help", "debug", "plot", "grid=", "blocksParams=", "soilParams="], 
        ["help", "Print all debug messages", "Plot 3D soil separation and blocks starting position via matplotlib UI",
        "Path to heightmap (ESRI ASCII (.asc))", 
        "Path to soil parameters file (.csv); Expected: (e, mu, mu_r)", 
        "Path to blocks parameters file (.csv); Expected: (number_blocks, height_fall_min, height_fall_max, number_points_hull, v_min, v_max)"])        
       
    inDebug = cmdOptions[1]
    inPlot = cmdOptions[2]
       
    # check IO interaction
    for j in range(3, len(cmdOptions)):
        if cmdOptions[j] is None:
            print("Not enough file paths given!")
            exit(-1)
   
   
    # load heightmap
    mX, mY, mZ, miscDict = hf.read_ESRI_ASCII(cmdOptions[3])
    
    maxX, maxY = miscDict[1], miscDict[2]
    miscDict = miscDict[0]
    
    
    # load parameters
    blocksParameters = hf.read_CSV(cmdOptions[4])
    soilParameters = hf.read_CSV(cmdOptions[5])


    # safety-check for number of parameters
    ### in extra function?? -> helperfunction?
    ## rather ValueError than AssertionError ?
    try:
        nSoilTypes = len(soilParameters)
        nSoilParams = len(soilParameters[0])
        
        # Expected: (e, mu, mu_r)
        if nSoilParams != 3:
            raise AssertionError("Not enough or too many parameters given. Expected 3, got " + str(nSoilParams) + "!")
        
    except:    
        print("  - An error occured while trying to load the soil parameters.\n    Did you specify everything correctly?\n    Expected shape: e, mu, mu_r")
        print("  - Error log:\n")
        print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")
        sys.exit(-1)
    
        
    try:
        nBlockParams = len(blocksParameters[0])
        
        # Expected: (number_blocks, height_fall_min, height_fall_max, number_points_hull, v_min, v_max)
        if nBlockParams != 6:
            raise AssertionError("Not enough or too many parameters given. Expected 6, got " + str(nBlockParams) + "!")
        
    except:    
        print("  - An error occured while trying to load the blocks parameters.\n    Did you specify everything correctly?\n    Expected shape: (number_blocks, height_fall_min, height_fall_max, number_points_hull, v_min, v_max")
        print("  - Error log:\n")
        print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")
        sys.exit(-1)
    
            
    return mX, mY, mZ, (maxX, maxY, float(miscDict["cellsize"]), nSoilTypes), soilParameters, blocksParameters, inPlot, inDebug
    
    
    
   
    
def run_simulation(level, simulation_path, simulation_options, output_name=None):
    """
    # TODO:
        - input simulation path as well as (other) options!
        - check execution ofr errors        
    """


    ####
    # Run the simulation with the given parameters ('factors') in the level

    # -IN- #    
    # level; <list(<float>)>:               A list containing the value for each parameter ('factor') needed as input for the simulation ; Choosing the first three to be 'e', 'mu', 'mu_r'
    ##                                          At the moment only len(list) == 3 supported
    # simulation_path; <str>:               PATH (absolute or relative) to python3 executable script taking at least "-n <soil_params_file>" as input option
    # simulation_options; <list(<str>)>:    List containg all extra options to script at simulation_path
    
    # -OUT- #
    # I/O: Created .hdf5 file containing the simulation output named <output_name>
    ####
    
     
    simulation_dir, simulation_name = simulation_path.rsplit("/", maxsplit=1)
    simulation_output = simulation_name.rsplit(".", maxsplit=1)[0]

    
    ## could be done nicer in while output_name is None or os.path.exists(output_name) ... but beware special cases!
    # If no output_name specified -> chose a generic one
    if output_name is None:
        # Hopefully 100 tries should be enough 
        for j in range(1, 101):
            output_name = f"siconos_output_{j}"
            if not os.path.exists(f"{simulation_dir}/{output_name}.hdf5"):
                break
            elif j == 100:
                print(f"WARNING: This will override the existing output-file: {output_name}!\nPress ENTER to continue anyway")
                input()
                
    # Check if a file with the specified output_name already exists
    elif os.path.exists(f"{simulation_dir}/{output_name}.hdf5"):
        print(f"WARNING: This will override the existing output-file: {output_name}!\nPress ENTER to continue anyway")
        input()
    
    
    
        # Adjust simulation input
        ## We keep the heightmap and starting positions (at least the boundaries) the same
       
    ## Only ONE soil type supported! --> maybe just add 3 more vars?: and than pick ascending (soil1: 0:2 etc.) ;  would need restructuring of level!
    hf.write_CSV(["e", "mu", "mu_r"], [level], f"{simulation_dir}/soil_params_{output_name}.csv")
        
        # Run simulation with varied soil parameters
    ### https://docs.python.org/3/library/subprocess.html#module-subprocess
    simulation = subprocess.run(["python3", simulation_path, *simulation_options, f"-n {simulation_dir}/soil_params_{output_name}.csv"], capture_output=True)
    
    print("errors: ", simulation.stderr)
    
    # Rename output file
    subprocess.run(["mv",  f"{simulation_output}.hdf5", f"{simulation_dir}/{output_name}.hdf5"])
    
    
    return
    
    
    
    
def run_simulation_levels(levels, simulation_path, simulation_options):
    """
    # TODO:
        - safety-check !?
    """


    ####
    # Run the simulation multiple times with different parameter-sets ('levels')

    # -IN- #    
    # levels; <list(<list(<float>)>)>:    A list containing all levels to be run (i.e. passed to run_simulation())
    
    # -OUT- #
    ####
        
           
    for l, level in enumerate(levels):
        run_simulation(level, simulation_path, simulation_options, output_name=f"simulation_level_{l}")
        print(f"\nsim {l} done")
        #input()
    
    return
    
    
    
    

def test_robustness(soil_parameters, number_runs=100):
    """
    # TODO:
    """


    ####
    # Run one parameter set number_runs times to check robustness
    ## dir and simulation file fixed!!

    # -IN- #    
    # soil_parameters; <list(<float>)>: All soil parameters (e, mu, mu_r) in list
    
    # number_runs; <int>:               Number of repetitions to run; Default: 100
        
   
    
    # -OUT- #
    ####

        
    lvls = []
    for j in range(number_runs):
        # level 10: best overall
        lvls.append(soil_parameters)

    run_simulation_levels(lvls, "incline/siconos_sphere.py", ["-g incline/incline_1000.0_100.0_20.0.asc", "-s incline/ellipse_soil_500.0_50.0_1.asc", "-b incline/ellipse_blocks_500.0_50.0_1.asc", "-m incline/blocks_params.csv"])

        # Cleanup
    # Create new directory
    subprocess.run(["mkdir", f"incline/robustness"])
    
    # and move all files into the new directory
    for j in range(number_runs):
        subprocess.run(["mv",  f"incline/simulation_level_{j}.hdf5", f"incline/robustness/simulation_level_{j}.hdf5"])
   
   
   
def create_new_environment(crater_params):
    """
    # TODO:
    """


    ####
    # Create a new crater, soil and blocks files

    # -IN- #            
   # crater_params; <list()>:   Specifications for the heightmap to create
    
    # -OUT- #
    # 
    ####
    
    
    # Change the crater_params file
    hf.write_CSV(["length", "width", "inclination", "resolution"], crater_params, "incline/crater_params.csv")
    
    # Create new heigthmap
    heightmap = subprocess.run(["python3", "incline/incline_generator.py", "-c incline/crater_params.csv"], capture_output=True)
    
    print("errors: ", heightmap.stderr)
    
    heightmap_file = heightmap.stdout.decode('utf-8')
    
    
        # Create corresponding blocks and soil files
    soil_blocks = subprocess.run(["python3",  "soil_blockStart_generator.py", f"-g {heightmap_file}", "-m incline/blocks_params.csv", "-n incline/soil_params.csv"], capture_output=True)
    
    
    print("errors: ", soil_blocks.stderr)
    soil_file, blocks_file = soil_blocks.stdout.decode('utf-8').split()[:]
    
    
    return heightmap_file, soil_file, blocks_file
    
    
    
def vary_inclination(soil_params):
    """
    # TODO:
    """


    ####
    # Vary the inclination

    # -IN- #            
    # soil_params; <list()>:   Specifications for soil type
   
    
    # -OUT- #
    ####


    # Create new directory
    subprocess.run(["mkdir", f"incline/inclination"])
    
    
    for alpha in range(0, 70, 10):
        heightmap_file, soil_file, blocks_file = create_new_environment([[1000, 100, alpha, 5]])
        
        
        run_simulation(soil_params, "incline/siconos_sphere.py", [f"-g {heightmap_file}", f"-s {soil_file}", f"-b {blocks_file}", "-m incline/blocks_params.csv"], output_name=f"incl_{alpha}")
        
        # Cleanup
        subprocess.run(["mv",  f"incline/incl_{alpha}.hdf5", f"incline/inclination/incl_{alpha}.hdf5"])

    
    
    
def vary_resolution(soil_params):
    """
    # TODO:
    """


    ####
    # Vary the resolution of the heightmap

    # -IN- #            
   
    
    # -OUT- #
    ####


    # Create new directory
    subprocess.run(["mkdir", f"incline/resolution"])
    
    #"""
    for res in range(1, 9, 1):
        heightmap_file, soil_file, blocks_file = create_new_environment([[1000, 100, 20, res]])
        
        
        run_simulation(soil_params, "incline/siconos_sphere.py", [f"-g {heightmap_file}", f"-s {soil_file}", f"-b {blocks_file}", "-m incline/blocks_params.csv"], output_name=f"res_{res}")
        
        # Cleanup
        subprocess.run(["mv",  f"incline/res_{res}.hdf5", f"incline/resolution/res_{res}.hdf5"])
        subprocess.run(["mv",  f"incline/soil_params_res_{res}.csv", f"incline/resolution/soil_params_res_{res}.csv"])
        
        print(f"sim res={res} done")
    #"""        
        
    for res in np.arange(4.1, 6.1, 0.1):
        res = res.round(1)
        
        if not res in (5.0, 6.0):
            
            heightmap_file, soil_file, blocks_file = create_new_environment([[1000, 100, 20, res]])
            
            
            run_simulation(soil_params, "incline/siconos_sphere.py", [f"-g {heightmap_file}", f"-s {soil_file}", f"-b {blocks_file}", "-m incline/blocks_params.csv"], output_name=f"res_{res}")
            
            # Cleanup
            subprocess.run(["mv",  f"incline/res_{res}.hdf5", f"incline/resolution/res_{res}.hdf5"])
            subprocess.run(["mv",  f"incline/soil_params_res_{res}.csv", f"incline/resolution/soil_params_res_{res}.csv"])
            
            print(f"sim res={res} done")
        
   
        
       
       
            
def test_bounces(soil_parameters):
    """
    # TODO:
    """


    ####
    # Run one parameter set number_runs times to check robustness
    ## dir and simulation file fixed!!

    # -IN- #    
    # soil_parameters; <list(<float>)>: All soil parameters (e, mu, mu_r) in list
   
    
    # -OUT- #
    ####

        
    lvls = []
    for e in np.arange(0, 1.1, 0.1):
        e = e.round(1)
        # level 10: best overall
        lvls.append([e, soil_parameters[1], soil_parameters[2]])

    run_simulation_levels(lvls, "incline/siconos_sphere.py", ["-g incline/incline_1000.0_100.0_20.0.asc", "-s incline/ellipse_soil_500.0_50.0_1.asc", "-b incline/ellipse_blocks_500.0_50.0_1.asc", "-m incline/blocks_params.csv"])

        # Cleanup
    # Create new directory
    subprocess.run(["mkdir", f"incline/bouncing"])
    
    # and move all files into the new directory
    for j, e in enumerate(np.arange(0, 1.1, 0.1)):
        e = e.round(1)
        subprocess.run(["mv",  f"incline/simulation_level_{j}.hdf5", f"incline/bouncing/e_{e}.hdf5"])
        
        
    
#########################################
################ MAIN ###################
######################################### 
   

# LHS for soil-params   
"""
### when is space filled good enough??!????
## confidence intervals for uniform distribution?
# -> Matala 2008 ?
## Space Filling Designs for Modeling & Simulation Validation - Heather Wojton ; Matala as well
# -> rule of thumb: 10x param number

## Matala table: 38 =^ 90%, 90% (at least for random sampling ?)

lvls = hf.create_levels(3, 38)

print(lvls)

# Run all simulations
run_simulation_levels(lvls, "incline/siconos_sphere.py", ["-g incline/incline_1000.0_100.0_20.0.asc", "-s incline/ellipse_soil_500.0_50.0_1.asc", "-b incline/ellipse_blocks_500.0_50.0_1.asc", "-m incline/blocks_params.csv"])
#"""

test_bounces([0.53134918, 0.0998972 , 0.88058602])










