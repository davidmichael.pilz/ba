


#
# Simple script to run all files/code necessary for preprocessing, simulation and analysis of rockfalls in one crater
#

import sys, os

# Only needed if helperfunctions in different directory
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))
import helperfunctions as hf

######################################################################
############################ MAIN ####################################
######################################################################


if __name__ == "__main__":

        # Paths and input name
    crater_name = 'Euclides_K'
    heightmap_path = 'data/Euclides_K/0_raw_data/SLDEM2015_512_30S_00S_315_360_FLOAT.IMG'
    LBL_path = 0
    features_path =  f'data/{crater_name}/0_raw_data/paths_and_shapes.geojson'
    rocks_path = None #'data/Euclides_K/rocks_params.csv'
    soil_path = 'data/Euclides_K/soil_params.csv'
    
    # Run simulation and everything necessary
    hf.run_scripts(crater_name, heightmap_path, LBL_path, features_path, soil_path, rocks_parameters=rocks_path)
    
    
    
    
    
    
    
    
    
