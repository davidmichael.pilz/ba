##########
# TODO: #
#   - read_cmd split to filepath retrieve (helperfunctions) and some manipulation?!!!
#
##########



##
## Create ESRI ASCII raster file (i.e. heightmap) for inclined plane from given parameters (length, width, inclination)
##


import numpy as np

import matplotlib.pyplot as plt


import sys, getopt, os
# needed for easy access to helperfunctions in different directory (../../shared_code)
sys.path.append(os.path.join(os.path.dirname(__file__), '../../shared_code'))
import helperfunctions as hf


def read_cmd():
    """
    # TODO:     
    """


    ####
    # Get input parameters (__ONE__ set) from commandline or file

    # -IN- #    
    # paramFilePath; <FILE_PATH>:   Path to parameters file (.csv) with __one__ parameter set (length, width, inclination (in degrees), resolution (length of one segment - assumed quadratic)) (CLA)
    #
    # plot; <bool>:                 Plot 3D height map via matplotlib UI (optional CLA) 
    # debug; <bool>:                Display debug info on stdout (optional CLA) 
    
    # -OUT- #
    # length, width, inclination, resolution, inPlot, indebug
    ####
        
        
    cmdOptions = hf.get_CMD_options("hdpc:", ["help", "debug", "plot",  "craterParams="], 
        ["help", "Print all debug messages", 
        "Plot 3D height map via matplotlib UI", 
        "Path to parameters file (.csv) with __one__ parameter set (length, width, inclination, resolution)"])


    # check IO interaction
    for j in range(3, len(cmdOptions)):
        if cmdOptions[j] is None:
            print("Not enough file paths given!")
            exit(-1)

    inDebug = cmdOptions[1]
    inPlot = cmdOptions[2]

    # Access parameters file and retrieve information
    ## if multiple sets specified -> only first used
    parameters = hf.read_CSV(cmdOptions[3].split()[0])[0]

    try:
        length = float(parameters["length"])
        width = float(parameters["width"])
        ## In degrees!
        inclination = float(parameters["inclination"])
        resolution = float(parameters["resolution"])
        
    except:
        print("  - An error occured while trying to load the input parameters.\n    Did you specify everything correctly?\n    Expected 1D shape: length, width, inclination, resolution")
        print("  - Error log:\n")
        print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")
        sys.exit(-1)


    return length, width, inclination, resolution, inPlot, inDebug
    
   
   
   
   
def create_incline(length, width, inclination, resolution):

    """
    #TODO:
        - where to put origin!?
    """

    ####
    # Create and return height-matrix

    # -IN- #    
    # length; <float>:      Length of inclined plane
    # width; <float>:       Width of inclined plane
    # inclination; <float>: Inclination of plane
    # resolution; <float>:  Length of the cell's sides
    
    # -OUT- #
    # mX; <np.array(<float>)>: X-axis grid (matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>: Y-axis grid (matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>: Z-axis grid / height grid (matrix), same shape as X and Y
    ####
    

    # Number of coordinate pairs i.e. segment resolution per axis 
    ## +1 due to implementation of heightmap in siconos: 
    ## X [Y] extension only (nX [nY] -1)*resolution as each grid point corresponds to one terrain junction ! (i.e. tl_corner == 0, 0 ; tr_corner == 0, -1 !)
    nX = int(np.ceil(length/resolution)) +1
    nY = int(np.ceil(width/resolution)) +1

    # Coordinate axis
    x_axis = np.linspace(0, length, nX)    
    y_axis = np.linspace(-width/2, width/2, nY)
        
    # Coordinate grid (adjust shapes)
    ## adjusted for siconos' coordinate system definition (x ^ y ->)
    mX, mY = np.meshgrid(x_axis, y_axis, indexing='ij')


    # Compute z for given inclination (measured from x-axis -> lower for greater x) 
    mZ = np.tan(inclination*np.pi/180) *np.flip(mX, axis=0)
    
    return mX, mY, mZ
   
   
      
   
def plot_crater(mX, mY, mZ):
    
    ####
    # Plot height-matrix / paraboloidic-like shaped crater (3D via matplotlib UI)

    # -IN- #    
    # mX; <np.array(<float>)>: X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>: Y-axis grid (2D matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>: Z-axis grid / height map (2D matrix), same shape as X and Y
    
    # -OUT- #
    # UI: matplotlib.pyplot.show()
    ####
    
    """
    # 2D option
    if dim == 2:
    
        # 2D color plot
        plt.xlabel("x")
        plt.ylabel("y")
        
        plt.pcolormesh(mX, mY, mZ)#, shading='gouraud')
        plt.colorbar(label="z")
        
        plt.show()


    elif dim == 3:
    """
    
    # 3D surface plot
    fig = plt.figure()

    ax = fig.add_subplot(projection='3d')

    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
                        
    ax.plot_surface(mX, mY, mZ)
    
    hf.set_axes_equal(ax)
    #ax.plot_wireframe(mX, mY, z)
    
    plt.show()
   
    return
   
   



######################################################################
############################ MAIN ####################################
######################################################################

   

# Get semimajor, semiminor, maxDepth, resolution, plot, debug from Commandline Interface (CLI)
l, w, incl, res, plot, debug = read_cmd()

# Create height-matrix; Shape: inclined plane, z: height, x: length, y: width
x, y, z = create_incline(l, w, incl, res)

# Plot crater (UI)
if plot: plot_crater(x, y, z)

# Write ESRI ASCII file; Output the filename
sys.stdout.write(hf.write_ESRI_ASCII(z, res, filename='incline_%.1f_%.1f_%.1f.asc' % (l, w, incl)))





