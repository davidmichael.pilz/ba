########
# TODO:
#   - DOCU!!! 
##########


##
##  Visualize results (3D Plot)
##



import numpy as np

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../shared_code'))

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import helperfunctions as hf

#np.set_printoptions(threshold=sys.maxsize)

    
    
    
#########################################
################ MAIN ###################
######################################### 


if __name__ == "__main__":



    cmdOptions = hf.get_CMD_options("hdg:b:s:m:n:t:", ["help", "debug", "grid=", "blocks=", "soil=", "blocksParams=", "soilParams=", 'trajectories='], 
            ["help", "Print all debug messages",
            "Path to heightmap (ESRI ASCII (.asc))", 
            "Path to blocks starting position grid (ESRI ASCII (.asc))",
            "Path to soil grid (ESRI ASCII (.asc))",  
            "Path to soil parameters file (.csv); Expected: (e, mu, mu_r)", 
            "Path to sphere parameters file (.csv); Expected: (v_min, v_max)", 
            "Path to trajectories file (.hdf) - Common output file of simulation"])    



    # check IO interaction
    for j in range(2, len(cmdOptions)):
        if cmdOptions[j] is None:
            print("Not enough file paths given!")
            exit(-1)
        else:
            cmdOptions[j] = cmdOptions[j].strip()



            # Load heightmap
    mX, mY, mZ, misc  = hf.read_ESRI_ASCII(cmdOptions[2])



            # Block departure zones

    mBlocks = hf.read_ESRI_ASCII(cmdOptions[3])[2]


            # Soil zones
            
    mSoil = hf.read_ESRI_ASCII(cmdOptions[4])[2]


            # Soil data
            
    soilParams = hf.read_CSV(cmdOptions[6])
           
           
            # Block starting params           
            
    blocksParams = hf.read_CSV(cmdOptions[5])



        # Extract trajectories
    with h5py.File(cmdOptions[7], "r") as hdf5File:
        
        # t, ID, x, y, z  #, orientation (quatruple)
        ## x ^, y ->
        blockTrajectories = np.asarray(hdf5File["data"]["dynamic"])[:, :5]
        
        
    
    # Plot heigth map, block positions, soil types and trajectories (UI)
    hf.visualize(mX, mY, mZ, (blocksParams, mBlocks), (soilParams, mSoil), blockTrajectories)

    # Further analysis of results?
    #hf.plot_results(params, blockAttr=block_properties, t_analysis=(None, int(0.25/1e-3)))





      
      
      


