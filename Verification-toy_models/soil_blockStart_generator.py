##########
# TODO: #
#
#   - BUG: somehow the block start position is only _mostly_ working ?! -> sometimes it fails? 
#       --> not that relevant for our case, but keep in mind!

#   - modularisierung split ellipse!! 
#   - blockstartingpositions not all _inside_ ellipse?!??!
#
#   - not only arclength, optionally equsized angles
#   - Einheiten ?
##########


##
##  Create ESRI ASCII file for soil parameters and block starting positions from original heightmap (.asc (grid)) and given parameters (.csv (parameters))
##



import numpy as np
from scipy import special 

import getopt

import matplotlib.pyplot as plt


import sys, os

# Only needed if helperfunctions in different directory
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))

import helperfunctions as hf
np.set_printoptions(threshold=sys.maxsize)


def read_cmd():

    """
    # TODO:
        - safety-check !?
    """


    ####
    # Load heightmap as well as soil and blocks parameters from files

    # -IN- #    
    # gridFilePath; <FILE_PATH>:    Path to heightmap (ESRI ASCII (.asc)) (CLA)
    # soilFilePath; <FILE_PATH>:    Path to soil parameters file (.csv); Expected: (e, mu, mu_r) (CLA)
    # blocksFilePath; <FILE_PATH>:  Path to blocks parameters file (.csv); Expected: (number_blocks, height_fall_min, height_fall_max, number_points_hull, v_min, v_max) (CLA)
    
    # plot; <bool>:                 Plot 3D soil separation and blocks starting position via matplotlib UI (optional CLA) 
    # debug; <bool>:                Display debug info on stdout (optional CLA) 
    
    # -OUT- #
    # mX; <np.array(<float>)>:                  X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>:                  Y-axis grid (2D matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>:                  Z-axis grid (2D matrix ; heightmap), shaped like mX; mY ; (only needed for plotting)
    # (maxX, maxY,                              Miscellaneous values: maxX/maxY: max(x/y_axis); 
    #   cellsize,                               'resolution' of heightmap;
    #   nSoilTypes); <tuple(<float>)>:          number of different soil types
    
    # soilParameters; <np.array(<float>)>:      All input soil parameters (only needed for plotting)
    # blocksParameters; <np.array(<float>)>:    All input blocks parameters (m-n relation, m<=n (<: for m->n: default to last parameter set) for n: numberSoilTypes) (mostly needed for plotting)
    
    # inPlot
    # inDebug
    ####
        
        
    cmdOptions = hf.get_CMD_options("hdpg:m:n:", ["help", "debug", "plot", "grid=", "blocksParams=", "soilParams="], 
        ["help", "Print all debug messages", "Plot 3D soil separation and blocks starting position via matplotlib UI",
        "Path to heightmap (ESRI ASCII (.asc))", 
        "Path to soil parameters file (.csv); Expected: (e, mu, mu_r)", 
        "Path to blocks parameters file (.csv); Expected: (number_blocks, height_fall_min, height_fall_max, number_points_hull, v_min, v_max)"])        
       
    inDebug = cmdOptions[1]
    inPlot = cmdOptions[2]
       
    # check IO interaction
    for j in range(3, len(cmdOptions)):
        if cmdOptions[j] is None:
            print("Not enough file paths given!")
            exit(-1)
   
   
    # load heightmap
    mX, mY, mZ, miscDict = hf.read_ESRI_ASCII(cmdOptions[3].split()[0])
    
    maxX, maxY = miscDict[1], miscDict[2]
    miscDict = miscDict[0]
    
    
    # load parameters
    blocksParameters = hf.read_CSV(cmdOptions[4].split()[0])
    soilParameters = hf.read_CSV(cmdOptions[5].split()[0])


    # safety-check for number of parameters
    ### in extra function?? -> helperfunction?
    ## rather ValueError than AssertionError ?
    try:
        nSoilTypes = len(soilParameters)        
        nSoilParams = len(soilParameters[0])
        
        # Expected: (e, mu, mu_r)
        if nSoilParams != 3:
            raise AssertionError("Not enough or too many parameters given. Expected 3, got " + str(nSoilParams) + "!")
        
    except:    
        print("  - An error occured while trying to load the soil parameters.\n    Did you specify everything correctly?\n    Expected shape: e, mu, mu_r")
        print("  - Error log:\n")
        print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")
        sys.exit(-1)
    
        
    try:
        nBlockParams = len(blocksParameters[0])
        
        # Expected: (number_blocks, height_fall_min, height_fall_max, number_points_hull, v_min, v_max)
        if nBlockParams != 6:
            raise AssertionError("Not enough or too many parameters given. Expected 6, got " + str(nBlockParams) + "!")
        
    except:    
        print("  - An error occured while trying to load the blocks parameters.\n    Did you specify everything correctly?\n    Expected shape: (number_blocks, height_fall_min, height_fall_max, number_points_hull, v_min, v_max")
        print("  - Error log:\n")
        print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")
        sys.exit(-1)
    
            
    return mX, mY, mZ, (maxX, maxY, float(miscDict["cellsize"]), nSoilTypes), soilParameters, blocksParameters, inPlot, inDebug
    
    
    
      
    

def equidistant_elliptic_positions(semimajor, semiminor, number, precision=1e-3):
    
    """
    # TODO: 
    #   - better root search!
    """
    
    ####
    # Compute and return best (always a bit over-) estimated angles to equidistant (in terms of arclength) positions on a given ellipse
    # (ref. https://math.stackexchange.com/questions/172766/calculating-equidistant-points-around-an-ellipse-arc)
    ## clone @ crater_block_generator.py !  //PlatRock//

    # -IN- #    
    # semimajor; <float>:   Semimajor of elliptic opening
    # semiminor; <float>:   Semiminor of elliptic opening
    # number; <int>:        Number of starting points
    
    # precision; <float>:   Precision of computed angles [rad] ; default: 1e-3
    
    # -OUT- #
    # angles; <np.array(<float>)>: All computed angles for equidistant points on the given ellipse [rad]; Starting at 0 == (semimajor, 0) anti-clockwise
    ####
    
    
    # Compute the total circumference of the ellipse 
    m = 1 - semiminor*semiminor/(semimajor*semimajor)
    circumference = 4 * semimajor * special.ellipe(m)
    
    
    # Compute the respective angle for each point
    
    # For k=0 : phi=0
    phi = 0
    angles = [0]
       
    
    # Compute best angle phi for each point k > 0
    for k in range(1, number):
    
        # k * circumference/amount =!= semimajor * E(phi, m)
        intTarget = k * circumference/number /semimajor
        
        ### Improve root search!!!
        while(intTarget - special.ellipeinc(phi, m) > 0):
            phi += precision
                    
        ## will always be larger than perfect value
        angles += [phi]
        
        
    return np.array(angles)
    
    
   
  
  
def split_ellipse(mX, mY, maxX, maxY, cellsize, number, blocksParameters):
    
    """
    # TODO:
        - choice of angles for 2 angles changes size of start patch?!?!??!?!??!!!!!!!
        
        - Modularisierung!!?
        - block fails unsave for large patches?! -> index out of range..?
        - precision for point search?!
    """
    
    
    ####
    # Split grid into equal slices and add (max distanced) starting positions for blocks in each slice

    # -IN- #    
    # mX; <np.array(<float>)>:                  X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>:                  Y-axis grid (2D matrix), shape adjusted for X-axis
    # number; <float>:                          number: Number of different soil types
    # blocksParameters; <np.array(<float>)>:    All input blocks parameters (m-n relation, m<=n (<: for m->n: default to last parameter set) for n: numberSoilTypes)
    
    # -OUT- #
    # soilGrid; <np.array(<float>)>:                X-Y-Grid with soil type (aka. corresponding index _+1_!) as values
    # blockStartingPositions; <np.array(<float>)>:  X-Y-Grid with default 0 and blocks type (aka. corresponding index _+1_!) at (quadratic) starting patches as values
    ####
    
    semimajor = maxX
    semiminor = maxY

    ## override default value of 1e-3
    #anglePrecision = 1e-3
      
    
    # Slice ellipse via equidistant 'slicing-points'
    angles = equidistant_elliptic_positions(semimajor, semiminor, number)
    angles = np.append(angles, np.array([2*np.pi]))
    
    # If there is only one soil type -> only two angles in angles --> we want to maximize the runout length aka. we want to roll from angle 0, not hlafway between 0 and 2pi!
    ### chagnes size of starting psoition?!?!?
    if len(angles) == 2: angles = np.array([-1e1, 1e1])


    # Soil grid
    soilGrid = np.zeros(mX.shape).astype(int)
    # 'Polar mask'
    polarSoilGrid = np.zeros(soilGrid.shape).tolist()
    
    # Block grid
    blockStartingPositions = np.zeros(soilGrid.shape).astype(int)
    
    
    ### exists a more computational efficient way?... https://stackoverflow.com/questions/13652518/efficiently-find-points-inside-a-circle-sector
    # Build polar mask: compute polar coordinates for each point ('polar/angle mask')
    
    maxj = soilGrid.shape[0] - 1
    maxk = soilGrid.shape[1] - 1
    
    
    #### modularize!!!
    for j, rows in enumerate(soilGrid):
        
        # Map matrix grid to coordinate grid (origin centered in matrix); absolute value: *semiminor _BUT_ this cancels out in y/r
        x = (1 - j * 2/maxj)
        
        for k, _ in enumerate(rows):
                
            y = (k * 2/maxk - 1)
            r = np.sqrt(x**2 + y**2)
            
            # arccos only gives values in [0, pi]
            ### (np.arcsin(y/r) + np.arccos(x/r))/2 for better numerical stability [gotta watch out with arcsin definition!] (but kinda works like this as well)
            ## siconos' coordinate system definition used!
            ## we want 0° at (semimajor, 0) !!
            if y > 0:
                polarSoilGrid[j][k] = 2*np.pi - np.arccos(x/r)
            else:
                polarSoilGrid[j][k] = np.arccos(x/r)

     
    # Set soil value for each grid point
    for a, angle in enumerate(angles[:-1]):
        for j, rows in enumerate(polarSoilGrid):
            for k, gridAngle in enumerate(rows):
                if gridAngle > angle and gridAngle < angles[a+1]:
                    ### +1 to detect if field hasn't been set at all (aka. 0); changeable!
                    soilGrid[j, k] = a + 1
           
               

    #### modularize!!!
    # quadratic and uneven (will 'round' up)
    size = 1#9
    
    # Starting position (assumed) quadratic
    cornerOffset = int(np.sqrt(size)/2)
        
    for a, angle in enumerate(angles[:-1]):
            
        # Get all corresponding indizes for this soil type
        indizes = np.argwhere(soilGrid == a+1)
                
        # Compare distances measured from origin (center of matrix)
        distances = []
        
        for soilInd in indizes: 
                
            # Create block starting position halfway inside soil patch
            ### magic precision number!!
            if abs(polarSoilGrid[soilInd[0]][soilInd[1]] - (angle + angles[a+1])/2) < 1e-1:
                
                # Map matrix grid to coordinate grid (origin centered in matrix); absolute value: *semiminor _BUT_ not needed
                x = (1 - soilInd[0] * 2/maxj)
                y = (soilInd[1] * 2/maxk - 1)

                # We only want starting positions _inside_ of the paraboloid!
                ### Well, now only middle point inside...!!!
                if x*x + y*y - 1 < 0:
                
                    # check if all corners (tl, lr, ll, tr) are inside the soil patch (i.e. in indizes)
                    if all(ind in indizes for ind in [soilInd - [cornerOffset, cornerOffset], soilInd + [cornerOffset, cornerOffset], soilInd + [cornerOffset, -cornerOffset], soilInd + [-cornerOffset, cornerOffset]]):
                        ### -> setting grid fails for large patch sizes?!!!?!? 
                        r = np.sqrt(x**2 + y**2)
                        distances += [[r, *soilInd]]
                    
                                
        if distances == []:   
            pass             
            ### TODO!! Change back
            #print(" - Could not find suitable block starting position as probably size seems to be too big for the soil patch #" + str(a+1) + " !")
        else:
            
            distances = np.array(distances)
            
            # Blocks should travel the longest distance -> starting position as far away from the origin as possible!                       
            maxDistIndex = np.argmax(distances[:, 0])
            startInd = distances[maxDistIndex, 1:].astype(int)
            
            nBlocksParams = len(blocksParameters)
            
            # Create starting position patch
            for j in range(-cornerOffset, cornerOffset+1):
                for k in range(-cornerOffset, cornerOffset+1):
                    if a in range(nBlocksParams):
                        ### +1 to detect if field hasn't been set at all (aka. 0); changeable!
                        blockStartingPositions[startInd[0]+j, startInd[1]+k] = a + 1
                    else:
                        ### +1 to detect if field hasn't been set at all (aka. 0); changeable!
                        # If no further params given -> default to last
                        blockStartingPositions[startInd[0]+j, startInd[1]+k] = nBlocksParams

   
    return soilGrid, blockStartingPositions
   
   
      
   
   
   

######################################################################
############################ MAIN ####################################
######################################################################


"""
soil_boulder_ring = []

for j in range(100):
    soil_boulder_ring.append([0.5313491830696339, 0.0998971996307547, 0.8805860231995135])

#hf.write_CSV(["e", "mu", "mu_r"], soil_boulder_ring, f"soil_params.csv")
"""

# Get x grid, y grid, misc (maxX, maxY, cellsize, nSoilTypes), heightmap, parameters, plot, debug from Commandline Interface (CLI)
x, y, z, misc, soilParams, blocksParams, plot, debug = read_cmd()


# Map grid points to parameters
#### right now: ring of 100 boulders around crater opening ; WATCHOUT: ignore soil file! (it will work, but has a lot of overlap..)
#soil_map, blocks_starting_map = split_ellipse(x, y, *misc[:-1], 100, blocksParams)
soil_map, blocks_starting_map = split_ellipse(x, y, *misc, blocksParams)


# Plot soil types and block positions (UI)
if plot: 
    hf.plot_start_setting(x, y, z, (blocksParams, blocks_starting_map), (soilParams, soil_map))
    plt.show()

# Write .asc
soilFile = hf.write_ESRI_ASCII(soil_map, misc[2], filename=f'ellipse_soil_{misc[0]}_{misc[1]}_{misc[-1]}.asc')
blocksFile = hf.write_ESRI_ASCII(blocks_starting_map, misc[2], filename="ellipse_blocks_"+str(misc[0])+"_"+str(misc[1])+"_"+str(misc[-1])+".asc")

sys.stdout.write(soilFile + " " + blocksFile)







