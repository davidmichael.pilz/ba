
#
# Simple script to run all files/code necessary for preprocessing, simulation and analysis of rockfalls in one crater
#

import sys, os

# Only needed if helperfunctions in different directory
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))
import helperfunctions as hf

######################################################################
############################ MAIN ####################################
######################################################################


if __name__ == "__main__":

        # Paths and input name
    crater_name = 'Atlas_East'
    heightmap_path = f'data/{crater_name}/0_raw_data/NAC_DTM_ATLAS1_E473N0448.IMG'
    LBL_path = f'data/{crater_name}/0_raw_data/NAC_DTM_ATLAS1.LBL'
    features_path =  f'data/{crater_name}/0_raw_data/paths_and_shapes.geojson'
    rocks_path = None
    soil_path = f'data/{crater_name}/soil_params.csv'
    
    # Run simulation and everything necessary
    hf.run_scripts(crater_name, heightmap_path, LBL_path, features_path, soil_path, rocks_parameters=rocks_path)
    
    
    
    
    
    
    
    
    
