########
# TODO:
#   - DOCU!!! 
##########


##
##  Visualize results (3D Plot)
##



import numpy as np

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../shared_code'))

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import helperfunctions as hf

#np.set_printoptions(threshold=sys.maxsize)

import subprocess
    
    
#########################################
################ MAIN ###################
######################################### 


if __name__ == "__main__":

    crater_params = 'data/0_raw_data/crater_params.csv'
    blocks_params = 'data/0_raw_data/blocks_params.csv'
    soil_params = 'data/0_raw_data/soil_params_3.csv'
    
       
    # 1_preprocessing
    print('0_crater_generator')

    crater_generator = subprocess.run(['python3', '0_crater_generator.py', f'-c {crater_params}'], capture_output=True)

    # Catch errors
    errors = crater_generator.stderr
    if not errors == b'':
        print(crater_generator.stdout.decode('utf-8'))
        print(crater_generator.stderr)

    map_file_path = crater_generator.stdout.decode('utf-8').strip()

        # Move output file (map_file_name)
    subprocess.run(['mv',  map_file_path, f'data/1_input/{map_file_path}'])
        
    map_file_path = f'data/1_input/{map_file_path}'
        
        
       
    # soil_blockStart_generator
    print('1 soil_blockStart_generator')

    soil_blockStart_generator = subprocess.run(['python3', '../soil_blockStart_generator.py', f'-g {map_file_path}', f'-m {blocks_params}', f'-n {soil_params}'], capture_output=True)

    # Catch errors
    errors = soil_blockStart_generator.stderr
    if not errors == b'':
        print(soil_blockStart_generator.stdout.decode('utf-8'))
        print(soil_blockStart_generator.stderr)

    soil_file_path, blocks_file_path = soil_blockStart_generator.stdout.decode('utf-8').split()[:2]

        # Move output file (soil_file_path, blocks_file_path)
    subprocess.run(['mv',  soil_file_path, f'data/1_input/{soil_file_path}'])
    subprocess.run(['mv',  blocks_file_path, f'data/1_input/{blocks_file_path}'])
        
    soil_file_path = f'data/1_input/{soil_file_path}'
    blocks_file_path = f'data/1_input/{blocks_file_path}'
    
    
        
        # 2_simulation
    print('2_simulation')
    
    ## positions <-> siconos.geojson!
    simulation = subprocess.run(['python3', '2_simulation.py', f'-g {map_file_path}', f'-s {soil_file_path}', f'-b {blocks_file_path}', f'-m {blocks_params}', f'-n {soil_params}'], capture_output=True)    
    
    
    # Catch errors
    errors = simulation.stderr
    if not errors == b'':
        print(simulation.stdout.decode('utf-8'))
        print(simulation.stderr)
    
    #"""
        # Move output file
    subprocess.run(['mv',  f'2_simulation.hdf5', 'data/2_simulation/crater_simulation.hdf5'])
    
    simulation_file_path = 'data/2_simulation/crater_simulation.hdf5'
    
    
    #"""
        # 3_analysis
    print('3_analysis')
    
    analysis = subprocess.run(['python3', '3_analysis.py', f'-g {map_file_path}', f'-s {soil_file_path}', f'-b {blocks_file_path}', f'-m {blocks_params}', f'-n {soil_params}', f'-t {simulation_file_path}'], capture_output=True)
    
    # Catch errors
    errors = analysis.stderr
    if not errors == b'':
        print(analysis.stdout.decode('utf-8'))
        print(analysis.stderr)
    
    '''
        # Move output file
    subprocess.run(['mv',  f'{simulation_name}_analysis.json', f'data/{crater_name}/3_analysis/{simulation_name}_analysis.json'])
    #"""
    '''
      
      


