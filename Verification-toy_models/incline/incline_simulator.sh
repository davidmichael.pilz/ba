#!/bin/bash

#
#TODO: maybe paramsfilepaths as cmd option or similar?
#   -error handling! 
#   - automatic file naming adjustment
#

echo "Generating inclined plane from 'crater_params.csv'"

craterFile=$(python3 incline_generator.py -c crater_params.csv)

printf "...done (Output: ${craterFile})\n"

printf "\nPress Enter to continue to generating the soil map and block starting positions..\n"
read 
## https://stackoverflow.com/questions/6726782/how-to-clear-out-std-and-transfer-stderr-to-std
#2>&3

printf "Generating underlying soil map.\nGenerating sphere starting positions, parameters: 'blocks_params.csv'\n"
printf "\n#####\nHere. the logic originally intended for ellipsoids with an elliptic opening is used. Hence the output files' naming convention (ellipse_<type>_<semimajor>_<semiminor>_<number_of_patches>.asc) differs slightly from the one used above to generate the underlying heightmap (inline_<length>_<width>_<inclination>.asc) - just as a heads up.\n\t - No additional actions required. - \n#####\n"

# Output is 'soilFilePath blocksFilePath'
files=$(python3 ../soil_blockStart_generator.py -g $craterFile -m blocks_params.csv -n soil_params.csv)

### https://www.geeksforgeeks.org/bash-scripting-split-string/
## Somehow inclineFile got captured as well (sepereated by \n)
readarray -d $'\n' array <<< "$files"
readarray -d " " array <<< "${array[1]}"

soilFile="${array[0]}"
## some weird trailing chars.. could do some of that: https://stackoverflow.com/questions/28080743/bash-how-to-only-keep-specific-characters-from-a-string or https://www.unix.com/shell-programming-and-scripting/31202-strip-all-non-alphanumerics.html , but works anyway
blocksFile="${array[1]}"


printf "\n...done (Output: ${soilFile} ${blocksFile})\n"


printf "\nPress Enter to continue to siconos simulation..\n"
read 


printf "Running siconos simulation: \n\n"


#python3 siconos_sphere.py -g $craterFile -b ellipse_blocks_501.75_51.75_1.asc -s ellipse_soil_501.75_51.75_1.asc -m blocks_params.csv -n soil_params.csv
python3 siconos_sphere.py -g $craterFile -b $blocksFile -s $soilFile -m blocks_params.csv -n soil_params.csv
#python3 siconos_cuboid.py -g $craterFile -b $blocksFile -s $soilFile -m blocks_params.csv -n soil_params.csv

printf "...done (Output: 'siconos_sphere.hdf5')\n"


printf "\nPress Enter to continue to plotting..\n"
read 


echo "Plotting results"

python3 ../plot_results.py -g $craterFile -b $blocksFile -s $soilFile -m blocks_params.csv -n soil_params.csv -t siconos_sphere.hdf5
#python3 ../plot_results.py -g $craterFile -b $blocksFile -s $soilFile -m blocks_params.csv -n soil_params.csv -t siconos_cuboid.hdf5

printf "\nPress Enter to continue to analysis..\n"
read 

echo "Analysing results"

python3 analysis.py -t siconos_sphere.hdf5 -n soil_params.csv
#python3 analysis.py -t siconos_cuboid.hdf5 -n $soilFile

printf "...terminated successfully.\n"
