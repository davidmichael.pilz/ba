########
# TODO:
#   - DOCU!!! 
##########


##
##  Analyse results and compare with analytical solution
##



import numpy as np

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../shared_code'))

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import helperfunctions as hf

#np.set_printoptions(threshold=sys.maxsize)

  
######################################################################
########################## FUNCTIONS #################################
######################################################################

def read_cmd():

    """
    # TODO: 
        - DOCU!
        - what is in the hdf5 ?!
        - HARDCODED soil/blocksparams atm!!!!
    """

    ####
    # Get input parameters from cmd

    # -IN- #    
    # trajectoriesFilePath; <FILE_PATH>:    Path to .hdf5 file where ["data"]["dynamic"] contains the trajectory and ["data"]["velocity"] the velocities
    # debug; <bool>:                        Display debug info on stdout; default: False (optional CLA) 
    
    # -OUT- #
    # mX; <np.array(<float>)>:                          X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>:                          Y-axis grid (2D matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>:                          Z-axis grid (2D matrix), shape adjusted for X-axis
    # blocksParameters; <list(<dic>)>:                  All input blocks parameters
    # blocksPolygons; <list(<list(<list(<float>)>)>)>:  List with closed polygon paths (list of vertices ([x, y])) for the blocks starting positions
    # soilParameters; <list(<dic>)>:                    All input soil parameters
    # soilPolygons; <list(<list(<list(<float>)>)>)>:    List with closed polygon paths (list of vertices ([x, y])) for the soil types
    # trajectories; <np.array(<float>)>:                3D array containing every block trajectory (i.e. contact point) _INCLUDING_ (-9999, -9999, -9999) at [0] !
    # inDebug
    ####
        

    cmdOptions = hf.get_CMD_options("hdn:t:", ["help", "debug", "soilParams=", "trajectories="], 
        ["help", "Print all debug messages", 
        "soilFilePath; <FILE_PATH>:    Path to soil parameters file (.csv); Expected: (e, mu, mu_r) (CLA)",
        "trajectoriesFilePath; <FILE_PATH>:  Path to .hdf5 file where ['data']['dynamic'] contains the trajectories and ['data']['velocity'] the velocities"])
    
    inDebug = cmdOptions[1]
    
    for j in range(2, len(cmdOptions)):
        if cmdOptions[j] is None:
            print("Not enough file paths given!")
            exit(-1)
            
    # soilParams
    soilParamsFilePath = cmdOptions[2].strip()
            
    # trajectories
    trajectoriesFilePath = cmdOptions[3].strip()
    
            
        # Access files and retrieve information    
    
    # Soil (x, y, type)
    soilParameters = hf.read_CSV(soilParamsFilePath)
    
    # Blocks (type)
    blockParameters = None#hf.read_CSV("blocks_params.csv")
    
    
    # Block trajectories (x, y, z)
    try:
    
        with h5py.File(trajectoriesFilePath, "r") as hdf5File:
            
            # t, ID, x, y, z, orientation (quatruple)
            ## x ^, y ->
            blockTrajectories = hdf5File["data"]["dynamic"]
            
            try:
                mZ = np.array(hdf5File["data"]["ref"]["Height_map"])
            except KeyError:
                mZ = np.array(hdf5File["data"]["ref"]["MyTerrain_0"])
            
                        
            nBlocks = int(np.amax(blockTrajectories[:, 1]))
            blockTrajectories = np.array(blockTrajectories[:, :5])
            
            # t, ID, vx, vy, vz, omega
            blockVelocities = hdf5File["data"]["velocities"]
            blockVelocities = np.array(blockVelocities[:, :])
            
            """
            for n in range(nBlocks):
                blockString = "block" + str(n) + "-shape"
                
                blockShapes.append(np.array(hdf5File["data"]["ref"][blockString][:]))
            """
            
    except:
        print("  - An error occured while trying to access the input block trajectories' filepath:")
        print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")  
        
      
    ## possible (Assumption: inclined _plane_):
    resolution = 5
    inclination = np.arctan(abs((np.max(mZ)-np.min(mZ))/(mZ.shape[0]*resolution)))   
    inclination = round(inclination*180/np.pi, 2)

    return (blockParameters, soilParameters), (blockTrajectories, blockVelocities), inclination, inDebug
    
    
    
  
######################################################################
############################ MAIN ####################################
######################################################################


# Get parameters, trajectories, debug from Commandline Interface (CLI)
params, block_properties, inclination, debug = read_cmd()

###### TOCHANGE!!!!!
plotStarting = 0
if plotStarting: hf.plot_start_setting(*grid, blockStarting, soil)


# Plot heigth map, block positions, soil types and trajectories (UI)

inclination = 50

hf.plot_results(params, block_properties, incline=inclination)




  
  
  


