########
# TODO:
#
#
##########


##
##  (Rudimantary) Tests for various preprocessing functions used in other scripts
##



import numpy as np

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import sys, os

# only needed if helperfunctions one directory above this file
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(os.path.join(os.path.dirname(__file__), '../LROC-LOLA/'))
import helperfunctions as hf
import preprocess_DTM_features as preprocess

#np.set_printoptions(threshold=sys.maxsize)

from osgeo import ogr


def map_features_to_siconos_test():
    print('\n map_features_to_siconos_test():\n')
    
    print('\nList (e.g. path) [[-5, -5], [5, -5], [-5, 5]]:')
    
    coordinates = {'coordinates':[[0, 0], [10, 0], [0, 10]]}
    geometry = {'geometry':coordinates}
    coordinates1 = {'coordinates':[[0, 0], [10, 0], [0, 10]]}
    geometry1 = {'geometry':coordinates1}
    feature = {'features':(geometry, geometry1)}
    
    print(preprocess.map_features_to_siconos(feature, (0, 0), 1, (11, 11), 1))
    
    
    print('\nSingle Point (e.g. end_position) [[5, -5] ; [0, -5]]:')
    
    coordinates = {'coordinates':[0, 0]}
    geometry = {'geometry':coordinates}
    coordinates1 = {'coordinates':[-5, 0]}
    geometry1 = {'geometry':coordinates1}
    feature = {'features':(geometry, geometry1)}
    
    print(preprocess.map_features_to_siconos(feature, (0, -10), 1, (11, 11), 1))
    
    return 0
    
    
    
    
##################################################################################################

# Need to comment the MAIN() part [not coded properly unfortunately..]    
map_features_to_siconos_test() ## Passed





