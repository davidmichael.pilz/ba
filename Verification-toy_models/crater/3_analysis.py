########
# TODO:
#   - DOCU!!! 
##########


##
##  Analyse results
##



import numpy as np

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../shared_code'))

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import helperfunctions as hf

#np.set_printoptions(threshold=sys.maxsize)


def read_cmd():

    """
    # TODO: 
        - DOCU!
        - what is in the hdf5 ?!
        - HARDCODED soil/blocksparams atm!!!!
    """

    ####
    # Get input parameters from cmd

    # -IN- #    
    # gridFilePath; <FILE_PATH>:        Path to ESRI ASCII grid file (.asc) (CLA)
    # blocksFilePath; <FILE_PATH>:      Path to blocks GEOJSON file (.geojson) (CLA)
    # soilFilePath; <FILE_PATH>:        Path to soil parameters file (.csv); Expected: (e, mu, mu_r) (CLA)
    # trajectoriesFilePath; <FILE_PATH>:  Path to .hdf5 file where ["data"]["dynamic"] contains the trajectory and ["data"]["velocity"] the velocities
    # debug; <bool>:                    Display debug info on stdout; default: False (optional CLA) 
    
    # -OUT- #
    # mX; <np.array(<float>)>:                          X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>:                          Y-axis grid (2D matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>:                          Z-axis grid (2D matrix), shape adjusted for X-axis
    # blocksParameters; <list(<dic>)>:                  All input blocks parameters
    # blocksPolygons; <list(<list(<list(<float>)>)>)>:  List with closed polygon paths (list of vertices ([x, y])) for the blocks starting positions
    # soilParameters; <list(<dic>)>:                    All input soil parameters
    # soilPolygons; <list(<list(<list(<float>)>)>)>:    List with closed polygon paths (list of vertices ([x, y])) for the soil types
    # trajectories; <np.array(<float>)>:                3D array containing every block trajectory (i.e. contact point) _INCLUDING_ (-9999, -9999, -9999) at [0] !
    # inDebug
    ####
        

    cmdOptions = hf.get_CMD_options("hdn:t:", ["help", "debug", "soilParams=", "trajectories="], 
        ["help", "Print all debug messages", 
        "soilFilePath; <FILE_PATH>:    Path to soil parameters file (.csv); Expected: (e, mu, mu_r) (CLA)",
        "trajectoriesFilePath; <FILE_PATH>:  Path to .hdf5 file where ['data']['dynamic'] contains the trajectories and ['data']['velocity'] the velocities"])
    
    inDebug = cmdOptions[1]
    
    for j in range(2, len(cmdOptions)):
        if cmdOptions[j] is None:
            print("Not enough file paths given!")
            exit(-1)
            
            
    # soilParams
    soilParamsFilePath = cmdOptions[2]
            
    # trajectories
    trajectoriesFilePath = cmdOptions[3]
            
            
            
        # Access files and retrieve information    
    
    # Soil (type)
    soilParameters = hf.read_CSV(soilParamsFilePath)
    
    # Blocks (type)
    blockParameters = hf.read_CSV("blocks_params.csv")
    
    
    # Block trajectories (x, y, z)
    try:
    
        with h5py.File(trajectoriesFilePath, "r") as hdf5File:
            
            # t, ID, x, y, z, orientation (quatruple)
            ## x ^, y ->
            blockTrajectories = hdf5File["data"]["dynamic"]
            
            ### get keys!!!!! -> id for datafields!
            
            nBlocks = int(np.amax(blockTrajectories[:, 1]))
            blockTrajectories = np.array(blockTrajectories[:, :5])
            
            # t, ID, vx, vy, vz, omega
            blockVelocities = hdf5File["data"]["velocities"]
            blockVelocities = np.array(blockVelocities[:, :])
            
            
            mZ = np.array(hdf5File["data"]["ref"]["MyTerrain_0"])
            
            
    except:
        print("  - An error occured while trying to access the input block trajectories' filepath:")
        print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")  
        
      

    return (blockParameters, soilParameters), (blockTrajectories, blockVelocities), mZ, inDebug
    
    
    
    
#########################################
################ MAIN ###################
######################################### 


if __name__ == "__main__":


    # Get parameters, trajectories, debug from Commandline Interface (CLI)
    #params, block_properties, mZ, debug = read_cmd()


    cmdOptions = hf.get_CMD_options("hdg:b:s:m:n:t:", ["help", "debug", "grid=", "blocks=", "soil=", "blocksParams=", "soilParams=", 'trajectories='], 
            ["help", "Print all debug messages",
            "Path to heightmap (ESRI ASCII (.asc))", 
            "Path to blocks starting position grid (ESRI ASCII (.asc))",
            "Path to soil grid (ESRI ASCII (.asc))",  
            "Path to soil parameters file (.csv); Expected: (e, mu, mu_r)", 
            "Path to sphere parameters file (.csv); Expected: (v_min, v_max)", 
            "Path to trajectories file (.hdf) - Common output file of simulation"])    



    # check IO interaction
    for j in range(2, len(cmdOptions)):
        if cmdOptions[j] is None:
            print("Not enough file paths given!")
            exit(-1)
        else:
            cmdOptions[j] = cmdOptions[j].strip()



            # Load heightmap
    mX, mY, mZ, misc  = hf.read_ESRI_ASCII(cmdOptions[2])



            # Block departure zones

    mBlocks = hf.read_ESRI_ASCII(cmdOptions[3])[2]


            # Soil zones
            
    mSoil = hf.read_ESRI_ASCII(cmdOptions[4])[2]


            # Soil data
            
    soilParams = hf.read_CSV(cmdOptions[6])
           
           
            # Block starting params           
            
    blocksParams = hf.read_CSV(cmdOptions[5])



        # Extract trajectories
    with h5py.File(cmdOptions[7], "r") as hdf5File:
        
        # t, ID, x, y, z  #, orientation (quatruple)
        ## x ^, y ->
        blockTrajectories = np.asarray(hdf5File["data"]["dynamic"])[:, :5]
        
    visualize = 1
    if visualize: hf.visualize(mX, mY, mZ, (blocksParams, mBlocks), (soilParams, mSoil), blockTrajectories)

    # Plot heigth map, block positions, soil types and trajectories (UI)
    #hf.plot_results(params, blockAttr=block_properties, t_analysis=(None, int(0.25/1e-3)))





      
      
      


