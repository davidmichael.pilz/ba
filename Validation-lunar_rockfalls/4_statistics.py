########
# TODO:
#   - ordentlicher json dump!
#
##########


##
##  Analyse results of one .hdf5 with simulations for all detected boulders run simultaneously
##



import numpy as np

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import sys, os

# only needed if helperfunctions one directory above this file
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))
import helperfunctions as hf

#np.set_printoptions(threshold=sys.maxsize)


import json

######################################################################



def evaluate_dataset(statistics):
    """
    #TODO:
        - evaluation function??
        - Hypothesis test? --> maybe directly at analysis?
        - errors!?
    """

    ####
    # Evaluate how well the simulation results fit the observations
    
    # -IN- #    
    # statistics; <dict()>: Statistics for each parameter about the analysis of simulation results as returned by hf.describe_dataset()
    
    # -OUT- #
    # 
    ####


    return None




######################################################################
############################ MAIN ####################################
######################################################################



if __name__ == "__main__":


    cmdOptions = hf.get_CMD_options("hda:", ["help", "debug", 'analysis='], 
            ["help", "Print all debug messages",
            'Path to analysis results (.csv)'])

    # check IO interaction
    for j in range(2, len(cmdOptions)):
        if cmdOptions[j] is None:
            print('Not enough file paths given!')
            exit(-1)
        else:
            cmdOptions[j] = cmdOptions[j].strip()


    analysis_resultsFile = cmdOptions[2]
    # The file name is the string inbetween the last '/' of the path and the last '_' separating 'analysis' and the file type
    simulation_name = analysis_resultsFile.rsplit('/')[-1].rsplit('_', maxsplit=1)[0]
    
    analysis_results, parameters = hf.load_analysis(analysis_resultsFile)
        
    statistics = hf.describe_dataset(analysis_results)
    
        
        # Output statistics
    
    json_output = {}
    
    # Assemble correct json_output object for [parameter1: statistics1, parameter2: statistics2, ...]
    for p, parameter in enumerate(parameters):
    
        # Get values corresponding to this parameter
        parameter_statistics = {statistic: statistics[statistic][p] for statistic in statistics.keys() if not 'correlation' in statistic}
    
        json_output[parameter] = parameter_statistics
        
        
    json_output = [json_output]
    
    
    # Output statistics
    with open(f'{simulation_name}_statistics.json', 'w') as output_statistics_file:
        json.dump(json_output, output_statistics_file)






