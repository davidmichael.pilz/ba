##########
# TODO: #
#   - siconos definiton!!!!!!!!!!!!


#   - Input safe fail / Optional options!
#   - read_cmd split to filepath retrieve (helperfunctions) and some manipulation?!!!
#
#   - Einheiten ?
#   - gewünschte Tiefe ohne Formänderung! ??? vlt. aber auch wie jetzt als workflow besser
##########



##
## Create ESRI ASCII raster file (i.e. heightmap) for elliptic paraboloid from given parameters (semimajor, semiminor, maxDepth)
##


import numpy as np

import getopt
import sys, os

# Only needed if helperfunctions in different directory
sys.path.append(os.path.join(os.path.dirname(__file__), '../../shared_code'))


import matplotlib.pyplot as plt


import helperfunctions as hf


def read_cmd():

    ####
    # Get input parameters (__ONE__ set) from commandline or file

    # -IN- #    
    # paramFilePath; <FILE_PATH>:   Path to parameters file (.csv) with __one__ parameter set (semimajor, semiminor, maxDepth, resolution) (CLA)
    #                              #  or  #
    # semimajor; <float>:           Semimajor of elliptic opening (CLA)
    # semiminor; <float>:           Semiminor of elliptic opening  (CLA)
    # maxDepth; <float>:            Max depth of crater (changes paraboloidic shape if not 1!) (CLA)
    # resolution; <float>:          Minimal subdivision size on x-/y-axis, i.e. cellsize (ESRI ASCII) (CLA)
    #                              #  ##  #
    # plot; <bool>:                 Plot 3D height map via matplotlib UI (optional CLA) 
    # debug; <bool>:                Display debug info on stdout (optional CLA) 
    
    # -OUT- #
    # semiminor, semimajor, maxDepth, resolution, inPlot, indebug
    ####
        
        
    inDebug = 0
    inPlot = 0
    
    # Safety check
    importedParameters = 0


    # Input Command Line Arguments ## https://www.tutorialspoint.com/python/python_command_line_arguments.htm
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hdpc:j:i:z:r:",["help", "debug", "plot", "craterParams=", "semimajor=", "semiminor=", "maxDepth=", "resolution="])
    except getopt.GetoptError:
        print("Only '-h', '--help'; '-d', '--debug'; '-p', '--plot'; '-c', '--craterParams'; '-j', '--semimajor'; '-i', '--semiminor'; '-z', '--maxDepth'; '-r', '--resolution' allowed!")
        sys.exit(-1)


    for opt, arg in opts:

        # help -> display available CLA        
        if opt in ('-h', '--help'):
            print("crater_generator.py [opts:\t -h --help\t (help)\n\t\t -d --debug\t (Print all Debugs messages)\n\t\t -p --plot\t (Plot 3D height map via matplotlib UI)\n\t\t -c --craterParams\t (Path to parameters file (.csv) with __one__ parameter set (semimajor, semiminor, maxDepth, resolution))\n\t\t -j <float>, --semimajor <float> \t (Semimajor of elliptic opening)\n\t\t -i <float>, --semiminor <float> \t (Semiminor of elliptic opening)\n\t\t -z <float>, --maxDepth <float> \t (Max depth of crater)\n\t\t -r <float>, --resolution <float> \t (Minimal subdivision size on x-/y-axis, thus sets the segment area (ESRI ASCII) to resolution**2)\n\t ]")
            sys.exit()

        # debug
        elif opt in ("-d", "--debug"):
            inDebug = 1

        # plot
        elif opt in ("-p", "--plot"):
            inPlot = 1
            
        # file
        elif opt in ('-c', '--craterParams'):
            paramFilePath = arg
                
            # Access parameters file and retrieve information
            parameters = hf.read_CSV(paramFilePath)[0]
                        
            try:
                semimajor = float(parameters["semimajor"])
                semiminor = float(parameters["semiminor"])
                maxDepth = float(parameters["maxDepth"])
                resolution = float(parameters["resolution"])
                
            except:
                print("  - An error occured while trying to load the input parameters.\n    Did you specify everything correctly?\n    Expected 1D shape: semimajor, semiminor, maxDepth, resolution")
                print("  - Error log:\n")
                print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")
                sys.exit(-1)
            
            
            # Everything imported correctly
            importedParameters = 4

        # or #            
            
        # semimajor
        elif opt in ("-j", "--semimajor"):
            semimajor = float(arg)
            importedParameters += 1
            
        # semiminor
        elif opt in ("-i", "--semiminor"):
            semiminor = float(arg)
            importedParameters += 1
            
        # maxDepth
        elif opt in ("-z", "--maxDepth"):
            maxDepth = float(arg)
            importedParameters += 1
            
        # resolution
        elif opt in ('-r', '--resolution'):
            resolution = float(arg)
            importedParameters += 1
            
            
    if importedParameters < 4:
        print("  - An error occured while trying to load the input parameters.\n Did you specify everything correctly?\n For available CLA see 'python3 crater_generator.py -h'")    
        sys.exit(-1)

    return semimajor, semiminor, maxDepth, resolution, inPlot, inDebug
    
   
   
   
def create_paraboloid(semimajor, semiminor, maxDepth, resolution):

    """
    #TODO: better computational way for leveling 
    """

    ####
    # Create and return height-matrix

    # -IN- #    
    # semimajor; <float>:   Semimajor of elliptic opening 
    # semiminor; <float>:   Semiminor of elliptic opening 
    # maxDepth; <float>:    Max depth of crater (changes paraboloidic shape if not 1!)
    # resolution; <float>:  Minimal subdivision size on x-/y-axis, thus sets the segment area (ESRI ASCII) to resolution**2 
    
    # -OUT- #
    # mX; <np.array(<float>)>: X-axis grid (matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>: Y-axis grid (matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>: Z-axis grid / height grid (matrix), same shape as X and Y
    ####
    

    # Number of coordinate pairs i.e. segment resolution per axis (total len = 2*semi)
    # X [Y] extension only (nX [nY] -1)*resolution as each grid point corresponds to one terrain junction ! (i.e. tl_corner == 0, 0 ; tr_corner == 0, -1 !) 
    nX = int(np.ceil(2*semimajor/resolution))+1
    nY = int(np.ceil(2*semiminor/resolution))+1

    # Coordinate axis
    x_axis = np.linspace(-semimajor, semimajor, nX)    
    y_axis = np.linspace(-semiminor, semiminor, nY)
    
    # Coordinate grid (adjust shapes)
    ## y-direction through meshgrid is downwards ?!!! (y = [-1, 0, 1])
    mX, mY = np.meshgrid(x_axis, -y_axis, indexing='ij')


    # Compute z via elliptic paraboloid formular
    mZ = mX*mX/(semimajor*semimajor) + mY*mY/(semiminor*semiminor)

    
    # Level all points which do not lie on the ellipse i.e. flat land outside crater
    for j, rows in enumerate(mX):
        for k, x in enumerate(rows):
            y = mY[j][k]
            if x*x/(semimajor*semimajor) + y*y/(semiminor*semiminor) - 1 > 0:
                mZ[j][k] = 1

    # Adjust height (Before: max(z) = 1); Linear scaling to maxDepth -> __changes paraboloidic shape!__
    mZ *= maxDepth

    return mX, mY, mZ
   
   
      
   
def plot_crater(mX, mY, mZ):
    
    ####
    # Plot height-matrix / paraboloidic-like shaped crater (3D via matplotlib UI)

    # -IN- #    
    # mX; <np.array(<float>)>: X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>: Y-axis grid (2D matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>: Z-axis grid / height map (2D matrix), same shape as X and Y
    
    # -OUT- #
    # UI: matplotlib.pyplot.show()
    ####
    
    """
    # 2D option
    if dim == 2:
    
        # 2D color plot
        plt.xlabel("x")
        plt.ylabel("y")
        
        plt.pcolormesh(mX, mY, mZ)#, shading='gouraud')
        plt.colorbar(label="z")
        
        plt.show()


    elif dim == 3:
    """
    
    # 3D surface plot
    fig = plt.figure()

    ax = fig.add_subplot(projection='3d')

    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
                        
    ax.plot_surface(mX, mY, mZ)
    
    hf.set_axes_equal(ax)
    #ax.plot_wireframe(mX, mY, z)
    
    plt.show()
   
    return
   
   




   
   
   
#########################################
################ MAIN ###################
######################################### 
   
   


# Get semimajor, semiminor, maxDepth, resolution, plot, debug from Commandline Interface (CLI)
a, b, maxZ, res, plot, debug = read_cmd()

# Create height-matrix; Shape: elliptic opening and paraboloid-like crater
x, y, z = create_paraboloid(a, b, maxZ, res)

# Plot crater (UI)
if plot: plot_crater(x, y, z)

# Write ESRI ASCII file; Output the filename
sys.stdout.write(hf.write_ESRI_ASCII(z, res))





