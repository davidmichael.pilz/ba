##########
# TODO: #
#   - detect and handle when run as main ?
#   - local import each time in func or rather here gloablly (import n times vs m biiig libs once) ?
#   - overview of included functions
#
##########



##
## Some miscellaneous helper functions used through some or all scripts concerning: I/O ; siconos simulation ; Analysis ; Plotting
##

import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl

from matplotlib import cm, colors   

import sys, getopt
import re
    
    
import subprocess 


####################################### I/O ###################################################

def get_CMD_options(shortArgs='hd', longArgs=['help', 'debug'], description=['help', 'Print all debug messages']):
    
    """
    # TODO: 
        - check if all len match!
        - exception string cleaning...!
        - return dict instead of list !!
    """

    ####
    # Get input parameters from cmd, display all available options with corresponding descriptions when '-h' or '--help' is used
    ## Used in all scripts, typically by read_cmd()

    # -IN- #    
    ## all lists should have same length!
    # shortArgs; <list(<str>)>:     Arguments triggered with '-'; syntax see getopt package
    # longArgs; <list(<str>)>:      Arguments triggered with '--'; syntax see getopt package
    # description; <list(<str>)>:   Short description for given arguments to display on '-h'
    #
    #       default options:
    # help; <bool>:     Display available CLA on stdout (optional CLA)
    # debug; <bool>:    Display debug info on stdout (optional CLA) 
    
    # -OUT- #
    # list of passed arguments <str> via CLI or None, if not set
    ####
    
    import os, sys, re, getopt
        
        
    inDebug = 0
    
    numberArgs = len(longArgs)

    ### not like this!!!! -> ':' !!!
    #if not (len(shortArgs) == numberArgs and numberArgs == len(description))):
    #    raise AssertionError('')


    # Input Command Line Arguments ## https://www.tutorialspoint.com/python/python_command_line_arguments.htm
    try:
        opts, args = getopt.getopt(sys.argv[1:], shortArgs, longArgs)
        # Get rid of ':' and '='
        shortArgs = re.sub(r'[^A-Za-z0-9 ]+', '', shortArgs)
        for j in range(numberArgs):
            longArgs[j] = re.sub(r'[^A-Za-z0-9 ]+', '', longArgs[j])
            
            
    except getopt.GetoptError:
    
        # Get rid of ':' and '='
        ## maybe as default..?
        shortArgs = re.sub(r'[^A-Za-z0-9 ]+', '', shortArgs)
        for j in range(numberArgs):
            longArgs[j] = re.sub(r'[^A-Za-z0-9 ]+', '', longArgs[j])
            
            
        print('Only ', end='')
        for j in range(numberArgs):
            if j != 0: print(', ', end='')
            print('-\''+shortArgs[j]+'\'', '--\''+longArgs[j]+'\'', end='')
            
        print(' allowed!')            
        sys.exit(2)
        

    # Prepopulate options output array
    optIn = []
    for j in range(len(longArgs)):
        optIn.append(None)
    
    for opt, arg in opts:

        # help -> display available CLA        
        if opt in ('-h', '--help'):
        
            print(os.path.basename(__file__) + ' [opts:\t ', end='')
            for j in range(numberArgs):
                if j != 0: print('\t\t ', end='')
                print('-'+shortArgs[j]+' --'+longArgs[j]+'\t('+description[j]+')')
            print('\t ]')
            
            sys.exit()

        # debug
        elif opt in ('-d', '--debug'):
            inDebug = 1
            
        # Get all other options    
        for j in range(numberArgs):
            if opt in ('-'+shortArgs[j], '--'+longArgs[j]):
                # Needed for 'set-options' like plot or debug   --> is None comparable to False?!
                if arg is None or arg == '': arg = True
                optIn[j] = arg

    return optIn





def read_CSV(filePath):
    """
    #TODO:
        - Kontext!
    """

    ####
    # Read and return content of a CSV file with descriptors of columns in the first line (not counting comments)
    ## Typically used when loading '<type>_params.csv' files (<type> e.g. 'crater' or 'incline') by read_cmd()

    # -IN- #    
    # filePath; <str>: PATH to .csv or csv like .txt file
    
    # -OUT- #
    # parameterList; <list(<dict(<str>:<str>)>)>: List of dictionaries, one for each parameter set (row; DOE: level)
    ####
       
    #import codecs
    import numpy as np
    import os
    
        
    with open(filePath.strip()) as f:
        # Ignore comments (needed for skiprows to work properly [comments will be counted as rows as well]) -> skiprows not needed if same generator is accessed :)
        content = (line for line in f if not line.startswith('#'))
            
        # Get descriptors
        #with codecs.open(filePath, encoding = 'UTF8') as encoded_file:
        #    parameterNames = np.loadtxt(encoded_file, delimiter=', ', dtype='str', max_rows=1)
        parameterNames = np.loadtxt(content, delimiter=',', dtype='str', max_rows=1)
        
        # Get values (we need a 2D array for the dict creation below -> force to be 2D)
        parameterValues =  np.atleast_2d(np.loadtxt(content, delimiter=',', dtype='str'))#, skiprows=1))

        # Create dicts for each parameterSet
        parameterList = []
        for j in range(parameterValues.shape[0]):
            parameterDict = {parameterNames[k].strip():parameterValues[j, k] for k in range(len(parameterNames))}
            parameterList.append(parameterDict)

    ### output items are dtype str!!!
    return parameterList



def write_CSV(header, values, filename):
    """
    #TODO:
        - Kontext!
    """

    ####
    # Write values in lines under header to a .csv named filename
    ## Will override file if 'filename' already exist!
    ##

    # -IN- #   
    # header; <list(<str>)>:    All names for the parameters stored inside this .csv ; Expected 1D 
    # values; <list(<list()>)>: Values to be stored corresponding to the parameters 
    # filePath; <str>:          Name of generated .csv file
    
    # -OUT- #
    # I/O: .csv file containing header and values
    ####

    with open(filename, 'w') as csv_file:
    
        # Write headers (parameter descriptions) separated by ', '
        for h, head in enumerate(header):
            if not h == 0:
                csv_file.write(', ')
            csv_file.write(head)
        csv_file.write('\n')
        
        # Write values separated by ', '
        for value_set in values:
            for v, value in enumerate(value_set):
                if not v == 0:
                    csv_file.write(', ')
                csv_file.write(str(value))
            csv_file.write('\n')
       
    return 0



def read_ESRI_ASCII(filePath):

    """
    # TODO:
        - generalize coordinate center origin!!!! via xll, yll
        - siconos definiton coord system working??
    """
    
    ####
    # Load ESRI ASCII grid. Returns additional features (e.g. x-/y-meshgrid)
    ## Beware! siconos's coordinate system definition used! x ^ (rows) y -> (columns)
    ## Center of this coordinate system assumed at (0, 0)
    ## Typically used for heightmaps or soil / block starting position masks

    # -IN- #    
    # filePath; <FILE_PATH>:    Path to ESRI ASCII file (.asc) 
    
    # -OUT- #
    # mX; <np.array(<float>)>:      X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>:      Y-axis grid (2D matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>:      Z-axis grid / height map (2D matrix), same shape as X and Y
    # headerDict; <dict(<str>)>:    Dictionary containing all ESRI ASCII header information
    # maxX/maxY; <float>:           Max values for x/y-axis
    ####
    
    import numpy as np
    
    import os
    
    with open(filePath, 'r') as esri_ascii_file:
    
        # Get header data (first 6 lines)
        header = [next(esri_ascii_file).split() for lines in range(6)]
        headerDict = {lines[0]:lines[1] for lines in header}
        
        
        cellLength = float(headerDict['cellsize'])

        ## Used siconos definition!: x ^ y ->
        nX = int(headerDict['nrows'])
        nY = int(headerDict['ncols'])
        
                   
        #### TODO: generalize!!!! via xll, yll -- might lead to conflicts in _creator, soil_block_.. logic
        # Center assumed at (0, 0)
        # X [Y] extension only (nX [nY] -1)*resolution as each grid point corresponds to one terrain junction ! (i.e. tl_corner == 0, 0 ; tr_corner == 0, -1 !) 
        maxX = (nX-1)/2 * cellLength
        maxY = (nY-1)/2 * cellLength

        # Coordinate axis (x,y)
        x_axis = np.linspace(-maxX, maxX, nX)    
        y_axis = np.linspace(-maxY, maxY, nY)
        
        # Coordinate grid (adjust shapes)
        mX, mY = np.meshgrid(x_axis, y_axis, indexing='ij')
        
    # Load z
    ### Comment l. 84 RasterTools.py: the origin of the ascii raster is at the bottom (right) ?????? BEWARE l. 83 !
    ### Well, the data is displayed in QGIS _as written_ i.e. ll is ll and lr is lr !!
    ## might be due to some confusion regarding the coordinate system definitions..
    
    # .T needed as ### depends on coordinate system definition
    mZ = np.loadtxt(filePath, skiprows=6)#, dtype=float)#.T
        
    #mZ = np.flip(mZ,axis=1)
            
    return mX, mY, mZ, (headerDict, maxX, maxY)
        
    
    

def write_ESRI_ASCII(grid, cellsize, nodatavalue=-9999, filename=None):
    """
    # TODO:
        - fstring?
    """

    ####
    # Write given grid to ESRI ASCII raster file with specified filename or default 'crater_<semimajor>_<semiminor>_<maxDepth>.asc'
    ## Origin of coordinate system assumed at center!
    ## Beware! siconos's coordinate system definition used! x ^ (rows) y -> (columns)
    ## Typically used for heightmaps or soil / block starting position masks by '<type>_generator.py' and 'soil_blocKStart_generator.py'

    # -IN- #    
    # grid; <np.array(<float>)>:    Raster grid (for example heightmap) to write to file
    # cellsize; <float>:            Length of one segment (assumed qudratic!)
    #
    # notdatavlaue; <float>:        Value where segments are not set; Default: -9999 (optional)
    # filename; <str>:              Name of resulting file; If None name will default to 'crater_<semimajor/xllcorner>_<semiminor/yllcorner>_<maxDepth>.asc' (optional)
    
    
    # -OUT- #
    # IO: ESRI ASCII file 
    # filename; <str>:  Name of created file
    ####
    
    import numpy as np       

    nrows, ncols = grid.shape
        
    # LowerLeftCorner ; origin at center!
    ## siconos' coordinate system definition x ^ y ->
    # X [Y] extension only (nX [nY] -1)*resolution as each grid point corresponds to one terrain junction ! (i.e. tl_corner == 0, 0 ; tr_corner == 0, -1 !) 
    xllcorner, yllcorner = -(nrows-1)/2 * cellsize, -(ncols-1)/2 * cellsize


    # Naming convention: 'crater_<semimajor>_<semiminor>_<maxDepth>.asc'
    if filename is None:
        filename = 'crater_'+ str(-xllcorner) +'_'+ str(-yllcorner) +'_'+ str(np.amax(grid)) +'.asc'


    with open(filename,'w', encoding='UTF-8') as out_file:

        # Write header information (ref. https://desktop.arcgis.com/en/arcmap/latest/manage-data/raster-and-images/esri-ascii-raster-format.htm)
        out_file.write('ncols ' + str(ncols) + '\nnrows ' + str(nrows) + '\nxllcorner ' + str(xllcorner) + '\nyllcorner ' + str(yllcorner) + '\ncellsize ' + str(cellsize) + '\nNODATA_value ' + str(nodatavalue) + '\n')

        # Write grid items to file
        for rows in range(0, nrows):
            for cols in range(0, ncols):
                out_file.write(str(grid[rows][cols]) + ' ')
            out_file.write('\n')
        
    return filename
    
    
    


def load_extracted_paths(filePath):
    """
    #TODO:
    """

    ####
    # Read and return content of a .GEOJSON file containing extracted features from map
    ## WATCH OUT! still in [lon, lat] for lat:row, lon:column when read !!
    ## Coordinates adjusted to match siconos' internal coordinate system definition (row, column)

    # -IN- #    
    # filePath; <str>:  PATH to .GEOJSON file containing extracted features from map adjusted to match siconos' internal coordinate system definition (row, column)
    
    
    # -OUT- #
    # paths; <dict(<str>:?)>: All coordinates (single [<list(<float>, <float>)>] and paths [<list(<list(<float>, <float>)>)>]) stored in the .GEOJSON with the corresponding label
    ####
    
    import json
    
    # Load file contents
    with open(filePath) as f:
        content = json.load(f)
    
    paths = {}
    
    # Typically multiple extracted features stored in one .GEOJSON
    for feature in content['features']:
        feature_name = feature['properties']['label']
        
        # Ignore the shape paths (e.g. 'path_1l', 'path_1s')
        if not any(char in feature_name for char in ('s', 'l')):
            paths[feature_name] = feature['geometry']['coordinates']
    
    
    return paths
       
     
       
def load_analysis(filePath):
    """
    #TODO:
    """

    ####
    # Read and return content of a .JSON file containing all analysis parameters
    ## We only care about the mean of 'individual_distance_statistics' thus ignore the rest of this set

    # -IN- #    
    # filePath; <str>:  PATH to .JSON file containing all analysis parameters
    
    
    # -OUT- #
    # analysis; <np.array(<float>)>:    All parameter (column) values for each site (row)
    # parameters; <list(<str>)>:        All parameters labels corresponding to the column of analysis ; Including percentile_location ! Here split into 'percentile_location_lower' and 'percentile_location_upper' same as the returned data
    ####
    
    import json
    
    # Load file contents
    with open(filePath) as f:
        content = json.load(f)
    
    analysis = []
        
    # Load all parameters for each site
    for s, site in enumerate(content[0].keys()):
    
        parameters = content[0][site].keys()
        parameter_labels = list(parameters)
        analysis.append([])
        
        # Load the corresponding values
        for p, parameter in enumerate(parameters):
        
            # For the individual_distance we only care about the mean
            if parameter == 'individual_distance_statistics':
                value = content[0][site][parameter]['mean'][0]
            # For the location, we have to extract both bounds: insert lower right now and upper later
            elif parameter == 'percentile_location':
                # WATCH OUT: we need to fix the parameter_labels as well!
                parameter_labels[p] = 'percentile_location_upper'
                parameter_labels.insert(p, 'percentile_location_lower')
                
                analysis[s].append(content[0][site][parameter][0])
                value = content[0][site][parameter][1]
            else:
                value = content[0][site][parameter]    
            
            # For the statistical analysis we don't care about the coordiantes
            if parameter in ('distribution_areas', 'distribution_center'):
                continue
                
            analysis[s].append(value)
        
    # We don't care about those
    parameter_labels.remove('distribution_areas')
    parameter_labels.remove('distribution_center')
    
    return np.array(analysis), parameter_labels
    
    
    

## Needed??
def write_JSON(parameters, values, filename, mode='w'):
    """
    #TODO:
        - 'a' could interfere with json.dump standard ?!?
    """

    ####
    # Write values in lines under header to a .csv named filename
    ## Will override file if 'filename' already exist!
    ##

    # -IN- #   
    # parameters; <list(<str>)>:    All names for the parameters stored inside this .csv ; Expected 1D 
    # values; <list(<list()>)>: Values to be stored corresponding to the parameters 
    # filePath; <str>:          Name of generated .csv file
    
    # mode; <str>:              Mode ('w' OR 'a') for file opening ; Default: 'w' (optional)
    
    # -OUT- #
    # I/O: .csv file containing header and values
    ####


    json_output = [{parameter: value} for parameter, value in zip(parameters, values)]
    
    with open(filename, mode) as json_file:
        json.dump(json_output, json_file)
    
    
    return 0
       
       
       
       
        

        

    
####################################### SICONOS ###############################################   


    
    
        # Resample and resize heightmap (linear interpolation): x, y -> 2*x+1, 2*y+1
def resampleheightmap(heightmap):

        heightmap_fin = np.zeros((2*heightmap.shape[0] + 1, 2*heightmap.shape[1] + 1))
        
        ## populate submatrix _without edges_ (x-1, y-1) because no meaningful interpolation can be done there
        for i in range(heightmap.shape[0]-1):

                ## right 'edge' (x-1, y-1)
                heightmap_fin[2*i+1][heightmap_fin.shape[1]-2] = heightmap[i][heightmap.shape[1]-1]
                heightmap_fin[2*i+2][heightmap_fin.shape[1]-2] = 0.5*(heightmap[i][heightmap.shape[1]-1] + heightmap[i+1][heightmap.shape[1]-1])
                
                for j in range(heightmap.shape[1]-1):
                
                        # populate submatrix with linear interpolation 
                        
                        heightmap_fin[2*i+1][2*j+1] = heightmap[i][j]
                        
                        heightmap_fin[2*i+2][2*j+2] = 0.5*(heightmap[i][j] + heightmap[i+1][j+1])
                        heightmap_fin[2*i+1][2*j+2] = 0.5*(heightmap[i][j] + heightmap[i][j+1])
                        
                        heightmap_fin[2*i+2][2*j+1] = 0.5*(heightmap[i][j] + heightmap[i+1][j])        
                        
                        
                        ## bottom 'edge' (x-1, y-1)
                        heightmap_fin[heightmap_fin.shape[0]-2][2*j+1] = heightmap[heightmap.shape[0]-1][j]
                        heightmap_fin[heightmap_fin.shape[0]-2][2*j+2] = 0.5*(heightmap[heightmap.shape[0]-1][j] + heightmap[heightmap.shape[0]-1][j+1])

        ## lr 'corner' (x-1,y-1)                        
        heightmap_fin[heightmap_fin.shape[0]-2][heightmap_fin.shape[1]-2] = heightmap[heightmap.shape[0]-1][heightmap.shape[1]-1]
        
        # ul corner        
        heightmap_fin[0][0] = heightmap[0][0]                                        
        
                
        ## adjust left-out edges
        for i in range(heightmap_fin.shape[0]):
                heightmap_fin[i][0] = heightmap_fin[i][1]
                heightmap_fin[i][heightmap_fin.shape[1]-1] = heightmap_fin[i][heightmap_fin.shape[1]-2]
                
        for i in range(heightmap_fin.shape[1]):
                ### will override all corners -- noproblem
                heightmap_fin[0][i] = heightmap_fin[1][i]
                heightmap_fin[heightmap_fin.shape[0]-1][i] = heightmap_fin[heightmap_fin.shape[0]-2][i]
                
                
        return heightmap_fin 




#print(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
#print(resampleheightmap(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])))


        # Resample and resize mask: x, y -> 2*x+1, 2*y+1
def resamplemask(mask):

        mask_fin = np.zeros((2*mask.shape[0]+1, 2*mask.shape[1]+1))
        mask_fin[0][0] = mask[0][0]
        
        for i in range(mask.shape[0]):
                for j in range(mask.shape[1]):
                        if mask[i][j]==1:
                                mask_fin[2*i][2*j] = mask[i][j]
                                mask_fin[2*i][2*j+1] = mask[i][j]        
                                mask_fin[2*i][2*j+2] = mask[i][j]
                                
                                mask_fin[2*i+1][2*j] = mask[i][j]
                                mask_fin[2*i+1][2*j+1] = mask[i][j]        
                                mask_fin[2*i+1][2*j+2] = mask[i][j]
                                
                                mask_fin[2*i+2][2*j] = mask[i][j]
                                mask_fin[2*i+2][2*j+1] = mask[i][j]        
                                mask_fin[2*i+2][2*j+2] = mask[i][j]
                                
        return mask_fin 
        
        
        
    
    
def add_padding(heightmap):
    """
    # TODO:
        - check explanation
    """
    
    
    ####
    # Add padding (right and bottom) to heightmap (x, y -> x+1, y+1) via constant expansion
    ## A   B        A  B  B
    ##        -->   C  D  D
    ## C   D        C  D  D   
    ## Needed when defining multiple soil types i.e. multiple heightmaps with different properties for the siconos simulation
    ## This way we avoid holes at soil transitioning due to the definition of heightmaps inside siconos
    ########## TOCHECK!: triangulated surfaces where vertical sides are forbidden (?) -> starting at 0 with slope >0 --> clipping of different soils/heightmaps needed!
    ## Typically used in combination with resamplemask()

    # -IN- #    
    # heightmap; <np.array(<float>)>:   Height grid (2D matrix, shape: x, y)
    
    # -OUT- #
    # heightmap_new; <np.array(<float>)>:   Padded height grid (new shape: x+1, y+1)
    ####

    
    # Initalize new heightmap array
    heightmap_new = heightmap.copy()
    
    # Right padding
    heightmap_new = np.hstack((heightmap_new, heightmap_new[:, -1].reshape((-1, 1))))
    
    # Bottom padding
    heightmap_new = np.vstack((heightmap_new, heightmap_new[-1, :].reshape((1, -1))))
    
    return heightmap_new
    
    


        # Resample and resize heightmap (default: linear interpolation): x, y -> 2*x-1, 2*y-1
def resampleheightmap1(heightmap, interpolation_func=lambda x, y: 0.5*(x+y)):
    """
    # TODO:
        - check explanation
    """
    
    
    ####
    # Resample (finer resolution) and resize the given heightmap (interpolation between originally adjacent tiles): x, y -> 2*x-1, 2*y-1
    ## A   B        A  AB  B
    ##        -->   AC AD  BD
    ## C   D        C  CD  D   
    ## Needed when defining multiple soil types i.e. multiple heightmaps with different properties for the siconos simulation
    ## This way we avoid holes at soil transitioning due to the definition of heightmaps inside siconos: we need to alwas
    ########## TOCHECK!: triangulated surfaces where vertical sides are forbidden (?) -> starting at 0 with slope >0 --> clipping of different soils/heightmaps needed!
    ## Typically used in combination with resamplemask()

    # -IN- #    
    # heightmap; <np.array(<float>)>:   Height grid (2D matrix, shape: x, y)
    
    # interpolation_func; <function>:   Function, which defines how to interpolate between tiles ; Default: linear interpolation (optional)
    
    # -OUT- #
    # heightmap_new; <np.array(<float>)>:   Resampled height grid (new shape: 2*x-1, 2*y-1)
    ####
    
    
    import numpy as np
    
    
    maxj = heightmap.shape[0] - 1
    maxk = heightmap.shape[1] - 1
    
    # Initalize new heightmap array
    ## - 1 due to final mapping of the old bottom row to the new bottom row: no further (2*maxj th) row possible (no further information in old grid)
    heightmap_new = np.zeros((2*heightmap.shape[0] - 1, 2*heightmap.shape[1] - 1))
        
    maxj_new = heightmap_new.shape[0] - 1
    maxk_new = heightmap_new.shape[1] - 1
    
    
        # Map old grid to new grid via a 2x2 kernel taking the right, bottom and right-bottom diagonal neighbors into account
    ## Except right (j, maxk) and bottom (maxj, k) edge: the geometry of neighboring tiles is different there
    for j in range(maxj):
        for k in range(maxk):
        
            # Map all constant values (0,0 -> 0,0 ; 1,1 -> 2,2 ; 0,1 -> 0,2 ;...): one additional tile (e.g. 1,1) between all (old) tiles (e.g. 0,0 and 2,2)
            heightmap_new[2*j, 2*k] = heightmap[j, k]
        
            # Right neighbor
            heightmap_new[2*j, 2*k+1] = interpolation_func(heightmap[j, k], heightmap[j, k+1])
            # Bottom neighbor
            heightmap_new[2*j+1, 2*k] = interpolation_func(heightmap[j, k], heightmap[j+1, k])
            # Diagonal neighbor
            heightmap_new[2*j+1, 2*k+1] = interpolation_func(heightmap[j, k], heightmap[j+1, k+1])
            
            
    # Right edge (j, maxk_new)
    ## Except lower right corner: no interpolation
    for j in range(maxj):
    
        # Constant values
        heightmap_new[2*j, maxk_new] = heightmap[j, maxk]
        
        # Bottom neighbor
        heightmap_new[2*j+1, maxk_new] = interpolation_func(heightmap[j, maxk], heightmap[j+1, maxk])
            
            
    # Bottom edge (maxj_new, k)
    ## Except lower right corner: no interpolation
    for k in range(maxk):
    
        # Constant values
        heightmap_new[maxj_new, 2*k] = heightmap[maxj, k]
        
        # Bottom neighbor
        heightmap_new[maxj_new, 2*k+1] = interpolation_func(heightmap[maxj, k], heightmap[maxj, k+1])
        
        
    # Lower right corner
    heightmap_new[maxj_new, maxk_new] = heightmap[maxj, maxk]
        
        
    return heightmap_new
    
    
    
    
    
        # Pad mask
def resamplemask2(mask):
    """
    # TODO:
        - nicer??
        - DOCU!
    """
    
    ####
    # Resample the given (boolean) mask grid (__assuming holding only__: 0: not set, 1: set)
    #### We need overlap --> look to the left and top as well!
    ## Needed when defining multiple soil types i.e. multiple heightmaps with different properties for the siconos simulation
    ## This way we avoid holes at soil transitioning due to the definition of heightmaps inside siconos: triangulated surfaces where vertical sides are forbidden -> starting at 0 with slope >0 --> clipping of different soils/heightmaps needed!
    ## Typically used in combination with resampleheightmap()

    # -IN- #    
    # mask; <np.array(<float>)>:    Mask grid (2D matrix, shape: x, y)
    
    # -OUT- #
    # maks_new; <np.array(<float>)>:   Resampled mask grid (new shape: 2*x-1, 2*y-1)
    ####
    
    import numpy as np
    
    
    maxj = mask.shape[0] - 1
    maxk = mask.shape[1] - 1
    
    # Initalize new heightmap array
    ## - 1 due to final mapping of the old bottom row to the new bottom row: no further (2*maxj th) row possible (no further information in old grid)
    mask_new = np.zeros(mask.shape) #(2*mask.shape[0] - 1, 2*mask.shape[1] - 1))
        
    maxj_new = mask_new.shape[0] - 1
    maxk_new = mask_new.shape[1] - 1
    
    
        # Map old grid to new grid via a 3x3 kernel on all adjacent tiles in the new grid
    for j in range(maxj+1):
        for k in range(maxk+1):
        
            if mask[j, k] == 1:            
                
                # Middle tile
                mask_new[j, k] = mask[j, k]
                
                # Top edge different
                if not j == 0:
                    # Top left corner
                    if not k == 0:
                        # Diagonal neighbor
                        mask_new[j-1, k-1] = mask[j, k]
                        
                    # Top neighbor
                    mask_new[j-1, k] = mask[j, k]
                    
                    # Top right corner
                    if not k == maxk:
                        # Diagonal neighbor
                        mask_new[j-1, k+1] = mask[j, k]
                    
                    
                # Right edge different
                if not k == maxk:
                    # Top right corner
                    if not j == 0:
                        # Diagonal neighbor
                        mask_new[j-1, k+1] = mask[j, k] 
                                           
                    # Right neighbor
                    mask_new[j, k+1] = mask[j, k]
                    
                    # Bottom right corner
                    if not j == maxj:
                        # Diagonal neighbor
                        mask_new[j+1, k+1] = mask[j, k]
                    
                    
                # Bottom edge different
                if not j == maxj:
                    # Bottom right corner
                    if not k == maxk:
                        # Diagonal neighbor
                        mask_new[j+1, k+1] = mask[j, k]
                        
                    # Bottom neighbor
                    mask_new[j+1, k] = mask[j, k]
                    
                    # Bottom left corner
                    if not k == 0:
                        # Diagonal neighbor
                        mask_new[j+1, k-1] = mask[j, k]            
            
            
                # Left edge different
                if not k == 0:
                    # Bottom left corner
                    if not j == maxj:
                        # Diagonal neighbor
                        mask_new[j+1, k-1] = mask[j, k] 
                        
                    # Left neighbor
                    mask_new[j, k-1] = mask[j, k]
                    
                    # Top left corner
                    if not j == 0:
                        # Diagonal neighbor
                        mask_new[j-1, k-1] = mask[j, k]
                            
    return mask_new
    
    
    
    

        # Resample and resize mask: x, y -> 2*x-1, 2*y-1
def resamplemask1(mask):
    """
    # TODO:
        - nicer??
        - DOCU!
    """
    
    ####
    # Resample the given (boolean) mask grid (__assuming holding only__: 0: not set, 1: set): x, y -> 2*x-1, 2*y-1
    #### We need overlap --> look to the left and top as well!
    ## Needed when defining multiple soil types i.e. multiple heightmaps with different properties for the siconos simulation
    ## This way we avoid holes at soil transitioning due to the definition of heightmaps inside siconos: triangulated surfaces where vertical sides are forbidden -> starting at 0 with slope >0 --> clipping of different soils/heightmaps needed!
    ## Typically used in combination with resampleheightmap()

    # -IN- #    
    # mask; <np.array(<float>)>:    Mask grid (2D matrix, shape: x, y)
    
    # -OUT- #
    # maks_new; <np.array(<float>)>:   Resampled mask grid (new shape: 2*x-1, 2*y-1)
    ####
    
    import numpy as np
    
    
    maxj = mask.shape[0] - 1
    maxk = mask.shape[1] - 1
    
    # Initalize new heightmap array
    ## - 1 due to final mapping of the old bottom row to the new bottom row: no further (2*maxj th) row possible (no further information in old grid)
    mask_new = np.zeros((2*mask.shape[0] - 1, 2*mask.shape[1] - 1))
        
    maxj_new = mask_new.shape[0] - 1
    maxk_new = mask_new.shape[1] - 1
    
    
        # Map old grid to new grid via a 3x3 kernel on all adjacent tiles in the new grid
    for j in range(maxj+1):
        for k in range(maxk+1):
        
            if mask[j, k] == 1:            
                
                # Middle tile
                mask_new[2*j, 2*k] = mask[j, k]
                
                # Top edge different
                if not j == 0:
                    # Top left corner
                    if not k == 0:
                        # Diagonal neighbor
                        mask_new[2*j-1, 2*k-1] = mask[j, k]
                        
                    # Top neighbor
                    mask_new[2*j-1, 2*k] = mask[j, k]
                    
                    # Top right corner
                    if not k == maxk:
                        # Diagonal neighbor
                        mask_new[2*j-1, 2*k+1] = mask[j, k]
                    
                    
                # Right edge different
                if not k == maxk:
                    # Top right corner
                    if not j == 0:
                        # Diagonal neighbor
                        mask_new[2*j-1, 2*k+1] = mask[j, k] 
                                           
                    # Right neighbor
                    mask_new[2*j, 2*k+1] = mask[j, k]
                    
                    # Bottom right corner
                    if not j == maxj:
                        # Diagonal neighbor
                        mask_new[2*j+1, 2*k+1] = mask[j, k]
                    
                    
                # Bottom edge different
                if not j == maxj:
                    # Bottom right corner
                    if not k == maxk:
                        # Diagonal neighbor
                        mask_new[2*j+1, 2*k+1] = mask[j, k]
                        
                    # Bottom neighbor
                    mask_new[2*j+1, 2*k] = mask[j, k]
                    
                    # Bottom left corner
                    if not k == 0:
                        # Diagonal neighbor
                        mask_new[2*j+1, 2*k-1] = mask[j, k]            
            
            
                # Left edge different
                if not k == 0:
                    # Bottom left corner
                    if not j == maxj:
                        # Diagonal neighbor
                        mask_new[2*j+1, 2*k-1] = mask[j, k] 
                        
                    # Left neighbor
                    mask_new[2*j, 2*k-1] = mask[j, k]
                    
                    # Top left corner
                    if not j == 0:
                        # Diagonal neighbor
                        mask_new[2*j-1, 2*k-1] = mask[j, k]
                            
    return mask_new
  
    
### TOCLEAN!!!
def create_vertices_rock3D(L1,L2,L3,nb_points_hull):
        #create convexhull with pyhull
        points = np.array([[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0], [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]])#np.random.rand(nb_points_hull, 3)
        
        # Create smallest convex hull, that incldues all points
        hull = pyhull.convex_hull.ConvexHull(points)
        
        
        # Get coordinates of the hull's vertices, i.e. of the convex hull's simplixces' vertices
        ## Here we have some points multiple times, as multiple simplices share them as vertices
        vertices = []
        for simplex in hull.simplices:
            vertices.append(simplex.coords)
        
        # Reshape array to get simple 'list' of points
        vertices = np.array(vertices, dtype=float).reshape((-1, 3))
        # And pick the unique points
        vertices = np.unique(vertices, axis=0)
        
        
        # Get all points as close as possible to 0, 0, 0
        ## (? original comment) homothety of the convexhull
        ## We only work with positive values here
        vertices[:,0] = vertices[:,0] - np.min(vertices[:,0])
        vertices[:,1] = vertices[:,1] - np.min(vertices[:,1])
        vertices[:,2] = vertices[:,2] - np.min(vertices[:,2])
        
        # And 'normalize' them before stretching (max(dim_j) is L_j)
        ## Does not preserve the shape
        vertices[:,0] = L1*vertices[:,0]/np.max(vertices[:,0]) 
        vertices[:,1] = L2*vertices[:,1]/np.max(vertices[:,1]) 
        vertices[:,2] = L3*vertices[:,2]/np.max(vertices[:,2]) 
        
        """
        # And normalize -> stretch them to their new size 
        ## well, that does not preserve the original shape either...
        vertices = vertices.tolist()
        
        ## better way?!?
        for p, point in enumerate(vertices):
            norm = np.linalg.norm(point)
            vertices[p] = [point[0]*L1/norm, point[1]*L2/norm, point[2]*L3/norm]
        """
            
               
        return vertices
    
    
    
####################################### ANALYSIS ################################################



    
    
def sort_array(data_array):
    """
    # TODO:
        - sorted_array shape: sure?!?
    """
    
    ####
    # Sort rows in 2D array as sub-arrays ascending by ID given in [_, 1]
    
    # -IN- #    
    # data_array; <np.array(<float>)>:  2D array with IDs specified in [_, 1]
    
    # -OUT- #    
    # sorted_array>; <np.array(<float>)>:   3D array rows in sub-arrays sorted by IDs stack ontop each other [[row1_ID1, row2_ID1, ...], [row1_ID2, row2_ID2, ...], ...]
    ####
    
    sorted_array = None
    n_columns = len(data_array[0])
    
    # Get IDs
    IDs = np.unique(data_array[:, 1])
    
    # Loop through all IDs
    for ID in IDs:
        # Get all rows corresponding to specific ID
        indizes = np.argwhere(data_array[:, 1] == ID)
        
        # Stack (sub-)arrays ontop each other
        if sorted_array is None: 
            sorted_array = data_array[indizes].reshape((1, -1, n_columns))
        else:     
            sorted_array = np.concatenate((sorted_array, data_array[indizes].reshape((1, -1, n_columns))))
    
    
    return sorted_array
        
    
    
    
def kinetic_energy(r_v, omega_v, R=1, m=1, I=lambda m, R: 2/5*m*R*R):
    """
    # TODO:
    """
    
    ####
    # Calculate the kinetic energy (translation and rolling) - Default: Sphere: I = 2/5 M R²
    ## Ekin = E_translation + E_rotation = 0.5 M v**2 + 0.5 I omega**2
    
    # -IN- #    
    # r_v; <np.array(<float>)>:     1D _OR_ 2D (e.g. multiple translational starting velocities v0) array containing translational velocities 
    # omega_v; <np.array(<float>)>: 1D _OR_ 2D (e.g. multiple translational starting velocities v0) array containing rotational velocities 
    
    # R; <float>:       Characteristical length (e.g. radius of sphere) for the calculation of I; Default: 1
    # m; <float>:       Mass of body corresponding to the velocities; Default: 1
    # I; <float/FUNC>:  Inertia of body _OR_ 2 dimensional (2 inputs: m, R) function for the calculation of the ineratia; Default: lambda m, R: 2/5*m*R*R
    
    # -OUT- #    
    # Ekin; <np.array(<float>)>:    Same shaped array as r_v with the total kinetic energy for each row
    ####
    
    # If I is a function: evaluate it at m, R
    if callable(I):
        I = I(m, R)
    
    Ekin = None
        
        # 2D array
    if len(r_v.shape) == 2:
    
        # Look at each starting velocity separately
        for v0 in range(r_v.shape[0]):
            if Ekin is None:
                Ekin = (0.5*m * r_v[v0]*r_v[v0] + 0.5*I * omega_v[v0]*omega_v[v0]).reshape((1, -1))
            else:
                Ekin = np.concatenate((Ekin, (0.5*m * r_v[v0]*r_v[v0] + 0.5*I * omega_v[v0]*omega_v[v0]).reshape((1, -1))))
                
        return Ekin
        
    # 1D array
    return 0.5*m * r_v*r_v + 0.5*I * omega_v*omega_v
        
    
    
def potential_energy(z, m=1, g=1.62):
    """
    # TODO:
    """
    
    ####
    # Calculate the potential energy in a homogeneous gravitational field
    ## Epot = M g z
    
    # -IN- #    
    # z; <np.array(<float>)>:   1D array containing heights measured parallel to the gravitational field lines
    
    # m; <float>:   Mass of body corresponding to the velocities; Default: 1
    # g; <float>:   Gravitational acceleration; Default: 1.62 (moon: https://nssdc.gsfc.nasa.gov/planetary/factsheet/moonfact.html)    
    
    # -OUT- #    
    # Epot; <np.array(<float>)>:    1D array containing the corresponding potential energy for each height
    ####
    
    return m*g*z
    
    
 
def analytic_solution(t, r0=0, r_v0=0, omega0=0, omega_v0=0, R=1, alpha=20*np.pi/180, g=1.62):
    """
    # TODO:
        - generalize for arbitrary I
    """
    
    ####
    # Calculate the analytical solution (r, v, omega) for aDisc rolling down a 2D plane _without_ slipping
    ## Multiple translational starting velocities may be passed; same position and rotational starting velocity assumed though
    ## part. ref https://physics.uwo.ca/~mhoude2/courses/PDF%20files/physics350/Lagrange.pdf#page=14
    
    # -IN- #    
    # t; <np.array(<float>)>:   Moments in time (e.g. time-axis starting at 0 to tmax) to calculate the analytical solution for
    # alpha; <float>:           Inclination of the plane in rad; Default: 20*pi/180    
    
    # r_v0; <float>/<np.array(<float>)>:    Translational starting velocity(ies)
    # omega0; <float>>:                     Rotational starting velocity
    # r0; <float>>:                         Starting position along length of plane (x-axis)
    
    # R; <float>:       Characteristical length (e.g. radius of sphere) for the calculation of I; Default: 1
    # I; <float/FUNC>:  Inertia of body _OR_ 2 dimensional (2 inputs: m, R) function for the calculation of the ineratia; Default: lambda m, R: 2/5*m*R*R
    
    # g; <float>:   Gravitational acceleration; Default: 1.62 (moon: https://nssdc.gsfc.nasa.gov/planetary/factsheet/moonfact.html)  
    
    
    # -OUT- #    
    # [r, omega], [r_v, omega_v]; <float>/<np.array(<float>)>:  Analytical solutions for the problem; Same dtype as t
    ####
    
    
        # Analytical solutions
        ## Calculations done for a sphere: I = 2/5 M R² -> 3/2 --> 7/5
    # r
    def f_r(t, r0, v0, g, alpha):
        return 5/14*g*np.sin(alpha) * t*t + v0 * t + r0
    # v
    def f_v(t, v0, g, alpha):
        return 5/7*g*np.sin(alpha) * t + v0
    # omega
    def f_omega(t, omega_v0, omega0, g, alpha):
        return 5/14*g*np.sin(alpha)/R * t*t + omega_v0 * t + omega0
    # omega_v
    def f_omega_v(t, omega_v0, g, alpha):
        return 5/7*g*np.sin(alpha)/R * t + omega_v0
        
        
    r = None

    # Populate arrays if multiple translational starting velocities given
    if type(r_v0) == np.ndarray:
        for v0 in r_v0:
            if r is None:
                r = f_r(t, r0, v0, g, alpha).reshape((1, -1))
                r_v = f_v(t, v0, g, alpha).reshape((1, -1))
                
                omega = f_omega(t, omega_v0, omega0, g, alpha).reshape((1, -1))
                omega_v = f_omega_v(t, omega_v0, g, alpha).reshape((1, -1))
            else:
                r = np.concatenate((r, f_r(t, r0, v0, g, alpha).reshape((1, -1))))
                r_v = np.concatenate((r_v, f_v(t, v0, g, alpha).reshape((1, -1))))
                
                omega = np.concatenate((omega, f_omega(t, omega_v0, omega0, g, alpha).reshape((1, -1))))    
                omega_v = np.concatenate((omega_v, f_omega_v(t, omega_v0, g, alpha).reshape((1, -1))))
    
    
    # Or return float
    else:
        r = f_r(t, r0, r_v0, g, alpha)
        r_v = f_v(t, r_v0, g, alpha)
        
        omega = f_omega(t, omega_v0, omega0, g, alpha)
        omega_v = f_omega_v(t, omega_v0, g, alpha)
    
    ## maybe to change
    return [r, omega], [r_v, omega_v]
    
    
    
def calculate_runoutlength(trajectory):

    """
    # TODO: 
        - maybe extra function for incremental changes as the same logic gets used in bounces()
    """
    
    
    ####
    # Calculate the total runoutlength of the given trajectory by cumulatively summing up the difference between consecutive points

    # -IN- #    
    # trajectory; <list(<np.array(<float>, <float>, <float>)>)>:  List containing 2D arrays with xyz coordinates of successive points along the trajectory
    
    # -OUT- #
    # runoutlength; <float>:    The total distance traveled along the given trajectory
    ####


    r_x = trajectory[0]
    r_y = trajectory[1]
    r_z = trajectory[2]    
    
    # [0]: get each incremental position (dx, dy, dz) difference; [1]: stacked ontop eachother -> [[dx1, dy1, dz1], [dx2, dy2, dz2],...]
    dr = np.stack((r_x[1:]-r_x[:-1], r_y[1:]-r_y[:-1], r_z[1:]-r_z[:-1]), axis=-1)
    
    # Calculate the respective norms -> [[norm(dx1, dy1, dz1), norm(dx2, dy2, dz2), ..]
    norms = np.linalg.norm(dr, axis=1)
    
    # And sum each row cumulatively up
    runoutlength = np.cumsum(norms)
    
    return runoutlength




def detect_bounces(velocities, threshold=1e-1):

    """
    # TODO: 
        - maybe extra function for incremental changes as the same logic gets used in runoutlength()
    """
    
    
    ####
    # Detect bounces in the trajectory by looking at (rapid) velocity changes

    # -IN- #    
    # velocities; <list(<np.array(<float>, <float>, <float>)>)>:  List containing 2D arrays with v_xyz lateral velocities corresponding to successive points along the trajectory
    
    # threshold; <float>:   Every velocity change above this value will be considered to be a bounce ; Default: 1e-1 (optional)
    
    # -OUT- #
    # bounces; <np.array(<int>)>:    Indices of detect bounces
    ####


    v_x = velocities[0]
    v_y = velocities[1]
    v_z = velocities[2]    
    
    # [0]: get each incremental velocity (dv_x, dv_y, dv_z) difference; [1]: stacked ontop eachother -> [[dv_x1, dv_y1, dv_z1], [dv_x2, dv_y2, dv_z2],...]
    dv = np.vstack((v_x[1:]-v_x[:-1], v_y[1:]-v_y[:-1], v_z[1:]-v_z[:-1]))
        
    # Calculate the respective norms -> [[norm(dv_x1, dv_y1, dv_z1), norm(dv_x2, dv_y2, dv_z2), ..]
    norms = np.linalg.norm(dv, axis=0)
        
            
    # Detect bounces based on the overall velocity change 
    bounces = np.argwhere(norms > threshold).flatten()
    
    
    return bounces

   


def get_bounce_properties(r, v=None, threshold=1e-1, bounce_indices=None):

    """
    # TODO: 
        - detect contact in between?? // check with contacts?
    """
    
    
    ####
    # Return properties (flight_distance (distance between consecutive bounces) and flight_height (max relative height _gain_ from starting point)) of detected bounces
    ## Either pass already detected bounces _OR_ velocity and (optional) threshold to evaluate the properties

    # -IN- #    
    # r; <list(<np.array(<float>, <float>, <float>)>:   xyz-coordinates of successive points along trajectory (matching velocities)    
    # v; <list(<np.array(<float>, <float>, <float>)>)>: List containing 2D arrays with v_xyz lateral velocities corresponding to successive points along the trajectory
    
    # threshold; <float>:   Every velocity change above this value will be considered to be a bounce ; Default: 1e-1 (optional)
    
    # -OUT- #
    # flight_distance; <list(<float>)>: Distance (3D) between successive bounces
    # flight_height; <list(<float>)>:   Max height gain relative to bounce start point during flight
    ##                                      Well, that does not really say anything, as the max height above the surface might be the thing we want -> difficult, when surface sloped/not straight..
    ####

    #r_x = r[0]
    #r_y = r[1]
    r_z = r[2]    

    ## now: columns: xyz ; rows: coordinates
    r = np.array(r).T


    if bounce_indices is None:
        # Detect bounces based on the overall velocity change 
        bounce_indices = detect_bounces(v, threshold)
    
    # Relative height gain to starting point during flight phase
    ## when splitting (bounce_index is start _AND_ end of flight simultaneously), we only care about the coordinates _between_ split points -> 0, -1 are omitted ; .max() for each segment individually    
    max_height = np.array([array.max() for array in np.split(r_z, bounce_indices)[1:-1]])
    flight_height = max_height - r_z[bounce_indices[:-1]]
    
    # Distance between bounces (direct distance from start->end point) ; norm: row-wise -> axis=-1
    flight_distance = np.linalg.norm(r[bounce_indices[1:]] - r[bounce_indices[:-1]], axis=1)
    
    
    return flight_distance, flight_height
    
    
    
def trajectory_analysis(blockAttr, ind_v0=...):

    """
    # TODO: 
        - analyse one trajectory at a time
    
        - Rolling <-> Sliding!!
        - maybe .hdf5 input function!
        - tmax conversion?!
        
        - return dict ?!? --> WATCH OUT! used a lot through out code with indices!
    """
    
    
    ####
    # Analyse properties of trajectories and velocities as given for each body in the .hdf5 file 

    # -IN- #    
    # blockAttr; <tuple(<np.array>, <np.array>)>:                                       Containing the raw trajectories and velocites as retrieved from the .hdf5 file
    # ind_v0; <list(<int>)> / slice / ellipse:                                          Indices of trajectories to analyse (i.e. block IDs-1); Default: ... (ellipse: all trajectories)
    
    # -OUT- #
    # ( r_x, r_y, r_z, r, r0 ):                     xyz coordinates and corresponding distance from origin (2D <np.array>) for each body; r0: distance from origin to starting position (<np.array(<float>)>)
    # ( v_x, v_y, v_z, v, v0 ):                     xyz translational velocities and corresponding norms (2D <np.array>) for each body; v0: starting velocity (<np.array(<float>)>)
    # ( omega_x, omega_y, omega_z, omega, omega0 ): xyz rotational velocities and corresponding norms (2D <np.array>) for each body; omega0: starting velocity (<np.array(<float>)>)
    # ( runoutlength; <np.array(<float>)>:          Distance covered from starting position
    # bounce_properties; <dict(<str>:<list>)> ):    Properties of detected (via sudden velocity changes) bounces: 
    #       'bounce_indices'; <list(<int>)>:            Index of bounce occurence counted from inside _ONE_ trajectory
    #       'number_bounces'; <int>:                    Number of detected bounces
    #       'bounce_distance'; <list(<float>)>:         (Direct) Distance between start - end position of bounces
    #       'bounce_height'; <list(<float>)>:           Height gain during bounce flight __RELATIVE__ to starting position of bounce (not necessarily useful... -> better: height realive to underlying surface (difficult..))
    #( t; <np.array(<float>)>:                      Time corresponding to each datapoint (same for r, velocity, .. !)
    # tmax; <int> ):                                Max t in dataset converted to array index; Default conversion: 1e-3 (look in siconos simulation for io.run-options!!! might not always be 1e-3 !)
    ####
    
    
    
        # Trajectories and velocities
        
    blockTrajectories,  blockVelocities = blockAttr
    
    
        # Sort the arrays by block IDs  
    blockTrajectories = sort_array(blockTrajectories)
    blockVelocities = sort_array(blockVelocities)

    
    ## For normalization: Assumption: all blocks with different velocity start at the same starting position!!
    r_x  = blockTrajectories[ind_v0, :, 2]
    #r_x = abs(r_x - r_x[0, 0])
    r_y  = blockTrajectories[ind_v0, :, 3]
    #r_y = abs(r_y - r_y[0, 0])
    r_z  = blockTrajectories[ind_v0, :, 4]
    #r_z = r_z - r_z[0, 0]
   
   
    r = np.sqrt(r_x*r_x + r_y*r_y + r_z*r_z)
    r0 = np.sqrt(r_x[:, 0]*r_x[:, 0] + r_y[:, 0]*r_y[:, 0] + r_z[:, 0]*r_z[:, 0])
    
    
        # Velocities
        
    v_x  = blockVelocities[ind_v0, :, 2]
    v_y  = blockVelocities[ind_v0, :, 3]
    v_z  = blockVelocities[ind_v0, :, 4]
    
    v = np.sqrt(v_x*v_x + v_y*v_y + v_z*v_z)
    v0 = np.sqrt(v_x[:, 0]*v_x[:, 0] + v_y[:, 0]*v_y[:, 0] + v_z[:, 0]*v_z[:, 0])
        
    omega_x  = blockVelocities[ind_v0, :, 5]
    omega_y  = blockVelocities[ind_v0, :, 6]
    omega_z  = blockVelocities[ind_v0, :, 7]
    
    omega = np.sqrt(omega_x*omega_x + omega_y*omega_y + omega_z*omega_z)
    omega0 = np.sqrt(omega_x[:, 0]*omega_x[:, 0] + omega_y[:, 0]*omega_y[:, 0] + omega_z[:, 0]*omega_z[:, 0])
    
        # Time
        
    t = blockTrajectories[0, :, 0]
    
    ## look in siconos simulation for io.run-options!!! might not always be 1e-3
    tmax = int(t[-1]/1e-3)
    
    
        # Trajectory properties
        
    """
    ### Alternative for simultaneous computation of all runoutlengths
    # For each velocity ->[0]: get each incremental position (dx, dy, dz) difference; [1]: stacked ontop eachother -> [[[dx1_v1, dy1_v1, dz1_v1], [dx2_v1, dy2_v1, dz2_v1], ..], [[dx1_v2, dy1_v2, dz1_v2], [dx2_v2, dy2_v2, dz2_v2], ..], ..] 
    dr = np.stack((r_x[:, 1:]-r_x[:, :-1], r_y[:, 1:]-r_y[:, :-1], r_z[:, 1:]-r_z[:, :-1]), axis=-1)
    # Calculate the respective norms -> [[norm(dx1_v1, dy1_v1, dz1_v1), norm(dx2_v1, dy2_v1, dz2_v1), ..], [norm(dx1_v2, dy1_v2, dz1_v2), norm(dx2_v2, dy2_v2, dz2_v2), ..], ..]
    norms = np.linalg.norm(dr, axis=2)
    
    # And sum each row cumulatively up
    runoutlength = np.cumsum(norms, axis=1)
    
    """
    
    
    runoutlength = []
    
    bounce_properties = {'bounce_indices' : [], 'number_bounces' : [], 'bounce_distance' : [], 'bounce_height' : []}
    
    # If an ellipsis got passed: we need a way to evaluate len(ind_v0) later
    ## Workaround
    if ind_v0 is ... :
        ind_v0 = range(len(r_x))


    # Loop through each trajectory
    for trajectory_index in range(len(ind_v0)):
        
        r_trajectory = [r_x[trajectory_index], r_y[trajectory_index], r_z[trajectory_index]]
        v_trajectory = [v_x[trajectory_index], v_y[trajectory_index], v_z[trajectory_index]]
        
            # Runoutlength
        runoutlength.append(calculate_runoutlength(r_trajectory))
        
            # Bounces
        bounce_ind = detect_bounces(v_trajectory)
        bounce_properties['bounce_indices'].append(bounce_ind)
        bounce_properties['number_bounces'].append(len(bounce_ind))
        
        bounce_distance, bounce_height = get_bounce_properties(r_trajectory, bounce_indices=bounce_ind)
        bounce_properties['bounce_distance'].append(bounce_distance)
        bounce_properties['bounce_height'].append(bounce_height)

    runoutlength = np.array(runoutlength)

    
    return [r_x, r_y, r_z, r, r0], [v_x, v_y, v_z, v, v0], [omega_x, omega_y, omega_z, omega, omega0], [runoutlength, bounce_properties], [t, tmax]
    
    
    
    
    
####################################### Geometry ##############################################


def indices_calculate_normal(grid, point_index, cell_side_length=1):
    """
    # TODO:
        - also take into account the diagonal points?
        - maybe its enough to just look from the point to two other?
    """
    
    ####
    # Calculate the unit normal vector to a 3D surface (a discrete 2D heightmap/grid) at a specific point denoted by it's index
    ## This (_BASIC_) calculation assumes smoothness as we look at the (__non-diagonal__!) neighboring points to calculate the 
    ### surface's tangent and in turn the normal.
    ## Though, if the point lies on an edge/corner of the grid, we just start at the point itself.

    # -IN- #    
    # grid; <np.array(<float>)>:    A 2D grid with the height as value
    # index; [<int>, <int>]:        The corresponding index to the point where the normal vector should be calculated
    # cell_side_length; <float>:    Length of the cell's side; cell assumed to be quadratic (x_length = y_length); Default: 1

    # -OUT- #    
    # normal; <np.array([<float>, <float>, <float>])>:  The calculated unit normal to the surface at the point 
    ####
    
    
    point_j, point_k = point_index
   
    # Initialize all indices of non-diagonal neighbors: top, right, bottom, left
    neighbor_points = [[point_j-1, point_k], [point_j, point_k+1], [point_j+1, point_k], [point_j, point_k-1]]
    
        # Check if original point was on an edge/corner -> correct the index ; Complete coordinate vector
    for p, point in enumerate(neighbor_points):
        for i, index in enumerate(point):
            if index < 0:
                point[i] += 1
            # Check if index greater than max_j/max_k
            elif index > grid.shape[i]-1: 
                point[i] -= 1
            
        # Adjust grid index to 'real' coordinate
        neighbor_points[p] = [point[0]*cell_side_length, point[1]*cell_side_length]
          
        # Get z-component
        neighbor_points[p].append(grid[point[0], point[1]])  
        
    
    neighbor_points = np.array(neighbor_points)
    
    # Compute the corresponding vectors: b->t, l->r
    bt = neighbor_points[2] - neighbor_points[0]
    lr = neighbor_points[3] - neighbor_points[1]
    
    
    # Return normalized normal
    normal = np.cross(lr, bt)
    
    return normal/np.linalg.norm(normal)
    



def coordinates_calculate_normal(grid, coordinate, tl_row_coordinate, tl_column_coordinate, conversion_factor):
    """
    # TODO:
    """
    
    ####
    # Calculate the unit normal vector to a 3D surface (a discrete 2D heightmap/grid) at a specific point denoted by it's xy coordinates
    ## We work with the fact that this 3D surface gets interpolated with triangles, thus we calculate the normal to this triangle (i.e. plane)
    ## WATCH OUT: if coordinates fall on a grid point or raster line between grid points -> we try to find the next diagonal triangle by jiggling the coordinates a bit (1e-3*1/conversion_factor to not overshoot)

    # -IN- #    
    # grid; <np.array(<float>)>:        A 2D grid with the height as value
    # coordinate; [<float>, <float>]:   The corresponding index to the point where the normal vector should be calculated
    # conversion_factor; <float>:       Conversion between grid (pixel) and coordinate system: COORDINATES * CONVERSION_FACTOR = PIXEL

    # -OUT- #    
    # normal; <np.array([<float>, <float>, <float>])>:  The calculated unit normal to the surface at the point 
    ####
    
    triangle_vertices = []
    
    # Get interpolated triangle of closest grid points at coordinate
    triangle_indices = get_closest_triangle(coordinate, tl_row_coordinate, tl_column_coordinate, conversion_factor)
        
    # Assemble coordinates
    for index in triangle_indices:
        xy = get_coordinate(index, tl_row_coordinate, tl_column_coordinate, conversion_factor)
        triangle_vertices.append(np.array(xy + [float(grid[index[0], index[1]])]))


    # Get vectors from one vertex to the other vertizes
    surface_vector_1 = np.array(triangle_vertices[2]) - np.array(triangle_vertices[0])
    surface_vector_2 = np.array(triangle_vertices[1]) - np.array(triangle_vertices[0])
    
    
    # Calculate normal
    normal = np.cross(surface_vector_1, surface_vector_2)
    
    # Normal should always point upwards (+z)
    if normal[2] < 0:
        normal = -normal        


    # Return normalized normal
    return normal/np.linalg.norm(normal)





def calculate_rotation_quaternion(start_vector, end_vector, precision=1e-4):
    """
    # TODO:
        - check if convention fits for quternions !!!
        - maybe https://en.wikipedia.org/wiki/Euler%E2%80%93Rodrigues_formula#Connection_with_quaternions better?!
    """
    
    ####
    # Calculate the quaternion (X, Y, Z, W) for a rotation of start_vector to end_vector
    ## Ref: https://stackoverflow.com/questions/1171849/finding-quaternion-representing-the-rotation-from-one-vector-to-another
    
    # -IN- #    
    # start_vector; <np.array(<float>)>:    The start orientation (3D vector)
    # end_vector; <np.array(<float>)>:      The end orientation (3D vector)

    # -OUT- #    
    # quaternion; <np.array(<float>)>:  The calculated unit quaternion (X, Y, Z, W) for a rotation from end_vector to start_vector
    ####
    
    
    # Normalize the input vector
    start_vector = start_vector/np.linalg.norm(start_vector)
    end_vector = end_vector/np.linalg.norm(end_vector)
    
   
    # If the vectors are opposite: a 180 deg rotation around an orthogonal vector will do the trick
    dot_product = np.dot(start_vector, end_vector)
    if dot_product < -(1-precision):
        
        # Try the x-axis for orthogonality
        euler_axis = np.cross(start_vector, [1, 0, 0])
        
        # If that did not work -> y-axis will
        if np.linalg.norm(euler_axis) < 1e-7:
            euler_axis = np.cross(start_vector, [0, 1, 0])
            
        euler_axis = euler_axis/np.linalg.norm(euler_axis)
                        
        return np.array([np.pi] + euler_axis.tolist() )
    
    
    # Calculate Euler axis (axis to rotate around)
    euler_axis = np.cross(start_vector, end_vector).tolist()
    
    # Assemble the quaternion and return the normalized vector
    quaternion = np.array([1 + dot_product] + euler_axis)
    
    
    return quaternion/np.linalg.norm(quaternion)



def composite_rotation_quaternion(first_rotation, second_rotation):
    """
    # TODO:
    """
    
    ####
    # Calculate the composite rotation for two quaternions (W, X, Y, Z) 
    ## https://en.wikipedia.org/wiki/Euler%E2%80%93Rodrigues_formula#Connection_with_quaternions
    # quaternions might need to be unit!(?)
    
    # -IN- #    
    # first_rotation; <np.array(<float>)>:  The first rotation (unit quaternion)
    # second_rotation; <np.array(<float>)>: The second rotation (unit quaternion)

    # -OUT- #    
    # quaternion; <np.array(<float>)>:  The resulting unit quaternion (W, X, Y, Z) for the composite rotation 1 -> 2
    ####

    w1, x1, y1, z1 = first_rotation
    w2, x2, y2, z2 = second_rotation

    w = w1*w2 - x1*x2 - y1*y2 - z1*z2
    x = w1*x2 + x1*w2 - y1*z2 + z1*y2
    y = w1*y2 + y1*w2 - z1*x2 + x1*z2
    z = w1*z2 + z1*w2 - x1*y2 + y1*x2

    resulting_quaternion = [w, x, y, z]

    return resulting_quaternion/np.linalg.norm(resulting_quaternion)
    


def convex_distribution(points):

    """
    # TODO: 
        - how similar is this to k-means/clustering analysis with k=1? --> existing theory/evaluation?
        --> distribution, centroid, connectivity based clustering
        - centroid last cluster / probabilistic mean ??
    """
    
    
    ####
    # Iteratively get all 2D convex shapes (vertices therof) including all, all\old_vertices, ... points and corresponding percentage of total points _OUTSIDE_ this shape ('percentile').
    ## Get areas of spatial distribution corresponding to certain tightness around the last cluster's centroid / probabilistic mean / original cluster's center (?) / expectation
    ## We don't assume equal distribution inside the original_cluster convex shape (--> would be Polygon centroid)! ; This is more like a 'median' centroid (NOT a geometric median though!)
    ## Assumption: Distribution has _ONE_ peak (which we want to coincide with the extracted endposition)
    ## _WATCHOUT_: Projection of all points onto xy-plane ! z component ignored!
    
    
    # -IN- #    
    # points; <np.array(<float>, <float>, <float>)>:    3D (x, y, z) coordinates of points to analyse.


    # -OUT- #
    # Dictionary containing: 'distribution': distribution_areas ; 'centroid': centroid
    
    # distribution_areas; <list(<dict('hull':<shapely.Polygon>, 'percentile':<float>)>)>:   All iteratively created distribution areas (convex polygons) with the corresponding fraction of points _OUTSIDE_ this area (measurement of how centered/tight around the last cluster's centroid / probabilistic mean this hull is)
    # centroid; <shapely.Point>:                                                            Centroid of the last cluster / probabilistic mean of the original cluster
    ####


    from shapely.geometry import MultiPoint

    # Ignore the z component
    ## We want a 2D convex hull later !
    ## Assumption: 'monotaneous' surface (no caves, etc..), no need to look at z separatly
    points = points[:, :2]    
    
    # Use shapely classes
    cluster = MultiPoint(points)
    
    # Total number of points
    cluster_original_size = len(cluster.geoms)
    
    # Centroid of cluster (Point-wise, not Polygon-wise !)
    ## This should be the 'probabilistic mean' / expected value
    ## TODO: TOCHECK! does this make sense? is this a valid measurement? 
    cluster_centroid = cluster.centroid
    
    
    distribution_areas = []
    
    
    # Initial size of cluster
    cluster_size = cluster_original_size
    
    # Iteratively create convex shapes around points excluding the old vertices
    ## Watch out! The original convex_hull Polygon centroid [weighted by area] != centroid of cluster [weighted only by points] == 'median' (kind of) over all points !
    ## We don't care about a single point or a line !
    while cluster_size > 2:
    
        # Create convex hull using some points as vertices
        hull = cluster.convex_hull
        
        # Fraction of points _OUTSIDE_ this hull / between this shape and the exterior boundary of the original cluster
        ## Measurement of how centered/tight around the cluster's centroid/probabilistic mean this hull is
        percentile = 1 - cluster_size/cluster_original_size
    
    
        distribution_areas.append({'hull' : hull, 'percentile' : percentile})
        
        # Update centroid of new cluster (Point-wise, not Polygon-wise !)
        cluster_centroid = cluster.centroid
        
        
            # Update the point set
            
        # Remove all points used as previous vertices 
        cluster = cluster.difference(hull.exterior)
    
        # We might get only one Point or even an Empty Point, both cases are irrelevant
        if isinstance(cluster, MultiPoint):
            cluster_size = len(cluster.geoms)
        else:
            cluster_size = 0
    
    
    return {'distribution' : distribution_areas, 'centroid' : cluster_centroid}




def get_closest_triangle(coordinate, tl_row_coordinate, tl_column_coordinate, conversion_factor, recursive_loop=0):
    """
    # TODO: 
        - handle recursive loop break correctly
    """
    
    
    ####
    # Get the closest three grid points to the input position (row, column)
    ## Assumption: equidistant grid spacing
    ## WATCH OUT: coordinates must not fall on grid vertex or raster line between two vertizes as the 'closest' triangle is not well defined then!

    # -IN- #    
    # coordinate; <list(<float>, <float>)>: Coordinate in original coordinate system (row, column)
    # tl_row_coordinate; <int>:             Coordinate in original coordinate system of the top left corner's row to adjust center (0, 0) to top left corner of array [0, 0].
    # tl_column_coordinate; <int>:          Coordinate in original coordinate system of the top left corner's column to adjust center (0, 0) to top left corner of array [0, 0].
    # conversion_factor; <float>:           Conversion between grid (pixel) and coordinate system: COORDINATES * CONVERSION_FACTOR = PIXEL
    
    # recursive_loop; <int>:                Breaking condition for recursive loop; Default:0 , will be adjusted with each recursive execution ; breaks at 10 (DO NOT SET MANUALLY)
    
    
    # -OUT- #
    # coordinate; <list(<int>, <int>)>: Nearest three corresponding index pairs to given coordinates (tl, br, bl/tr corners)
    ####
    
    
    indices = []
    
    # Conversion to row, column values
    converted_coordinates = [abs((coordinate[0]- tl_row_coordinate) * conversion_factor), abs((coordinate[1]- tl_column_coordinate) * conversion_factor)]
        
    # Top left corner
    indices.append([int(converted_coordinates[0]), int(converted_coordinates[1])])
                    
    # Bottom right corner
    indices.append([int(np.ceil(converted_coordinates[0])), int(np.ceil(converted_coordinates[1]))])
   
   
        # Third corner
        
    row_dezimals = converted_coordinates[0] - int(converted_coordinates[0])
    column_dezimals = converted_coordinates[1] - int(converted_coordinates[1])
   
    # Bottom left corner ; row > column 
    if row_dezimals > column_dezimals:
        indices.append([int(np.ceil(converted_coordinates[0])), int(converted_coordinates[1])])
    # Default: Top right corner ; row <= column
    else:
        indices.append([int(converted_coordinates[0]), int(np.ceil(converted_coordinates[1]))])
   
    
    # If coordinates fall on grid vertex or raster line between vertices: triangle not well defined --> jiggle a bit asymetric and try recursively until we get a valid triangle
    ## WATCH OUT: will break indexing if point at bottom/right edge of grid!
    if not len(np.unique(indices, axis=0)) == 3 and recursive_loop < 10:
        indices = get_closest_triangle([coordinate[0] + 1e-4*1/conversion_factor, coordinate[1] + 1e-3*1/conversion_factor], tl_row_coordinate, tl_column_coordinate, conversion_factor, recursive_loop=recursive_loop+1)
    
    
    return indices
    
    
    

def get_coordinate(index, tl_row_coordinate, tl_column_coordinate, conversion_factor):
    """
    #TODO:
    """

    ####
    # Return corresponding coordinate (x, y) to given array index (row, column)

    # -IN- #    
    # coordinate; <list(<float>, <float>)>: Coordinate in array index system (row, column)
    # tl_row_coordinate; <int>:             Coordinate in original coordinate system of the top left corner's row to adjust center (0, 0) to top left corner of array [0, 0].
    # tl_column_coordinate; <int>:          Coordinate in original coordinate system of the top left corner's column to adjust center (0, 0) to top left corner of array [0, 0].
    # conversion_factor; <float>:           Conversion between grid (pixel) and coordinate system: COORDINATES * CONVERSION_FACTOR = PIXEL
    
    
    # -OUT- #
    # coordinate; <list(<int>, <int>)>: Coordinate to given index
    ####
    
    x = index[0]/conversion_factor + tl_row_coordinate
    y = index[1]/conversion_factor + tl_column_coordinate
   
    return [x, y]
    
    

def intersection_line_triangle(start, end, triangle):
    """
    # TODO: 
    
        - nested ifs better?
        - maybe adjust tolerances isclose() ?
    """
    
    
    ####
    # Get the (unique) intersection point between a 3D simple line (only start and end position) and a 3D triangle (3 3D vertices)
    ## WATCH OUT: Touching results in None
    ##
    ## https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection

    # -IN- #    
    # start; <np.array(<float>, <float>, <float>)>:                 Start point of ray (x, y, z)
    # end; <np.array(<float>, <float>, <float>)>:                   End point of ray (x, y, z)
    # triangle; <list(<np.array()>, <np.array()>, <np.array()>)>:   Triangle to get intersection with as described by three vertices, each (x, y, z)
    

    # -OUT- #
    # intersection; <np.array(<float>, <float>, <float>)>:  The coordinates of the intersecting point ; or None if none found (line parallel to triangle, no intersection / only touching, or completely in triangle)
    ####


    
        # Setup
        
    # Line direction vector
    start_end = end - start
    
    # Defining plane vectors
    p0 = triangle[0]
    p01 = triangle[1] - p0
    p02 = triangle[2] - p0
    
    # System of linear equations
    matrix = np.array([-start_end, p01, p02])
    
    
    # (1) Check if unique solution exists (det != 0) ; otherwise line in plane defined by triangle or parallel to it
    determinant = np.linalg.det(matrix)
    if np.isclose(np.zeros(determinant.shape), determinant):
        return None
    
    
    # (2) Find intersection
    # (2a) Solve for line_multiplier [i.e. t] (end = start + start_end*line_multiplier for line_multiplier == 1)
    
    p01xp02 = np.cross(p01, p02)
    dot2 = start - p0
    divisor = np.dot( -start_end, p01xp02)
    
    line_multiplier = np.dot( p01xp02, dot2) / divisor
    
    # (2b) Check if point on line segment between start-end (i.e. line_multiplier \in [0, 1])
    if not 0 <= line_multiplier <= 1:
        return None
        
                
    # (3) Check if point inside triangle
    # (3a) Check if point inside parallelgram 
    u = np.dot( np.cross(p02, -start_end), dot2) / divisor
    v = np.dot( np.cross( -start_end, p01), dot2) / divisor
    
    if not (0 <= u <= 1 and 0 <= v <= 1):
        return None
        
    
    # (3b) Check if point directly inside triangle
    if not u + v <= 1:
        return None    
    
    # Calculate intersection
    intersection = start + start_end*line_multiplier
    
    return intersection
    
    
    
def get_interpolated_height(coordinate, grid, tl_row_coordinate, tl_column_coordinate, conversion_factor):
    """
    # TODO: 
        - DOCU!
        - check for Nones from intersection_ray_triangle() ??
    """
    
    
    ####
    # Get the height at input position on the interpolated triangular surface between grid points
    # Method: pass beam from min_z of triangle (xy=coordinate, z=min_z) through triangle and get intersection
    ## Assumption: equidistant grid spacing
    ## Assumption: triangle not only vertical

    # -IN- #    
    # coordinate; <list(<float>, <float>)>: 2D coordinate in original coordinate system corresponding to (row, column) for grid
    # grid; <np.array(<float>)>:            Grid containing z values, coordinate lies inside the area covered
    # tl_row_coordinate; <int>:             Coordinate in original coordinate system of the top left corner's row to adjust center (0, 0) to top left corner of array [0, 0].
    # tl_column_coordinate; <int>:          Coordinate in original coordinate system of the top left corner's column to adjust center (0, 0) to top left corner of array [0, 0].
    # conversion_factor; <float>:           Conversion between grid (pixel) and coordinate system: COORDINATES * CONVERSION_FACTOR = PIXEL


    # -OUT- #
    #
    ####


    triangle_vertices = []
    
    # Get interpolated triangle of closest grid points at coordinate
    triangle_indices = get_closest_triangle(coordinate, tl_row_coordinate, tl_column_coordinate, conversion_factor)
            
                
    # Assemble coordinates
    for index in triangle_indices:
        xy = get_coordinate(index, tl_row_coordinate, tl_column_coordinate, conversion_factor)
        triangle_vertices.append(np.array(xy + [float(grid[index[0], index[1]])]))


        # Create ray from coordinate with min_z of triangle to max_z of triangle

    # Add point we are sure lies below/in triangle surface (z=min_z)
    min_z = np.amin(np.array(triangle_vertices)[:, 2])
    start = np.array([*coordinate, min_z])
    
    # Add point we are sure lies above/in triangle surface (z=max_z)
    end = np.array([*coordinate, np.amax(np.array(triangle_vertices)[:, 2])])

    # We might stumble across a flat surface --> min_z == max_z == z (start == end as well)
    if np.isclose(end, start, rtol=1e-8, atol=1e-10).all():
        return min_z

    # Get intersection
    ## We don't expect any Nones here and don't check!
    intersection = intersection_line_triangle(start, end, triangle_vertices)
    
    # return z component
    return intersection[-1]



####################################### Statistics ##############################################


def describe_dataset(data, whis=[5, 95]):

    """
    # TODO: 
        - SPearman corrcoef as well !
    
        - naming!
        - confidence intervals around median?
        - rather whiskers to min/max (--> whis=(0, 100)) than extra def for outliers?
    """
    
    
    ####
    # Get statistics concerning the inherent variation of a dataset with multiple (k) parameters
    # Return mean, sample_variance, median, quantiles and extremes for each parameter
    ## whiskers set to 5th, 95th percentile per default! (Tukey's default: 1.5*IQR)

    # -IN- #    
    # data; <np.array(<float>)>:    The dataset to be analysed (row: value, column: parameter)
    
    # whis; <float> or <list()>:    Definition of the whiskers --> outliers ; Default: [5, 95] i.e. at 5th and 95th percentile (optional)
    
    # -OUT- #
    # dictionary containing:
    # mean, sample_variance, quartile 1 (25th percentile), quartile 3 (75th percentile); <list(<float>)>
    # (possible) outliers (outside whiskers, i.e. lying outside the 5th or 95th percentile); <list(<np.array(<float>)>)>
    # correlation_matrix; <np.array(<float>)>/<float>:  Correlation coefficients (Pearson) for whole dataset
    ####

    
    #import statistics
    import matplotlib
    

    _, k = data.shape
    
        # Calculate especially means and sample variance of data
    mean = []
    sample_variance = []
    
    median = []
    q1 = []
    q3 = []
    fliers = []
    
    min_data = []
    max_data = []
    
    # Look at each parameter individually
    for j in range(k):
    
        data_j = data[:, j]
        
            # Get statistics about dataset (originally for box-plotting)
        # https://matplotlib.org/stable/api/cbook_api.html#matplotlib.cbook.boxplot_stats
        ## whis=[5, 95] : get 5th and 95th percentiles
        stats_j = matplotlib.cbook.boxplot_stats(data_j, whis=whis)[0]

            # Mean
        mean_j = stats_j['mean']
        mean.append(mean_j)
               
        
            # Sample variance 
        if len(data_j) > 1:
            #sample_variance.append(statistics.variance(data_j, mean_j))
            sample_variance.append(np.var(data_j))
        else:
            sample_variance.append(np.NaN)
        
            # Various other statistics
        median.append(stats_j['med'])
        q1.append(stats_j['q1'])
        q3.append(stats_j['q3'])
        # Sort fliers first!
        fliers.append(np.sort(stats_j['fliers']).tolist())
        
        min_data.append(np.amin(data_j))
        max_data.append(np.amax(data_j))
        
        ## maybe cilo, cihi (confidence interval for median, notched boxes) as well? https://sci-hub.se/https://doi.org/10.2307/2683468 : here assuming gaussian-like shape around 'middle portion'
        
    # We have row: values, column: parameters
    correlation_matrix = np.corrcoef(data, rowvar=False).tolist()
    
            
    return {'mean' : mean, 'sample_variance' : sample_variance, 'median' : median, 'q1' : q1, 'q3' : q3, 'fliers' : fliers, 'min' : min_data, 'max' : max_data, 'correlation' : correlation_matrix}
    
    


def univar_sci(x, y, alpha=0.05):

    """
    # TODO: 
        - sample variance (own implementation?)
    """
    
    
    ####
    # Calculate the simultaneous confidence intervals (sci) for comparisons between n predictions of k parameters (x, model) and N observations of the same k parameters (y, system)
    ## We have dependend output data (due to trace-driven nature [same inputs for model, system])
    ## Ref: Osman Balci & Robert G. Sargent (1984) Validation of Simulation Models via Simultaneous Confidence Intervals, American Journal of Mathematical and Management Sciences, 4:3-4, 375-406, DOI: 10.1080/01966324.1984.10737151

    # Technique IV
    ## total confidence: >= 1 - (sum_j^k alpha_m,j) - (sum_j^k alpha_s,j) !
    
    # -IN- #    
    # x; <np.array(<float>)>: Model data points (2D array: n,k)
    # y; <np.array(<float>)>: System data points (2D array: N,k)
    
    # alpha; <float> < 1:                       1 - (total !) confidence for two sided percentage point of t-distribution; Default: 0.05 -> 95% confidence (optional)
    
    # -OUT- #
    # sci; <list(<tuple(<float>, <float>)>)>:   Simultaneous confidence intervals for each parameter at total confidence 1-alpha
    ####
    
    
    from scipy import stats
        
    n, k = x.shape
    N, _ = y.shape
    
        # Calculate means and sample variance of observations
    x_stats = describe_dataset(x)
    y_stats = describe_dataset(y)
    
    x_mean, x_sample_variance = x_stats['mean'], x_stats['sample_variance']
    y_mean, y_sample_variance = y_stats['mean'], y_stats['sample_variance']
    
    
        # Technique IV
        # CI_j = (x_(mean, j) - y_(mean, j)) +- ( t_(alpha_(m,j)/2, N-1) sqrt( (S_(m,j))² /n) + t_(alpha_(s,j)/2, N-1) sqrt( (S_(s,j))² /N) )
        
    sci = []
    
    # We assume same confidence for all parameters
    ## total confidence: >= 1 - (sum_j^k alpha_m,j) - (sum_j^k alpha_s,j) !
    alpha_j = alpha/k /2
    
    # Student's t factor
    x_t = abs(stats.t.ppf(alpha_j/2, n-1))
    y_t = abs(stats.t.ppf(alpha_j/2, N-1))
    
    for j in range(k):
    
        sci_len = x_t * np.sqrt(x_sample_variance[j]*x_sample_variance[j]/n) + y_t * np.sqrt(y_sample_variance[j]*y_sample_variance[j]/N)
        
        mean_difference = x_mean[j] - y_mean[j]
        
        sci.append((mean_difference - sci_len, mean_difference + sci_len))


    return sci
    
    
    
def univar_paired_sci(x, y, alpha=0.05):

    """
    # TODO: 
        - sample variance (own implementation?)
    """
    
    
    ####
    # Calculate the simultaneous confidence intervals (sci) for comparisons between N predictions of k parameters (x, model) and N observations of the same k parameters (y, system)
    ## We have dependend output data (due to trace-driven nature [same inputs for model, system])
    ## Ref: Osman Balci & Robert G. Sargent (1984) Validation of Simulation Models via Simultaneous Confidence Intervals, American Journal of Mathematical and Management Sciences, 4:3-4, 375-406, DOI: 10.1080/01966324.1984.10737151

    # Technique III
    ## total confidence: >= 1 - (sum_j^k alpha_j) !
    
    # -IN- #    
    # x; <np.array(<float>)>: Model data points (2D array: N,k)
    # y; <np.array(<float>)>: System data points (2D array: N,k)
    
    # alpha; <float> < 1:                       1 - (total !) confidence for two sided percentage point of t-distribution; Default: 0.05 -> 95% confidence (optional)
    
    # -OUT- #
    # sci; <list(<tuple(<float>, <float>)>)>:   Simultaneous confidence intervals for each parameter at total confidence 1-alpha
    ####
    
    
    from scipy import stats
        
    N, k = x.shape
    
        # Calculate means and sample variance of differences of paired observations
    stats = describe_dataset(x-y)
    
    difference_mean, sample_variance = stats['mean'], stats['sample_variance']
        
    
        # Technique III
        # CI_j = d_(mean, j) +- t_(alpha_j/2, N-1) sqrt( (S_(d,j))² /N)
        
    sci = []
    
    # We assume same confidence for all parameters
    ## total confidence: >= 1 - (sum_j^k alpha_j) !
    alpha_j = alpha/k
    
    # Student's t factor
    t = abs(stats.t.ppf(alpha_j/2, N-1))
    
    for j in range(k):
    
        sci_len = t * np.sqrt(sample_variance[j]*sample_variance[j]/N)
    
        sci.append((difference_mean[j] - sci_len, difference_mean[j] + sci_len))


    return sci



def multivar_paired_sci(x, y, alpha=0.05):

    """
    # TODO: 
        - (sample?) pooled variance-covariance matrix
        - hotelling squared ppf ???
    """
    
    
    ####
    # Calculate the simultaneous confidence intervals (sci) for comparisons between N predictions of k parameters (x, model) and N observations of the same k parameters (y, system)
    ## We have dependend output data (due to trace-driven nature [same inputs for model, system])
    ## Ref: Osman Balci & Robert G. Sargent (1984) Validation of Simulation Models via Simultaneous Confidence Intervals, American Journal of Mathematical and Management Sciences, 4:3-4, 375-406, DOI: 10.1080/01966324.1984.10737151


    # Technique VI
    ## total confidence: = 1 - alpha
    
    # -IN- #    
    # x; <np.array(<float>)>: Model data points (2D array: N,k)
    # y; <np.array(<float>)>: System data points (2D array: N,k)
    
    # alpha; <float> < 1:                       1 - (total !) confidence for two sided percentage point of t-distribution; Default: 0.05 -> 95% confidence (optional)
    
    # -OUT- #
    # sci; <list(<tuple(<float>, <float>)>)>:   Simultaneous confidence intervals for each parameter at total confidence 1-alpha
    ####
    
    
    from scipy import stats
        
    N, k = x.shape
    
        # Calculate means and sample variance of differences of paired observations
    stats = describe_dataset(x-y)
    
    difference_mean, sample_variance = stats['mean'], stats['sample_variance']
    
        # Technique VI
        # CI_j = d_(mean, j) +- t_(alpha_j/2, N-1) sqrt( (S_d)² /N)
        
    sci = []
    
    # We assume same confidence for all parameters
    ## total confidence: 1 - (sum_j^k alpha_j) !
    alpha_j = alpha/k
    
    # Student's t factor
    t = abs(stats.t.ppf(alpha_j/2, N-1))
    
    for j in range(k):
    
        sci_len = t * np.sqrt(sample_variance[j]*sample_variance[j]/N)
    
        sci.append((difference_mean[j] - sci_len, difference_mean[j] + sci_len))


    ###### not ready!
    return None
    
    

def joint_confidence(x, y, alpha=0.05, accuracy=None):

    """
    # TODO: 
        - jcr!
        - multivariate!
        - accuracy check!
        
        - safety checks?
    """
    
    
    ####
    # Calculate the simultaneous confidence intervals / joint confidence region for comparisons between n predictions of k parameters (x, model) and N observations of the same k parameters (y, system)
    ## n==N needed for paired comparison: x,y ordered to be paired observations!
    ## Ref: Osman Balci & Robert G. Sargent (1984) Validation of Simulation Models via Simultaneous Confidence Intervals, American Journal of Mathematical and Management Sciences, 4:3-4, 375-406, DOI: 10.1080/01966324.1984.10737151
    ## WATCH OUT: t-distribution assumed here!!
    
    
    ## As we have dependend output data (due to trace-driven nature [same inputs for model, system]), Techniques III/IV (univariate), VI/VII (multivariate), IX (joint confidence region [no accuracy, i.e. desired range of confidence intervals, given]) are of interest.
    ## WATCH OUT: really trace-driven? we approximate v_0 and shape by random allocations!
    ## BUT simulation - observation not independent (taken from the same distribution) either...!

    # -IN- #    
    # x; <array-like(<float>)>: Model data points (2D array: n,k)
    # y; <array-like(<float>)>: System data points (2D array: N,k)
    
    # alpha; <float> < 1:                       1 - confidence for two sided percentage point of t-distribution; Default: 0.05 -> 95% confidence (optional)
    # accuracy; <array-like(<float>, <float>)>: Acceptable range of confidence interval (e.g. -0.035 < x_mean[0] - y_mean[0] < 0.035 : (-0.035, 0.035))
    
    # -OUT- #
    # 
    ####
    
    
    n, _ = x.shape
    N, _ = y.shape

        
        # Joint confidence region (jcr)
    if accuracy is None:
        jcrs = []
        ...
        return
        
        # Simultaneous confidence intervals (sci)
    else:
        scis = []
        
        # Paired observations possible
        if n == N:
            # Technique III
            scis.append(univar_paired_sci(x, y, alpha=alpha))
            # Technique VI
            #scis.append(multivar_paired_sci(x, y, alpha=alpha))
            
        else:
            # Technique IV
            scis.append(univar_sci(x, y, alpha=alpha))
            # Technique VII
            #scis.append(multivar_sci(x, y, alpha=alpha))
    

    
####################################### Plotting ################################################

     
def set_axes_equal(ax):
    ### Helper function for nice/easy 3D plotting
    ## https://stackoverflow.com/questions/13685386/matplotlib-equal-unit-length-with-equal-aspect-ratio-z-axis-is-not-equal-to
    
    """Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    """

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])
    
    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])
    
    
    
def plot_polygon_exterior(ax, polygon, color='black', label=None, dimension=2, z=0):

    """
    # TODO: 
    """
    
    
    ####
    # Plot exterior boundary of given polygon
    
    
    # -IN- #    
    # ax; <matplotlib.pyplot.Axes>: Axes to plot the exterior polygon boundary on
    # polygon; <shapely.Polygon>:   Polygon to plot exterior boundary of
    
    # color; <matplotlib.color>:    Color (matplotlib compatible) for the boundary ; Default: black (optional)    
    # label; <str>:                 Label for the boundary plot ; Default: None (optional)
    # dimension; <int>:             2 OR 3 dimensional plot ; Default: 2 (optional)
    # z; <float>:                   z value to append to ALL 2D points if dimension=3 set ; Default: 0 (optional)


    # -OUT- #
    # None
    ####
    
    exterior_coords = np.asarray(polygon.exterior.coords)
    
    # Add an additional z value to each xy coordinate for 3D plot
    if dimension == 3:
        exterior_coords = np.concatenate([exterior_coords, z*np.ones((exterior_coords.shape[0], 1))], axis=1)
    
    ax.plot(*exterior_coords.T, color=color, label=label)
    
    return None



def convex_distribution_plot(distribution_areas, centroid, points, dimension=2, percentiles=None, equal_axes=True):

    """
    # TODO: 
    """
    
    
    ####
    # Plot the convex distribution polygons extracted from points cluster via convex_distribution()
    
    
    # -IN- #    
    # distribution_areas; <dict(<str>:<shapely.Polygon>, <str>:<float>)>:   All extracted distribution areas ['hull'] as shapely Polygon objects as well as the corresponding percentile ['percentile']
    # centroid; <shapely.Point>:                                            The centroid (xy) of the analysed cluster [WATCH OUT: z coordinate for 3D plotting adjusted to mean_z(points) for better visualization!]
    # points; <np.array(<float>, <float>, <float>)>:                        Points of analysed cluster (xy[z for 3D])
    
    # dimension; <int>:                                                     2 OR 3 dimensional plot
    # equal_axes; <bool>:                                                   Toggel whether set_axes_equal() should be called already as this permanently changes the limits for each axis such that taking additional data into account is not easily possible later ; Default: True (optional)

    # -OUT- #
    # fig, ax:  Figure and axes objects generated
    ####
    
    
    # Get centroid coordinates
    centroid = np.asarray(centroid.coords)[0]
    
            # Typical plot setup for 3D or 2D
    fig = plt.figure()

    if dimension == 3:
        
        from scipy.stats import norm
        
        ax = fig.add_subplot(projection='3d')

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('\'percentile\'')#'z')
       
        # Plot polygon boundaries with increasing visibility (esp. alpha value of RGBA color)
        total_number = len(distribution_areas)+1
        max_z = np.amax(points, axis=0)[-1]
        min_z = np.min(points, axis=0)[-1]
        
        for d, distribution in enumerate(distribution_areas):
            percentile = distribution['percentile']
        
            color = (0.1, 0.2, percentile, 0.1 + (0.9)*percentile)#(0.1, 0.2, d/total_number, (d+1)/total_number)
            plot_polygon_exterior(ax, distribution['hull'], label=percentile, color=color, dimension=dimension, z=percentile)#*(max_z - min_z) + min_z)
        
        # Scatter the original points and centroid ontop
        ax.scatter(points[:, 0], points[:, 1], points[:, 2], c='black', s=5, label='Points')
        ax.scatter(*centroid, (max_z + min_z)/2, c='red', s=10, label=f'Centroid: {centroid}')
        
        x = np.linspace(np.amin(points[:, 0]), np.amax(points[:, 0]), 1000)
        normpdf = norm.pdf(x, 0, 1)
        ax.plot(x, np.zeros(x.shape), normpdf/max(normpdf), c='orange')
        
        # Adjust axis to equal spacing between ticks
        #if equal_axes: set_axes_equal(ax)
        
    elif dimension == 2:
        ax = fig.add_subplot()

        ax.set_xlabel('x')
        ax.set_ylabel('y')
       
          
        # Plot polygon boundaries with increasing visibility (esp. alpha value of RGBA color)
        total_number = len(distribution_areas)+1
        
        for d, distribution in enumerate(distribution_areas):
            percentile = distribution['percentile']
        
            color = (0.1, 0.2, percentile, 0.1 + (0.9)*percentile)#(0.1, 0.2, d/total_number, (d+1)/total_number)
            plot_polygon_exterior(ax, distribution['hull'], label=percentile, color=color, dimension=dimension)
        
        ax.scatter(points[:, 0], points[:, 1], c='black', s=5, label='Points')
        ax.scatter(*centroid, c='red', s=10, label=f'Centroid: {centroid}')
        
        ax.set_aspect('equal')
            
    return fig, ax
    


def plot_start_setting(mX, mY, mZ, blocks, soil):

    """
    # TODO: 
        - better distinction?!
        - toggle overlay?
        
        - Does not plot the last column?! -- seems to be a deeply rooted matplot issue? --> causes offset between trajectories (when plotted) and starting patches ; Maybe tiles == nodes here ?
    """
    
    
    ####
    # Plot height grid / paraboloidic-like crater and block trajectories with colored areas for different soil types and block starting positions
    # ref. https://stackoverflow.com/questions/7855229/how-to-make-a-4d-plot-using-python-with-matplotlib
    # https://matplotlib.org/stable/tutorials/colors/colormaps.html

    # -IN- #    
    # mX; <np.array(<float>)>:                              X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>:                              Y-axis grid (2D matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>:                              Z-axis grid / height grid (2D matrix), same shape 
    # blocks; <tuple(<blocksParameters, blocksGrid>)>:      _see below_
    # soil; <tuple(<soilParameters, soilGrid>)>:            _see below_
    #
    # blocksParameters; <list(<dic>)>:                      All input blocks parameters
    # mBlocksType; <list(<list(<list(<float>)>)>)>:         Grid with blocks start positions
    # soilParameters; <list(<dic>)>:                        All input soil parameters
    # mSoilType; <list(<list(<list(<float>)>)>)>:           Grid with soil grid
    
    # -OUT- #
    # fig; <matplotlib.pyplot.figure>:  3D figure
    # ax; <matplotlib.pyplot.3dAxes>:   Corresponding 3d Axes
    ####
            
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from matplotlib import cm, colors     
                    
    
    blocksParameters, mBlocksType = blocks
    soilParameters, mSoilType = soil
        
        
        # Typical 3D color plot setup
    fig = plt.figure()

    ax = fig.add_subplot(projection='3d')

    ax.set_xlabel('$x$ in m')
    ax.set_ylabel('$y$ in m')
    ax.set_zlabel('$z$ in m')
   
          
        # Offset blocksType (where set) by maxSoil to get unique values for each segment (combining soil _AND_ block starting position) type
    blocksMask = np.argwhere(mBlocksType > 0)
    maxSoil = np.amax(mSoilType)
    
    ## Watch out!! .copy() needed, otherwise it will change the original grid! 
    colorGrid = mSoilType.copy()
    colorGrid[blocksMask[:, 0], blocksMask[:, 1]] = mBlocksType[blocksMask[:, 0], blocksMask[:, 1]] + maxSoil
    
        # Setup up the cmap used for coloring the 3D heightmap acording to their type (soil type / block_starting patch)
    maxColor = np.amax(colorGrid)
        
    norm = colors.Normalize(vmin=0, vmax=maxColor, clip=True)
    mapper = cm.ScalarMappable(norm=norm, cmap=cm.hsv)#Greys_r)tab20c)#
    
        # Create a legend for the cmap
    labels = []
    handles = []
    
    if not isinstance(soilParameters, list):
        soilParameters = [soilParameters]
    if not isinstance(blocksParameters, list):
        blocksParameters = [blocksParameters]
    
    joinedParameters = soilParameters + blocksParameters
    
    # 0 is a default value (no soil type set) -> [1:]
    for labelInd in np.unique(colorGrid)[1:]:
        labelInd = int(labelInd)
        # Create 'fake' Line2D object to later stick a label (here: parameters) to
        handles.append(mpl.lines.Line2D([0],[0], linestyle='none', color=mapper.to_rgba(labelInd), marker='o'))
        
        # -1 needed as values in color grid for features start at 1
        labels.append(joinedParameters[labelInd-1])
        
      
    # Plot underlying heigth map with _real_ number of segments - no interpolation (rstride, cstride = 1, 1)
    ## WATCH OUT: plot_surface interpolates _between_ grid points! -> n-1 segments for n columns !
    #ax.plot_surface(mX, mY, mZ, facecolors=mapper.to_rgba(colorGrid), rstride=1, cstride=1, alpha=0.75)#, #rcount=rowsCount, ccount=columnsCount, alpha=0.75) # shade=False, 
    ax.plot_surface(mX, mY, mZ, color='yellow', shade=True, rstride=1, cstride=1, alpha=0.75)
    
    # Adjust axis to equal spacing between ticks
    set_axes_equal(ax)
        
    #ax.legend(handles, labels)
           
           
    return fig, ax
    
    
    
    
## TODO
def visualize(mX, mY, mZ, blocks, soil, trajectories):

    """
    # TODO: 
    """
    
    
    ####
    # Plot height grid / paraboloidic-like crater and block trajectories with colored areas for different soil types and block starting positions, and trajectories of .hdf5
    # ref. https://stackoverflow.com/questions/7855229/how-to-make-a-4d-plot-using-python-with-matplotlib
    # https://matplotlib.org/stable/tutorials/colors/colormaps.html

    # -IN- #    
    # mX; <np.array(<float>)>:                              X-axis grid (2D matrix), shape adjusted for Y-axis
    # mY; <np.array(<float>)>:                              Y-axis grid (2D matrix), shape adjusted for X-axis
    # mZ; <np.array(<float>)>:                              Z-axis grid / height grid (2D matrix), same shape 
    # blocks; <tuple(<blocksParameters, blocksGrid>)>:      _see below_
    # soil; <tuple(<soilParameters, soilGrid>)>:            _see below_
    #
    # blocksParameters; <list(<dict>)>:                     All input blocks parameters
    # mBlocksType; <list(<list(<list(<float>)>)>)>:         Grid with blocks start positions
    # soilParameters; <list(<dict>)>:                       All input soil parameters
    # mSoilType; <list(<list(<list(<float>)>)>)>:           Grid with soil grid
    #
    # trajectories; <np.array(float)>:                      Trajectories as extracted from .hdf5 including t, ID, x, y, z
    
    
    # -OUT- #
    # fig; <matplotlib.pyplot.figure>:  3D figure
    # ax; <matplotlib.pyplot.3dAxes>:   Corresponding 3d Axes
    ####

        
    fig, ax = plot_start_setting(mX, mY, mZ, blocks, soil)
       
    
    
        # Order trajectories by ID
    orderedTrajectories = []
    
    numberTraj = int(np.amax(trajectories[:, 1]))
    
    
    # t, ID, x, y, z  
    for n in range(1, numberTraj+1):
        ## hopefully _first_occurence, no shuffling!
        #### better way than .copy() ???!??!??
        orderedTrajectories.append(trajectories[np.argwhere(trajectories[:, 1] == n)].reshape(-1, 5).copy())
    
    
        # Plot
    norm = colors.Normalize(vmin=1, vmax=numberTraj, clip=True)
    mapper = cm.ScalarMappable(norm=norm, cmap=cm.tab20)
    
    handles = []
    labels = []
    
    #### TODO: COLOR CHOICE!!!
    ## hmmm lcose mubers together get not different color... e.b. 79 80 ??
    for j, trajectories in enumerate(orderedTrajectories):
        ## x,y seems to be mixed (due to siconos coordinate system definition maybe?)
        #ax.plot(trajectories[:, 3], trajectories[:, 2], trajectories[:, 4], color=mapper.to_rgba(trajectories[0, 1]), zorder=j+10)
        ax.plot(trajectories[:, 3], trajectories[:, 2], trajectories[:, 4], color='green', zorder=j+10)
    
            # Legend
        # Create 'fake' Line2D object to later stick a label (here: parameters) to
        #handles.append(mpl.lines.Line2D([0],[0], linestyle='none', color=mapper.to_rgba(trajectories[0, 1]), marker='o'))
        handles.append(mpl.lines.Line2D([0],[0], linestyle='none', color='green', marker='o'))
    
    
        # -1 needed as values in color grid for features start at 1
        labels.append('Trajectories')
        
    #ax.legend(handles[::4], labels[::4])
    #ax.legend([handles[0]], [labels[0]])
    plt.show()
        
    return fig, ax
    
    
    
    
    
    
def plot_multiboxplot_subplot(figure, data, x_label='x', y_label='y', rows=1, columns=1, position=1, graph_label=None, linestyle='solid', title=None):
    """
    # TODO:
        - Further implementation!: ticks label, already computed whiskers, etc.?...
        - Defaults for graph_label, linestyle better!
        - print values!: number of fliers, max/min, median, mean, percentiles
    """
    
    ####
    # Plot multiple boxplots with corresponding labels at a certain position in a subplot environement
    ## Ref: https://matplotlib.org/stable/gallery/statistics/boxplot_demo.html
    
    # -IN- #    
    # data; <np.array(<float>)>:    2D dataset containing the datapoints of one parameter (for multiple runs)
    
    # graph_label; <list(<list(<str>)>)> / <list(<str>)>:   List with list of strings _OR_ list of strings containing the labels for each graph; Default: None
    # linestyle; <list(<matplotlib.pyplot -> linestyles>)> 
    ##              / <matplotlib.pyplot -> linestyles>:    List of strings _OR_ string specifying the desired linestyle for a group of graphs; Default: 'solid'
    
    # figure; <matplotlib.pyplot.figure>:   Matplotlib.pyplot figure to add subplot to
    # x_label; <str>:                       x-axis label of plot; Default: 'x' (optional)
    # y_label; <str>:                       y-axis label of plot; Default: 'y' (optional)
    # rows; <int>:                          Number of total rows in subplot; Default: 1
    # columns; <int>:                       Number of total columns in subplot; Default: 1
    # position; <int>:                      Position (<= rows*columns) of this plot in grid of subplots; Default: 1
    
    
    # -OUT- #    
    # I/O: Added subplot to fig
    # 0 on success
    ####


        # Subplot setup
    ax = figure.add_subplot(rows, columns, position)
    
    ax.set_title(title)
    
    # Add a horizontal grid to the plot, but make it very light in color
    # so we can use it for reading data values but not be distracting
    ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
                   alpha=0.5)

    ax.set(
        axisbelow=True,  # Hide the grid behind plot objects
        #title='Comparison of IID Bootstrap Resampling Across Five Distributions',
        xlabel='',#x_label,
        ylabel=y_label
    )
    ax.tick_params(axis='x', bottom=False, labelsize=0)
    
        # Plot all boxplots
        
    # whis: 5th and 95th percentile
    bp = ax.boxplot(data, whis=[5, 95], showmeans=True, meanline=True)#, notch=0, sym='+', vert=1, whis=1.5)
    #plt.setp(bp['boxes'], color='black')
    #plt.setp(bp['whiskers'], color='black')
    plt.setp(bp['fliers'], color='red', marker='+') #marker='D', ms=0.1)
    
    
    if not graph_label is None: plt.legend()
    
    return 0
    
    
    
    
def plot_multigraph_subplot(figure, x, y, x_label='x', y_label='y', rows=1, columns=1, position=1, graph_label=None, linestyle='solid'):
    """
    # TODO:
        - linestyle for each graph?
        - Defaults for graph_label, linestyle better!
    """
    
    ####
    # Plot multiple graphs (same x interval) with corresponding labels and linestyles at a certain position in a subplot environement
    
    # -IN- #    
    # x; <np.array(<float>)>:                               1D array containing e.g. time
    # y; <list(<np.array(<float>)>)> / <np.array(<float>)>: List with 2D arrays _OR_ 2D arrays containing multiple graphs (rows) ; len(rows) has to match len(x) !
    
    # graph_label; <list(<list(<str>)>)> / <list(<str>)>:   List with list of strings _OR_ list of strings containing the labels for each graph; Default: None
    # linestyle; <list(<matplotlib.pyplot -> linestyles>)> 
    ##              / <matplotlib.pyplot -> linestyles>:    List of strings _OR_ string specifying the desired linestyle for a group of graphs; Default: 'solid'
    
    # figure; <matplotlib.pyplot.figure>:   Matplotlib.pyplot figure to add subplot to
    # x_label; <str>:                       x-axis label of plot; Default: 'x' (optional)
    # y_label; <str>:                       y-axis label of plot; Default: 'y' (optional)
    # rows; <int>:                          Number of total rows in subplot; Default: 1
    # columns; <int>:                       Number of total columns in subplot; Default: 1
    # position; <int>:                      Position (<= rows*columns) of this plot in grid of subplots; Default: 1
    
    
    # -OUT- #    
    # I/O: Added subplot to fig
    # 0 on success
    ####


        # Subplot setup
    ax = figure.add_subplot(rows, columns, position)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    
    
    ax.grid(alpha=0.5)
    #ax.set_ylim(bottom=0)
    #ax.set_xlim(left=0)
    ax.spines.right.set_visible(False)
    ax.spines.top.set_visible(False)
    
    
        # Plot all graphs: different for <list> or 2D <np.array>
    #try:
    
    # If y is <list>
    if type(y) is list:
        for g, graph in enumerate(y):
            for j in range(len(y[g][:, 0])): plt.plot(x, y[g][j, :], label=graph_label[g][j], ls=linestyle[g])
            
    # If y is <np.array> (2D)
    elif len(y.shape) == 2:
        for j in range(len(y[:, 0])): plt.plot(x, y[j, :], label=graph_label[j], ls=linestyle)
        
    #except:
    #    print('ERROR: Wrong y supplied')
    
    if not graph_label is None: plt.legend()
    
    return 0
    
   
    
def multigraph_labeling(parameter_name, v0, soil_params, trajectory_index):

    """
    # TODO: 
    """
    
    
    ####
    # Plot properties of trajectories and velocities as given for each body in the .hdf5 file 

    # -IN- #    
    # parameter_name; <str>:                    Latex name for the parameter to be visualized in multigraph plot
    # v0; <array-like(<float>)>:                All v0s for all trajectories (ordered by soil-type)
    # soil_params; <array-like()>:              All different soil params (not nessecarily one for each trajectory)
    # trajectory_index; <array-like(<int>)>:    What trajectories to plot (not all have to be plotted) 
    
    # -OUT- #
    # labels; <list(<str>)>:    All labels for each graph sorted by soil -> v0
    ####
    
    # Number of different v0 (each soil type has same number of different v0)
    number_v0 = len(np.unique(v0))
    # As trajectories are ordered by soil -> v0: we want the corresponding soil type for each number_v0s v0
    soil_indices = np.array(trajectory_index)//number_v0
    
    # Create unique string for each trajectory
    return [f'${parameter_name}$ at $v_0$: {vel:.2f} for {soil}' for soil, vel in zip(soil_params[soil_indices], v0)]

   
   
   
def plot_results(parameters, blockAttr=None, trajectory_index=..., r_analysis=None, v_analysis=None, omega_analysis=None, trajec_analysis=None, t_analysis=None, incline=None):

    """
    # TODO: 
        - auto-detect inclination !
    """
    
    
    ####
    # Plot properties of trajectories and velocities as given for each body in the .hdf5 file 

    # -IN- #    
    # parameters; <tuple(<list(<dict(<str>:<str>)>)>, <list(<dict(<str>:<str>)>)>)>:    Block parameters and soil parameters as retrieved from the corresponding .csv files
    
    # blockAttr; <tuple(<np.array>, <np.array>)>:                                       Raw trajectories and velocites as retrieved from the .hdf5 file; Default: None (optional)
    # trajectory_index; <tuple(<np.array>, <np.array>)>:                                Indices for the trajectories to evaluate; Default: ... [ellipsis: all] (optional)
    
    ## _OR_ ##
    
    # r; <tuple(<np.array>, ...)>:  r_x, r_y, r_z, r, r0 :
    # r: ( r_x, r_y, r_z, r, r0 ):                          xyz coordinates and corresponding distance from origin (2D <np.array>) ; r0: distance from origin to starting position (<float>)
    # v: ( v_x, v_y, v_z, v, v0 ):                          xyz translational velocities and corresponding norms (2D <np.array>) ; v0: starting velocity (<float>)
    # omega: ( omega_x, omega_y, omega_z, omega, omega0 ):  xyz rotational velocities and corresponding norms (2D <np.array>) ; omega0: starting velocity (<float>)
    # trajec_analysis: ( runoutlength; <np.array(<float>)>: Distance covered from starting position
    # bounce_properties; <dict(<str>:<list>)> ):            Properties of detected (via sudden velocity changes) bounces: 
    #       'bounce_indices'; <list(<int>)>:            Index of bounce occurence counted from inside _ONE_ trajectory
    #       'number_bounces'; <int>:                    Number of detected bounces
    #       'bounce_distance'; <list(<float>)>:         (Direct) Distance between start - end position of bounces
    #       'bounce_height'; <list(<float>)>:           Height gain during bounce flight __RELATIVE__ to starting position of bounce (not necessarily useful... -> better: height realive to underlying surface (difficult..))
    #( t; <np.array(<float>)>:                              Time corresponding to each datapoint (same for r, velocity, .. !)
    
    # tmax; <int> ):                                        Max t in dataset converted to array index (will be honored if set !)
    
    # incline; <float>:                                     If set: inclination of the analytical solution for an inclined plane to compare to; Default: None
    
    # -OUT- #
    # UI: matplotlib.pyplot.show()
    # stdout by print()
    # 0 on success
    ####
        
        
        # Soil parameters
        
    _, soilParams = parameters
    soilParams = np.array(soilParams)
    
    
    
        # Trajectory properties
    
    # Get only certain trajectories: sorted by soilType (e.g. 0) -> velocity (e.g. 0, 1, 2, 3 ; all blocks for soil 0, if only 4 velocities simulated)
    #trajectory_index =  ... #[0, 1, 2, 3]#...#[8] # [1, 5, 8]

    # Honor passed tmax
    tmax = None
    if not t_analysis is None:
        tmax = t_analysis[1]
        

    # Conduct analysis of .hdf5 file if not already done: no blockAttr passed
    if not blockAttr is None:
        r_analysis, v_analysis, omega_analysis, trajec_analysis, t_analysis = trajectory_analysis(blockAttr, ind_v0=trajectory_index)
        
    # r
    r_x = r_analysis[0]
    r_y = r_analysis[1]
    r_z = r_analysis[2]
    r = r_analysis[3]
    r0 = r_analysis[4]
    
    # For the incline plane we want the starting point at the top
    if not incline is None:
        r_x = abs(r_x - r_x[0, 0])
        r_y = abs(r_y - r_y[0, 0])
        r_z = abs(r_z - r_z[0, 0])  
       
        r = np.sqrt(r_x**2 + r_y**2 + r_z**2)
        
        
    # v
    v_x = v_analysis[0]
    v_y = v_analysis[1]
    v_z = v_analysis[2]
    v = v_analysis[3]
    v0 = v_analysis[4]
    
    # omega
    omega_x = omega_analysis[0]
    omega_y = omega_analysis[1]
    omega_z = omega_analysis[2]
    omega = omega_analysis[3]
        
        
    t = t_analysis[0]
    # Honor passed tmax
    if tmax is None:
        tmax = t_analysis[1]
        
        
        # trajectory properties
            
    runoutlength = trajec_analysis[0]
    total_runoutlength = runoutlength[:, -1]
    
    print('\n\t\t\t\t -- Analysis -- \n')
    print('maximal run-out length: ')
    for total_runoutlength_v0 in total_runoutlength:
        print('\t', total_runoutlength_v0)
    
    bounce_dict = trajec_analysis[1]
    bounce_indices = bounce_dict['bounce_indices']
    number_bounces = bounce_dict['number_bounces']
    bounce_distance = bounce_dict['bounce_distance']
    bounce_height = bounce_dict['bounce_height']
    
            
    print('\nBounce properties: ')
    print('Number of bounces: ')
    for number_bounces_v0 in number_bounces:
        print('\t', number_bounces_v0)
    print(f'Bounce indices (max: {tmax}): ')
    for bounce_indices_v0 in bounce_indices:
        print('\t', bounce_indices_v0)
    print('Mean bounce distance: ')
    for bounce_distance_v0 in bounce_distance:
        print('\t', bounce_distance_v0)
        #print('\t', describe_dataset(bounce_distance_v0.reshape((-1, 1)))['mean'][0])
    
    ## TODO ## toggle plotting/further options
    #return
    
    
    print('Bounce_height (not necessarily useful): ')
    for bounce_height_v0 in bounce_height:
        print('\t', bounce_height_v0)
    
    print('\n\t\t\t\t -- -- \n')
    
    
        
        
        # Plotting
        ##### yes, very well possible to do that nicer (somehow..?)
        
        
    # If an ellipsis got passed: we need a way for the graph labeling later to work
    ## Workaround
    if trajectory_index is ... :
        trajectory_index = range(len(r_x))
        
        # Detailed plot
        
    #"""
    fig = plt.figure()
    
    rows = 3
    columns = 4    
    
    '''
    plot_multigraph_subplot(fig, t[:tmax], -r_y[:, :tmax], x_label='$t$ in s', y_label='$r_y$(t) in m', rows=rows, columns=columns, position=2, graph_label=[f'$r_y$ at $v_0$: {vel:.2} m/s' for vel in v0])
    plt.show()
    
    input()
    '''
    
    
    
    
        # r
    # r_x
    plot_multigraph_subplot(fig, t[:tmax], r_x[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=1, graph_label=multigraph_labeling('r_x', v0, soilParams, trajectory_index))
    # r_y
    plot_multigraph_subplot(fig, t[:tmax], r_y[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=2, graph_label=multigraph_labeling('r_y', v0, soilParams, trajectory_index))
    print(r_y[:, -1])
    # r_z
    plot_multigraph_subplot(fig, t[:tmax], r_z[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=3, graph_label=multigraph_labeling('r_z', v0, soilParams, trajectory_index))
    # runoutlength
    plot_multigraph_subplot(fig, t[:tmax], runoutlength[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=4, graph_label=multigraph_labeling('runoutlength', v0, soilParams, trajectory_index))

        # v
    # v_x
    plot_multigraph_subplot(fig, t[:tmax], v_x[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=5, graph_label=multigraph_labeling('v_x', v0, soilParams, trajectory_index))
    # v_y
    plot_multigraph_subplot(fig, t[:tmax], v_y[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=6, graph_label=multigraph_labeling('v_y', v0, soilParams, trajectory_index))
    # v_z
    plot_multigraph_subplot(fig, t[:tmax], v_z[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=7, graph_label=multigraph_labeling('v_z', v0, soilParams, trajectory_index))
    # v
    plot_multigraph_subplot(fig, t[:tmax], v[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=8, graph_label=multigraph_labeling('v', v0, soilParams, trajectory_index))

        # omega
    # omega_x
    plot_multigraph_subplot(fig, t[:tmax], omega_x[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=9, graph_label=multigraph_labeling('\omega_x', v0, soilParams, trajectory_index))
    # omega_y
    plot_multigraph_subplot(fig, t[:tmax], omega_y[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=10, graph_label=multigraph_labeling('\omega_y', v0, soilParams, trajectory_index))
    # omega_z
    plot_multigraph_subplot(fig, t[:tmax], omega_z[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=11, graph_label=multigraph_labeling('\omega_z', v0, soilParams, trajectory_index))
    # omega
    plot_multigraph_subplot(fig, t[:tmax], omega[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=12, graph_label=multigraph_labeling('\omega', v0, soilParams, trajectory_index))
    
    
    #plt.show()
    #"""
    

        # Overview of the key properties
          
    #"""
     
    fig = plt.figure()
    
    rows = 2
    columns = 2
    
     
    # If an inclination was passed: compare the analytical solution!
    if not incline is None:
        ##     inclination = np.arctan(abs((np.max(mZ)-np.min(mZ))/(mZ.shape[0]*resolution)))
        # All r0 assumed to be the same
        analyt_sol = analytic_solution(t[:tmax], r0=r[0, 0], r_v0=v0, alpha=np.pi/180 * incline)
    
    
        # r
        plot_multigraph_subplot(fig, t[:tmax], [r[:, :tmax], analyt_sol[0][0]], x_label='$t$ in s', y_label='$r$ in m', rows=rows, columns=columns, position=1, graph_label=[[f'$r$ at $v_0$: {vel:.2} m/s' for vel in v0], [f'analytical $r$ at $v_0$: {vel:.2} m/s' for vel in v0]], linestyle=['solid', 'dotted'])
        
        # r_v
        plot_multigraph_subplot(fig, t[:tmax], [v[:, :tmax], analyt_sol[1][0]], x_label='$t$ in s', y_label='$v$ in m/s', rows=rows, columns=columns, position=2, graph_label=[[f'$v$ at $v_0$: {vel:.2} m/s' for vel in v0], [f'analytical $v$ at $v_0$: {vel:.2} m/s' for vel in v0]], linestyle=['solid', 'dotted'])

        
        # omega_v
        plot_multigraph_subplot(fig, t[:tmax], [omega[:, :tmax], analyt_sol[1][1]], x_label='$t$ in s', y_label='$\omega$ in 1/s', rows=rows, columns=columns, position=3, graph_label=[[f'$\omega$ at $v_0$: {vel:.2} m/s' for vel in v0], [f'analytical $\omega$ at $v_0$: {vel:.2} m/s' for vel in v0]], linestyle=['solid', 'dotted'])
        
        # E_kin
        plot_multigraph_subplot(fig, t[:tmax], [kinetic_energy(v[:, :tmax], omega[:, :tmax]), kinetic_energy(*analyt_sol[1])], x_label='$t$ in s', y_label='$E_{kin}$ in J', rows=rows, columns=columns, position=4, graph_label=[['$E_{kin}$' + f' at $v_0$: {vel:.2} m/s' for vel in v0], ['analytical $E_{kin}$' + f' at $v_0$: {vel:.2} m/s' for vel in v0]], linestyle=['solid', 'dotted'])
        
    
        '''
        # r
        plot_multigraph_subplot(fig, t[:tmax], [r[:, :tmax], analyt_sol[0][0]], x_label='$t$', y_label=None, rows=rows, columns=columns, position=1, graph_label=[multigraph_labeling('r', v0, soilParams, trajectory_index), [f'analytical $r$ for $\\alpha={incline}$ at $v_0$: {vel}' for vel in v0]], linestyle=['solid', 'dotted'])
        
        # r_v
        plot_multigraph_subplot(fig, t[:tmax], [v[:, :tmax], analyt_sol[1][0]], x_label='$t$', y_label=None, rows=rows, columns=columns, position=2, graph_label=[multigraph_labeling('v', v0, soilParams, trajectory_index), [f'analytical $v_r$ for $\\alpha={incline}$ at $v_0$: {vel}' for vel in v0]], linestyle=['solid', 'dotted'])

        
        # omega_v
        plot_multigraph_subplot(fig, t[:tmax], [omega[:, :tmax], analyt_sol[1][1]], x_label='$t$', y_label=None, rows=rows, columns=columns, position=3, graph_label=[multigraph_labeling('\omega', v0, soilParams, trajectory_index), [f'analytical $\omega$ for $\\alpha={incline}$ at $v_0$: {vel}' for vel in v0]], linestyle=['solid', 'dotted'])
        
        # E_kin
        plot_multigraph_subplot(fig, t[:tmax], [kinetic_energy(v[:, :tmax], omega[:, :tmax]), kinetic_energy(*analyt_sol[1])], x_label='$t$', y_label=None, rows=rows, columns=columns, position=4, graph_label=[multigraph_labeling('simulation\; E_{kin}', v0, soilParams, trajectory_index), ['analytical $E_{kin}$ for $\\alpha=' + str(incline) + f'$ at $v_0$: {vel}' for vel in v0]], linestyle=['solid', 'dotted'])
        '''

    else:
    
        # runoutlength
        plot_multigraph_subplot(fig, t[:tmax], runoutlength[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=1, graph_label=multigraph_labeling('runoutlength', v0, soilParams, trajectory_index))
        
        # v
        plot_multigraph_subplot(fig, t[:tmax], v[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=2, graph_label=multigraph_labeling('v', v0, soilParams, trajectory_index))
        
        # omega
        plot_multigraph_subplot(fig, t[:tmax], omega[:, :tmax], x_label='$t$', y_label=None, rows=rows, columns=columns, position=3, graph_label=multigraph_labeling('\omega', v0, soilParams, trajectory_index))
        
        # E_kin
        plot_multigraph_subplot(fig, t[:tmax], kinetic_energy(v[:, :tmax], omega[:, :tmax]), x_label='$t$', y_label=None, rows=rows, columns=columns, position=4, graph_label=multigraph_labeling('simulation\; E_{kin}', v0, soilParams, trajectory_index))
        
        
    #plt.show()
    #"""
    
    
    """
    # Hide grid lines
    ax.grid(False)

    # Hide axes ticks
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    #"""
       
    
    
    """ bounce 'overlay': TEST
    bounce_graph = np.zeros(v_z.shape[-1])
    bounce_graph[bounce_indices] = v_z[0, bounce_indices]*2
    
    bounce_graph = np.array([bounce_graph])
    #print(bounce_graph[0, np.argwhere(bounce_graph != 0)])    
       
    fig = plt.figure()
    
    rows = 1
    columns = 1
    
    # v_z with bounces
    plot_multigraph_subplot(fig, t[:tmax], [v_z[:, :tmax], bounce_graph[:, :tmax]], x_label='$t$', y_label=None, rows=rows, columns=columns, position=1, graph_label=[['$v_z$ at $v_0$: ' + str(vel) + ' for ' + str(soil) for soil, vel in zip(soilParams[np.array(trajectory_index)//number_soils], v0)],  ['bounces'*len(trajectory_index)]], linestyle=['solid', 'dashed'])
           
           
    plt.show()
    
    #"""
    
    plt.show()
    
    
    return 0

    
    
    
    
##################################### CODE EXECUTION ############################################


  
def create_levels(number_parameters, number_levels):#, sampling_design='LatinHypercube'):
    """
    # TODO:
        - safety-check !?
    """


    ####
    # Create and return multiple parameter sets ('levels') that fill the parameter space (good enough)

    # -IN- #    
    # number_parameters; <int>:     Number of independent parameters (i.e. dimensionality of the parameter space)
    # number_levels; <int>:         Number of points to sample from parameter space
        
    
    
    ### sampling_design; <string>:    Design used to generate the different levels ; Currently only 'LatinHypercube' supported (optional)
    
    # -OUT- #
    # levels; <list(<list(<float>)>)>:  List containing multiple parameter sets ('levels') generated by the specified sampling_design
    ####
    
    
    # Quasi-Monte_carlo
    from scipy.stats import qmc
    
    
    seed = 11235813213455
    rng = np.random.Generator(np.random.PCG64(seed))
    
    # Create LatinHypercube as a good spacefilling sampling design
    ## Ref: https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.qmc.LatinHypercube.html#rf9f02aa1cbac-6
    #strength=2: orthogonal LHS (better projection, better variability? -> wiki LHS ; but not necessarily good spacefilling -> Ye1998 /4.)
    # with optimization probably ok -> but not necessarily strength2 anymore... ?
    sampler = qmc.LatinHypercube(d=number_parameters, optimization="random-cd", seed=rng)
    
    levels = sampler.random(n=number_levels)
    
    ## potentially for non-normalized parameters: qmc.scale(levels, l_bounds, u_bounds)
    
    
    return levels
    




def check_directories(directories):
    """
    #TODO:
    """

    ####
    # Check if expected directory structure exists and create missing directories

    # -IN- #    
    # directories; <list(<PATH>)>:  (Relative) Paths to directories to check
        
    
    # -OUT- #
    # 0 on success
    # potentially created directories if missing
    ####
    
    import os
    
    # Check all given directories
    for directory in directories:
        # Create directory if missing ; DOES NOT check for permission and stuff
        if not os.path.exists(directory):
            subprocess.run(['mkdir', f'{directory}'])
            
            
    return 0
    
    
    

def run_scripts(crater_name, DTM, LBL, features, soil_parameters, rocks_parameters=None, simulation_name=None, run_preprocessing=True):
    """
    #TODO:
        - get newly created filepath from stdout
    
        - positions needed??
        
        - Verbose print outs ?! / UI input()
    """

    ####
    # Run all scripts (0_preprocessing, 1_simulation, 2_analysis) for _VALIDATION_ with the input files given by their filepaths

    # -IN- #    
    # crater_name; <str>:   Name of the crater (used for output naming and directory access)
    
    # DTM; <PATH>:          Path to Digital Terrain Model (DTM) in shape of a binary table for the crater
    # LBL; <PATH> OR 0:     Path to corresponding LBL file ; 0 if naming same as DTM only .LBL instead of .IMG ; __WATCH OUT__: sometime convention in LBL not honored! Manual adjustment of row_numers in 0_preprocessing needed
    # features; <PATH>:     Path to .GEOJSON file holding the extracted rockfall paths (!) from NACs
    
    # soil_parameters; <PATH>:  Path to .csv holding the parameters for the soil typ to simulate ; __WATCH OUT__: only ONE soil type implemented at the moment ! ; ; Expected: (e, mu, mu_r)
    # rocks_parameters; <PATH>: Path to .csv holding all parameters for the rocks of each rockfall site ; Expected: (L1, L2, L3, points, bulk_density, starting_uncertainty, v0) ; Default: None --> Generate own .csv from additional shape information (shape paths for longest and perpendicular axis!) provided via features (optional)
    
    # simulation_name; <str>:   Special name for the simulation output if desired ; Default: None -> {crater_name}.hdf5 ; __WATCH OUT__ will OVERRRIDE existing files in /data/{crater_name}/analysis/{simulation_name}.hdf5
    # run_preprocessing; <bool>: Toggle whether to execute the preprocessing routine including running the '1_preprocessing.py' script; Default: True (optional)
    
    
    # -OUT- #
    #
    ####
    
    
    #import os
    #os.environ['OMP_NUM_THREADS'] = '4'
    
    # Check and create expected directory structure
    ## Relative paths from top level script's directory accessing this function
    directories = ['data', f'data/{crater_name}', f'data/{crater_name}/1_input', f'data/{crater_name}/2_simulation', f'data/{crater_name}/3_analysis', f'data/{crater_name}/4_statistics']
    
    check_directories(directories)
    
    
    #"""
    
        # 1_preprocessing
    if run_preprocessing:
        print('1_preprocessing')

        preprocessing = subprocess.run(['python3', '1_preprocessing.py', f'-i {DTM}', f'-l {LBL}', f'-f {features}', f'-n {crater_name}'], capture_output=True)

        # Catch errors
        errors = preprocessing.stderr
        if not errors == b'':
            print(preprocessing.stdout.decode('utf-8'))
            print(preprocessing.stderr)

            # Move output files ['map.asc', 'soil.asc', 'siconos.geojson']
        ## positions <-> siconos.geojson!
        for appendix in ['map.asc', 'soil.asc', 'siconos.geojson']:
            subprocess.run(['mv',  f'{crater_name}_{appendix}', f'data/{crater_name}/1_input/{crater_name}_{appendix}'])
            
        # Delete generated .csv rock parameter file OR move newly generated
        if not rocks_parameters is None:
            subprocess.run(['rm',  f'{crater_name}_rock_parameters.csv'])
        else:
            rocks_parameters = f'data/{crater_name}/1_input/{crater_name}_rock_parameters.csv'
            subprocess.run(['mv', f'{crater_name}_rock_parameters.csv', rocks_parameters])
            
    # If rocks_parameters is None and no preprocessing -> we assume already generated and moved
    elif rocks_parameters is None: rocks_parameters = f'data/{crater_name}/1_input/{crater_name}_rock_parameters.csv'
         
        
        # 2_simulation
    print('2_simulation')
    
    ## positions <-> siconos.geojson!
    simulation = subprocess.run(['python3', '2_simulation.py', f'-g data/{crater_name}/1_input/{crater_name}_map.asc', f'-s data/{crater_name}/1_input/{crater_name}_soil.asc', f'-b data/{crater_name}/1_input/{crater_name}_siconos.geojson', f'-m {rocks_parameters}', f'-n {soil_parameters}'], capture_output=True)    
    
    
    # Catch errors
    errors = simulation.stderr
    if not errors == b'':
        print(simulation.stdout.decode('utf-8'))
        print(simulation.stderr)
    
    #"""
        # Move output file
    if simulation_name is None: simulation_name = crater_name
    subprocess.run(['mv',  f'2_simulation.hdf5', f'data/{crater_name}/2_simulation/{simulation_name}.hdf5'])
    
    
    
    #"""
        # 3_analysis
    print('3_analysis')
    
    analysis = subprocess.run(['python3', '3_analysis.py', f'-r data/{crater_name}/2_simulation/{simulation_name}.hdf5', f'-f data/{crater_name}/1_input/{crater_name}_siconos.geojson', f'-m {rocks_parameters}', f'-n {soil_parameters}'], capture_output=True)
    
    # Catch errors
    errors = analysis.stderr
    if not errors == b'':
        print(analysis.stdout.decode('utf-8'))
        print(analysis.stderr)
    
    
        # Move output file
    subprocess.run(['mv',  f'{simulation_name}_analysis.json', f'data/{crater_name}/3_analysis/{simulation_name}_analysis.json'])
    #"""
    

        # 4_statistics
    print('4_statistics')
    
    statistics = subprocess.run(['python3', '4_statistics.py', f'-a data/{crater_name}/3_analysis/{simulation_name}_analysis.json'], capture_output=True)
    
    # Catch errors
    errors = statistics.stderr
    if not errors == b'':
        print(statistics.stdout.decode('utf-8'))
        print(statistics.stderr)
    
    
        # Move output file
    subprocess.run(['mv',  f'{simulation_name}_statistics.json', f'data/{crater_name}/4_statistics/{simulation_name}_statistics.json'])
    
    #"""
   
    return f'data/{crater_name}/4_statistics/{simulation_name}_statistics.json'






