########
# TODO:
#
#
##########


##
##  (Rudimantary) Tests for various functions in calibration.py
##



import numpy as np

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import sys, os

# only needed if helperfunctions one directory above this file
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))
sys.path.append(os.path.join(os.path.dirname(__file__), '../Verification-lunar_rockfalls/'))
import helperfunctions as hf
import Calibration 

#np.set_printoptions(threshold=sys.maxsize)

from osgeo import ogr


def rank_results_test():
    print('\n rank_results_test():\n')
    
    print('\n One flip [1, 3, 2, 4, 5]:')
    
    results = [ ('level_1', 0.5, 0, 0, 0, 0, 0, 0, 0),
                ('level_2', 1, 0, 10, 0, 0, 0, 0, 0),
                ('level_3', 1.000000001, 0, 2, 0, 0, 0, 0, 0),
                ('level_4', 1.5, 0, 5, 0, 0, 0, 0, 0),
                ('level_5', 2, 0, 10, 0, 0, 0, 0, 0)]
                
    results = np.array(results, dtype=([('ID', 'S10'), ('centroid_distance_m', float), ('centroid_distance_variance', float), ('percentile_location_lower_m', float), ('percentile_location_lower_variance', float),('percentile_location_upper_m', float), ('percentile_location_upper_variance', float),('individual_distance_statistics_m', float), ('individual_distance_statistics_variance', float)]))
    
    
    print(Calibration.rank_results(results))
    
    
    
    print('\n Two flip with high outlier [1, 3, 4, 2, 5, 6, 7, 8, 9]:')
    
    results = [ ('level_1', 0.9, 0, 0, 0, 0, 0, 0, 0),
                ('level_2', 1, 0, 10, 0, 0, 0, 0, 0),
                ('level_3', 1.0000000001, 0, 2, 0, 0, 0, 0, 0),
                ('level_4', 1.0000000002, 0, 5, 0, 0, 0, 0, 0),
                ('level_5', 1.2, 0, 10, 0, 0, 0, 0, 0),
                ('level_6', 1.3, 0, 10, 0, 0, 0, 0, 0),
                ('level_7', 1.4, 0, 10, 0, 0, 0, 0, 0),
                ('level_8', 1.5, 0, 10, 0, 0, 0, 0, 0),
                ('level_9', 1.6, 0, 10, 0, 0, 0, 0, 0)]
              
      
    results = np.array(results, dtype=([('ID', 'S10'), ('centroid_distance_m', float), ('centroid_distance_variance', float), ('percentile_location_lower_m', float), ('percentile_location_lower_variance', float),('percentile_location_upper_m', float), ('percentile_location_upper_variance', float),('individual_distance_statistics_m', float), ('individual_distance_statistics_variance', float)]))
    
    
    print(Calibration.rank_results(results))
    
    
    
    return 0
    
    
    
    
##################################################################################################

 
rank_results_test() ## Passed





