########
# TODO:
#   - 4D LHS with L2 difference! ## visualization?
#
#   - optimized for robustness atm... -> maybe more verbose?
#   - omega <-> omega_v (only naming) mushed together at some point... :(
#
#   - .csv file input with all paths ? (not hardcoded anymore)
##########


##
##  Analyse results of multiple .hdf5 files (hardcoded PATHS!)
##



import numpy as np

import sys, os
# only needed if helperfunctions one directory above this file
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import helperfunctions as hf

import json

#np.set_printoptions(threshold=sys.maxsize)



def input_files(hdf5_files, soilparameter_files):
    """
    # TODO:
    """
    
    ####
    # Retrieve trajectories, velocities and corresponding soil parameters from multiple .hdf5 files and .csv files
    
    # -IN- #    
    # hdf5_files; <list(<PATH>)>:           List of PATHs to .hdf5 files (containing trajectories, velocities)
    # soilparameter_files; <list(<PATH>)>:  List of PATHs to .csv files (containing soil parameters)
    
    
    # -OUT- #    
    # blockTrajectories; <list(<np.array(<float>)>)>:       List of 2D arrays containing the trajectories (time, ID, x, y, z) of each .hdf5 file as saved in "data"/"dynamic"
    # blockVelocities; <list(<np.array(<float>)>)>:         List of 2D arrays containing the velocities (time, ID, v_x, v_y, v_z), omega_x, omega_y, omega_z of each .hdf5 file as saved in "data"/"velocities"
    # soilParameters; <list(<list(<dict(<str>:<str>)>)>)>:  Soil parameters (e, mu, mu_r) corresponding to each .hdf5 file
    ####
    
    
        # Soil (type)
        
    soilParameters = []
    
    for files in soilparameter_files:
        soilParameters.append(hf.read_CSV(files))
        

        # Block trajectories (x, y, z)

    blockTrajectories = []
    blockVelocities = []

    try:
        for files in hdf5_files:
            with h5py.File(files, "r") as hdf5File:
                
                # t, ID, x, y, z, orientation (quaternion)
                ## x ^, y ->
                trajectories = hdf5File["data"]["dynamic"]
                                
                #nBlocks = int(np.amax(blockTrajectories[:, 1]))
                blockTrajectories.append(np.array(trajectories[:, :5]))
                
                # t, ID, vx, vy, vz, omega
                velocities = hdf5File["data"]["velocities"]
                blockVelocities.append(np.array(velocities[:, :]))
                
    except:
        print("  - An error occured while trying to access the input block trajectories' filepath:")
        print("  - " + str(sys.exc_info()[0]) + " : " + str(sys.exc_info()[1]) + "\n")  
        
      
    return blockTrajectories, blockVelocities, soilParameters

    

def analyse_runs(trajectory_data, velocity_data):
    """
    # TODO:
    """
    
    ####
    # Retrieve detailed information about the generated trajectories and velocities for each run
    ## We assume t, v0-choices, starting positions to be the same for each run !
    
    # -IN- #    
    # blockTrajectories; <list(<np.array(<float>)>)>:       List of 2D arrays containing the trajectories (time, ID, x, y, z) of each .hdf5 file as saved in "data"/"dynamic"
    # blockVelocities; <list(<np.array(<float>)>)>:         List of 2D arrays containing the velocities (time, ID, v_x, v_y, v_z), omega_x, omega_y, omega_z of each .hdf5 file as saved in "data"/"velocities"
    
    # -OUT- #    
    # r_data, v_data, omega_data, traj_3_analysis_data; <list(<list(..)>)>)>: Data returned by hf.trajectory_analysis() ordered for each run
    
    ## just _ONE_ 'run'!!
    # t: (t; <np.array(<float>)>:                                           Time corresponding to each datapoint (same for r, velocity, .. !)
    # tmax; <int> ):                                                        Max t in dataset converted to array index; Default conversion: 1e-3 (look in siconos simulation for io.run-options!!! might not always be 1e-3 !)
    ####
    
        
    r_data = []
    v_data = []
    omega_data = []
    traj_3_analysis_data = []
        
        # Get details of trajectories
    for traj, vel in zip(trajectory_data, velocity_data):

        ## t, v0, starting position assumed to be the same for every run
        r, v, omega, traj_3_analysis, t = hf.trajectory_analysis((traj, vel))
        
        r_data.append(r)
        v_data.append(v)
        omega_data.append(omega)
        traj_3_analysis_data.append(traj_3_analysis)
        
    return r_data, v_data, omega_data, traj_3_analysis_data, t
    
    
    
def get_statistics(data):
    """
    # TODO:
    """
    
    ####
    # Analyse each run individually, then analyse this new 'meta' dataset ; as well as all runs simultaneously (pooled)
    ## We assume the data to be sorted for each different v0 ; additionally to t, v0-choices, starting positions to be the same for each run !
    
    # -IN- #    
    # data; <list(<np.array(<float>)>)>-like:   3D Data-array to be analysed sorted by v0, then runs
    
    # -OUT- #    
    # [means_means_data, individual_stats_v0, pooled_stats_data]; <tuple()>:    Statistics about the means (mirrored in individual_stats_v0) of each run; statistics about each run; statistics about the pooled data (everything put together - does not always make a lot of sense!!!) : Type as returned by hf.describe_dataset()
    # [means_data, pooled_data]; <list(<float>)>:                               All individual means together; all data together
    ####
    
    
        
        # Get statistics for each v0 
        
    individual_stats_v0 = []
    means_means_data = []
    
    ## really needed? -> could be extracted from individual stats..
    means_data = []

    pooled_data = []
    pooled_stats_data = []
    
    for data_v0_j in data:
        
        # Pooled mean #

            # Check, if pooling even possible (same amount of data in each run)
        array_lens = []
        
        for runs in data_v0_j:
            array_lens.append(len(runs))
            
        array_lens = np.array(array_lens)
        
        # Only if no different lens detected: pool data 
        if np.amax(array_lens) == np.amin(array_lens):
        
                # Pool all data
            total_data = []
            for runs in data_v0_j:
                total_data.append(runs.tolist())
            
            pooled_data.append(np.array(total_data).flatten())
            
                # Get pooled mean
            pooled_stats_data.append(hf.describe_dataset(np.array(total_data).reshape((-1, 1))))
            
        
        # Means -> mean #
        
            # Get the statistics for each run individually
        stats_data = []
        for runs in data_v0_j:
            stats_data.append(hf.describe_dataset(runs.reshape((-1, 1))))

        individual_stats_v0.append(stats_data)
       
            # Calculate mean of means
            ### could be included in FOR above..
        means_data_v0_j = []
        for run in stats_data:
            means_data_v0_j.append(run["mean"])
            
        means_data.append(np.array(means_data_v0_j).flatten())
        
        means_means_data.append(hf.describe_dataset(np.array(means_data_v0_j).reshape((-1, 1))))
       
       
    return [means_means_data, individual_stats_v0, pooled_stats_data], [means_data, pooled_data]



def change_hirachy(data, parameter_index=0, parameter_values=...):
    """
    # TODO:
        - naming?
    """
    
    ####
    # Extract parameter and change hirachy from runs -> v0 to v0 -> runs
    ## Comparable to a transpose(), only in higher dimensions with weird arrays
    
    # -IN- #    
    # data; <list(<np.array(<float>)>)>-like:   3D Data-array to be transposed sorted by runs, then parameters, then v0
    # parameter_index; <int>:                   Index of parameter in each run (data[run_index][parameter_index])
    # parameter_values; <slice>/<array(<int>)>: Values of parameter in each run to be considered (data[run_index][parameter_index][parameter_values]) ; Type: np.array index-type 
    # -OUT- #    
    # dataT; <list(<np.array(<float>)>)>-like:  Extracted parameter, now sorted by v0, then runs
    ####


    dataT = []

    # Lay the ground-work
    # We expect every data[run_index][parameter_index] to hold the same amount of v0s: data[0] is just one possibility
    for v0_j in range(len(data[0][parameter_index])):
        dataT.append([])

    # Change hirachy from run -> v0_j to v0_j -> run
    for run in data:
        # Access data[parameter_index] containing (v0 (rows), parameter_values (columns))
        # Only extracted desired values
        for v0_j, parameter_v0 in enumerate(run[parameter_index]):
            dataT[v0_j].append(parameter_v0[parameter_values])


    return dataT

  
  
  
def analyse_parameter(parameter_set, parameter_index=0, parameter_values=..., x_label="x", y_label="y", print_individual_stats=False, plot=False):#True):
    """
    # TODO:
        - boxplot labels!!
        - naming?
    """
    
    ####
    # Analyse parameter values behaviour over all runs
    
    # -IN- #    
    # data; <list(<np.array(<float>)>)>-like:   3D Data-array to be transposed sorted by runs, then parameters, then v0
    # parameter_index; <int>:                   Index of parameter in each run (data[run_index][parameter_index]); Default: 0
    
    # parameter_values; <slice>/<array(<int>)>: Values of parameter in each run to be considered (data[run_index][parameter_index][parameter_values]) ; Type: np.array index-type Default: ... (ellipsis) (optional)
    # x_label; <str>:                       x-axis label of plot; Default: "x" (optional)
    # y_label; <str>:                       y-axis label of plot; Default: "y" (optional)
    # print_individual_stats; <bool>:       Print the stats for each (run, v0) before calculating the 'meta' statistic (i.e. means-mean)
    # plot; <bool>:                         Plot the boxplots for pooled values and individual means over multiple runs
    
    # -OUT- #    
    # dataT; <list(<np.array(<float>)>)>-like:  Extracted parameter, now sorted by v0, then runs
    ####


        # Get statistics for each v0 concerning parameter (index: 1)
    # Extract vy and change hirachy from runs -> v0 to v0 -> runs
    parameter_data = change_hirachy(parameter_set, parameter_index=parameter_index, parameter_values=parameter_values)
              
    # Get statistics
    stats = get_statistics(parameter_data)

    means_means_parameter = stats[0][0]
    individual_stats_parameter = stats[0][1]
    pooled_stats_parameter = stats[0][2]

    means_parameter =  stats[1][0]
    pooled_parameter = stats[1][1]
            
        # 'Meta' statistic looking at the whole dataset
    for v0_j in range(len(means_means_parameter)):
        print(f"v0_j : {v0_j}\n")
        
        # Pooling could have been not possible (see get_statistics())
        if len(pooled_stats_parameter) > 0:
            print("Pooled-mean:")
            print(pooled_stats_parameter[v0_j])
            
        print("Means-mean: ")
        print(means_means_parameter[v0_j], end="\n\n")

        # Individual statistic for each (run, v0)
    if print_individual_stats:
        print("\n\n")
            
        for v0_j in range(len(means_means_parameter)):
            means = []
            
            print(f"v0_j : {v0_j}\n")
            print("Individual stats:\n")
            for individual_stats in individual_stats_parameter[v0_j]:
                print(individual_stats)
                means.append(individual_stats["mean"][0])
            print("\n")
            print(means)
        
        
        
    if plot:
        ######### extra func #####
        # Spread over all runs
        fig = plt.figure()
        
        hf.plot_multiboxplot_subplot(fig, pooled_parameter, x_label=x_label, y_label=y_label, title="Pooled Values")

        # Robustness (here: is it a persisting error? -> yes!)
        fig = plt.figure()

        hf.plot_multiboxplot_subplot(fig, means_parameter, x_label=x_label, y_label=y_label, title="Means of Values")

        plt.show()


    return stats


def search_min(data, plot=False):
    """
    # TODO:
    """
    
    ####
    # Find minimum value over all v0 using L2 Norm
    ## Could use MSE, but nope (see ML scripts..?)
    
    # -IN- #    
    # data; <list(<list(<dict>)>)>: Data containing the individual stats for multiple runs for each v0; for multiple parameters
    
    # plot; <bool>:                 Plot the L2 norm for all runs
    
    # -OUT- #    
    # minimum absolute value, corresponding run index 
    ####
    
    
    # Path to output directory 
    dir_path = 'incline/data/LHS38_soil/statistics'
    
    
    individual_means = []
    
    # Calculate the L2 difference for each parameter, for each v0, for each run
    for p, parameter_data in enumerate(data):
        individual_means.append([])
        
        v0_means = []
        # Gather all means of this parameter for each v0
        for v0_j, v0_runs in enumerate(parameter_data):
        
            ## TODO TO IMPROVE!
            n_runs = len(v0_runs)
            
            # Get mean for each run
            v0_means.append([])
            for runs_stats in v0_runs:
                v0_means[v0_j].append(runs_stats["mean"][0])
            
    
        # Calculate L2 norm for all v0s
        v0_means = np.array(v0_means)
        for j in range(n_runs):
            individual_means[p].append(np.sqrt(np.sum(v0_means[:, j]*v0_means[:, j])))
                
        
    individual_means = np.array(individual_means)
    
    print("\n\n\t\t -- L2 norm over (Ekin, r, v, omega) mean absolute relative (to analytical solution) difference Simulation - Analytical (separat) -- \n")
    print(individual_means)
    
    
        ## OUTPUT ##   
    # Output statistics
    np.savetxt(f'{dir_path}/individual_metric_L2_norms.csv', individual_means, header='E_kin, r, v, omega')
    
    '''
    with open(f'{dir_path}/individual_metric_L2_norms.csv', 'w') as output_statistics_file:
        json.dump(json_output, output_statistics_file)
    '''
        ## ##
        
        
        
    # Print all individual mins
    for parameter in individual_means:
        print(np.amin(parameter), np.argmin(parameter))    
    
    
    # Calculate L2 norm for all parameters
    ## Watch out! Units!
    final_diff = []
    for j in range(n_runs):
        final_diff.append(np.sqrt(np.sum(individual_means[:, j]*individual_means[:, j])))    
        
    print("\n\n\t\t -- L^2 norm over (Ekin, r, v, omega) mean absolute, relative (to analytical solution) difference Simulation - Analytical (all metrics combined) -- \n")
    print(np.array(final_diff))
    
    
    
        ## OUTPUT ##   
    # Output statistics
    np.savetxt(f'{dir_path}/final_L2_norms.csv', final_diff, header='L^2')
        ## ##
    
    
    # Plot L2 for all runs
    if plot:
        
        fig = plt.figure()

        hf.plot_multigraph_subplot(fig, range(n_runs), np.array([np.array(final_diff)]), x_label="run_index", y_label="$L^2$ norm of mean absolute, relative (to analytical solution) differences to analytical solution", linestyle="dotted", graph_label=[[""]])

        plt.show()
        
    

    return np.amin(final_diff), np.argmin(final_diff), [final_diff, individual_means]

  
  
  
def get_data(hdf5_files, soil_files):
    """
    # TODO:
    """
    
    ####
    # Extract raw data from files
    ## Assumption for v0: all runs have _SAME_ v0s
    ## we only check one run
    
    # -IN- #    
    # hdf5_files; <list(<str)>: Paths to all .hdf5 files
    # soil_files; <list(<str)>: Paths to all soil parameter files
    
    
    # -OUT- #  
    # r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs;   as returned by analyse_runs()
    # v0; <list(<float>)>:  All unique starting velocities
    # parameters;           Soil parameters as returned by input_files()
    ####
    
    
    # Extract raw data from files
    trajectories, velocities, parameters = input_files(hdf5_files, soil_files)

    # Analyse results
    r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs = analyse_runs(trajectories, velocities)


    ## For example run 0 holds all v0; Assumption: all runs have _SAME_ v0s
    v0 = [v0 for v0 in v_runs[0][-1]]
    print(f"\nv0 = {v0}")
    
    return r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, parameters
    
      
  
  
#########################################################################################
  
def analytical_comparison(r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, analytic, parameters, soil_search=False):

    # Comparison to analytical solution
    #"""
   
   
    # Path to output directory 
    dir_path = 'incline/data/LHS38_soil/statistics'
    
    
    Ekin = []

    t = t_runs[0]


    # Look at difference between analytical solution and simulation        
    for j, run in enumerate(r_runs):

            # First: get rid of offset, i.e. normalize
            ## and ignore the vector as well as the 0 component :)
        r_runs[j] = [run[0]-run[0][0, 0], run[1]-run[1][0, 0], run[2]-run[2][0, 0]]
        v_runs[j] = [v_runs[j][0], v_runs[j][1], v_runs[j][2]]
        omega_runs[j] = [omega_runs[j][0], omega_runs[j][1], omega_runs[j][2]]
        
        
        # Calculate norms
        ## for r: whole vecor used! not only x component!
        ## maybe np.linalg.norm() better ?
        r_runs[j] = np.sqrt(r_runs[j][0]*r_runs[j][0] + r_runs[j][1]*r_runs[j][1] + r_runs[j][2]*r_runs[j][2])
        v_runs[j] = np.sqrt(v_runs[j][0]*v_runs[j][0] + v_runs[j][1]*v_runs[j][1] + v_runs[j][2]*v_runs[j][2])
        omega_runs[j]  = np.sqrt(omega_runs[j][0]*omega_runs[j][0] + omega_runs[j][1]*omega_runs[j][1] + omega_runs[j][2]*omega_runs[j][2])
        
        
            # Prepare analytic solution
        r_analytic = analytic[j][0][0]
        v_analytic = analytic[j][1][0]
        omega_analytic = analytic[j][1][1]    
        
        Ekin_analytic = hf.kinetic_energy(v_analytic, omega_analytic)
        
        #'''
            # Get differences
        Ekin.append([(hf.kinetic_energy(v_runs[j], omega_runs[j])-Ekin_analytic)]) 
        
        r_runs[j] = [(r_runs[j]-r_analytic)]
        v_runs[j] = [(v_runs[j]-v_analytic)]
        omega_runs[j] = [(omega_runs[j]-omega_analytic)]
        
        
        '''
            # Get _relative_ differences !!
        Ekin.append([abs((hf.kinetic_energy(v_runs[j], omega_runs[j])-Ekin_analytic)/Ekin_analytic)]) 
        
        r_runs[j] = [abs((r_runs[j]-r_analytic)/r_analytic)]
        v_runs[j] = [abs((v_runs[j]-v_analytic)/v_analytic)]
        omega_runs[j] = [abs((omega_runs[j]-omega_analytic)/omega_analytic)]
        
        # We have to fix the divide by zeros and nans
        Ekin[-1][0][np.logical_or(Ekin_analytic == 0, np.isnan(Ekin_analytic))] = 0
        r_runs[j][0][np.logical_or(r_analytic == 0, np.isnan(r_analytic))] = 0
        v_runs[j][0][np.logical_or(v_analytic == 0, np.isnan(v_analytic))] = 0
        omega_runs[j][0][np.logical_or(omega_analytic == 0, np.isnan(omega_analytic))] = 0
        #'''
        
        
        # Statistics about differences for r, v, omega, Ekin
    print("\n\n\t\t -- Simulation - Analytical r relative Difference -- \n")
    r_stats = analyse_parameter(r_runs, parameter_index=0, parameter_values=..., x_label="v_0", y_label="$r$ difference", print_individual_stats=True, plot=False)
    #'''


    print("\n\n\t\t -- Simulation - Analytical v relative Difference -- \n")
    v_stats = analyse_parameter(v_runs, parameter_index=0, parameter_values=..., x_label="v_0", y_label="$v$ difference", print_individual_stats=True, plot=False)

    print("\n\n\t\t -- Simulation - Analytical omega relative Difference -- \n")
    omega_stats = analyse_parameter(omega_runs, parameter_index=0, parameter_values=..., x_label="v_0", y_label="$\omega_v$ difference", print_individual_stats=True, plot=False)
    #'''
    
    #'''    
    print("\n\n\t\t -- Simulation - Analytical Ekin relative Difference -- \n")
    Ekin_stats = analyse_parameter(Ekin, parameter_index=0, parameter_values=..., x_label="v_0", y_label="$E_{kin}$ difference", print_individual_stats=True, plot=False)


    '''
        ## OUTPUT ##   
    # Output statistics
    with open(f'{dir_path}/r_stats.csv', 'w') as output_statistics_file:
        json.dump([r_stats], output_statistics_file)
        
    with open(f'{dir_path}/v_stats.csv', 'w') as output_statistics_file:
        json.dump([v_stats], output_statistics_file)
        
    with open(f'{dir_path}/omega_stats.csv', 'w') as output_statistics_file:
        json.dump([omega_stats], output_statistics_file)
        
    with open(f'{dir_path}/Ekin_stats.csv', 'w') as output_statistics_file:
        json.dump([Ekin_stats], output_statistics_file)
        ## ##
    '''
        

    # all parameters_diff, individual stats, all v0
    #'''
    min_MSE, min_index, L2 = search_min([Ekin_stats[0][1], r_stats[0][1], v_stats[0][1], omega_stats[0][1]], plot=False)
    print(min_MSE, min_index)
    #'''


    # Soil search @ alpha=20
    
    if soil_search:
        ## Visualize 4D LHS
        #'''
            # Combine all parameters to one dataset 
        soil_parameters = []
        for run_parameters in parameters:
            # Get soil_type 0
            soil_parameters.append([float(run_parameters[0]["e"]), float(run_parameters[0]["mu"]), float(run_parameters[0]["mu_r"])])

        soil_parameters = np.array(soil_parameters)



        ######## extra func!!! ####

         # Plot 4D scatter plot to visualize response surface
        fontsize = 12
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')#121, projection='3d')
        ax.set_xlabel("$\mu$", fontsize=fontsize)#"$e$")
        ax.set_ylabel("$\mu_r$ ", fontsize=fontsize)#"$\mu$")
        ax.set_zlabel("Final $L^2$ norm (to be read dimensionless)", fontsize=fontsize)#"$\mu_r$")


        totalL2 = np.array(L2[0])

        ### naming!
        # only r
        #totalL2 = np.array(L2[1][1])

        reproduce_paper = True
        
        # Reproduce paper Acary/Bourrier: cone problem
        #''' 
        labelpad = 20
        fontsize = 25
                
        ax.set_xlabel("$\mu$", fontsize=fontsize, labelpad=labelpad)
        ax.set_ylabel("$\mu_r$ ", fontsize=fontsize, labelpad=labelpad)
        ax.set_zlabel('$\chi_{max}$ in m', fontsize=fontsize, labelpad=labelpad)
        
        #ax.set_xticks(ax.get_xticks(), labelsize=fontsize)
        #ax.set_yticks(ax.get_yticks(), labelsize=fontsize)
        
        totalL2 = []
        print(r_stats[0][1])
        for runs in traj_analysis_runs:
            # 0: runout; Only look at v0=0, last entry in runoutlength
            totalL2.append(runs[0][0][-1])
        totalL2 = np.array(totalL2)
        print(totalL2)
        #''' 


        # Point size
        totalL2_s = 10*np.amax(totalL2)/totalL2#1000*np.amax(totalL2)/totalL2**2#

            # Surface , cmap=autumn
        ax.scatter(soil_parameters[:, 1], soil_parameters[:, 2], totalL2, c=totalL2, cmap="bwr", s=totalL2_s, edgecolors='black')
        ax.plot_trisurf(soil_parameters[:, 1], soil_parameters[:, 2], totalL2, cmap="bwr", alpha=0.5)

        for v, value in enumerate(totalL2):
            if value < 100 and not reproduce_paper:
                ax.text(soil_parameters[v, 1]+2/100, soil_parameters[v, 2]+2/100, value, "%.2f"%value)
    
        #ax.set_zticks(ax.get_zticks(), labelsize=fontsize)
    
        ax.xaxis.set_tick_params(labelsize=fontsize)
        ax.yaxis.set_tick_params(labelsize=fontsize)
        ax.zaxis.set_tick_params(labelsize=fontsize)
    
        plt.show()
        input()
        
        
    
        """
        ax.view_init(0, 0)
        ax.set_xticks([])
        ax.set_xlabel('')
        """
        
        """
            # 2D projection
        #fig = plt.figure()
        ax = fig.add_subplot(122)
        ax.set_xlabel("$e$ (dimensionless)")
        #ax.set_xlabel("$\mu$ (dimensionless)")#"$e$")
        #ax.set_xlabel("$\mu_r$ (dimensionless)")#"$\mu$")
        ax.set_ylabel("Final $L^2$ norm (to be read dimensionless)")#"$\mu_r$")

        img = ax.scatter(soil_parameters[:, 0], totalL2, s=totalL2_s, c=totalL2, cmap="autumn", edgecolors='black')

        for v, value in enumerate(totalL2):
            if value < 100:
                ax.text(soil_parameters[v, 0]+2/100, value, "%.2f"%value)
                
        

        # Hide the right and top spines
        ax.spines.right.set_visible(False)
        ax.spines.top.set_visible(False)
        ax.set_ylim(bottom=0)
        ax.set_xlim(left=0)
        ax.set_yticks(range(0, 450, 50))
        
        ax.grid(alpha=0.5)
        
        fig.tight_layout()
        plt.show()
            
        #input()
        #"""
        
        #from matplotlib.ticker import MaxNLocator
        #ax.xaxis.set_major_locator(MaxNLocator(5))
        #ax.yaxis.set_major_locator(MaxNLocator(6))
        #ax.zaxis.set_major_locator(MaxNLocator(5))


        #hf.set_axes_equal(ax)
        #fig.tight_layout()
        #plt.show()

            # LHS
        #fig = plt.figure()
        ax = fig.add_subplot(122, projection='3d')
        ax.set_xlabel("$e$ (dimensionless)")
        ax.set_ylabel("$\mu$ (dimensionless)")
        ax.set_zlabel("$\mu_r$ (dimensionless)")

        img = ax.scatter(soil_parameters[:, 0], soil_parameters[:, 1], soil_parameters[:, 2], s=totalL2_s, c=totalL2, cmap="autumn", edgecolors='black')

        for v, value in enumerate(totalL2):
            if value < 100:
                ax.text(soil_parameters[v, 0],soil_parameters[v, 1]+2/100, soil_parameters[v, 2]+2/100, "%.2f"%value)
                
        hf.set_axes_equal(ax)
        fig.tight_layout()
        plt.show()

        #'''


        # Plot the difference for r (== more or less runoutlength here); (v, omega)->Ekin
        ## only for one run!
    fig = plt.figure()

    rows, columns = 2, 2

    hf.plot_multigraph_subplot(fig, t, np.array(r_runs[min_index][0]), x_label="t", y_label=f"$r$ absolute, relative (to analytical solution) difference (index: {min_index})", rows=rows, columns=columns, position=1, graph_label=["$r$ difference at $v_0$: " + str(vel) for vel in v0])
    hf.plot_multigraph_subplot(fig, t, np.array(v_runs[min_index][0]), x_label="t", y_label=f"$v$ difference (index: {min_index})", rows=rows, columns=columns, position=2, graph_label=["$v$ difference at $v_0$: " + str(vel) for vel in v0])
    hf.plot_multigraph_subplot(fig, t, np.array(omega_runs[min_index][0]), x_label="t", y_label=f"$\omega_v$ difference (index: {min_index})", rows=rows, columns=columns, position=3, graph_label=["$\omega_v$ difference at $v_0$: " + str(vel) for vel in v0])
    hf.plot_multigraph_subplot(fig, t, np.array(Ekin[min_index][0]), x_label="t", y_label="$E_{kin}$ difference (index: " + str(min_index) + ")", rows=rows, columns=columns, position=4, graph_label=["$E_{kin}$ difference at $v_0$: " + str(vel) for vel in v0])


    plt.show()
        
    #"""
    
    return 0



def analyse_runoutlength(traj_analysis_runs):

    # Analyse runoutlength
    #"""
    print("\n\n\t\t -- total runoutlength -- \n")
    analyse_parameter(traj_analysis_runs, parameter_index=0, parameter_values=[-1], x_label="v_0", y_label="total runoutlength")
    #"""  
  


def analyse_y(r_runs, v_runs):
        
    # Analyse the y component
    #"""
        # Analyse v_y

    print("\n\n\t\t -- v_y -- \n")
    analyse_parameter(v_runs, parameter_index=1, x_label="v_0", y_label="v_y", print_individual_stats=True)


        # Analyse r_y

    # Start != (0, 0, 0)
    ## only ry of interest here!
    for r, run in enumerate(r_runs):
        r_runs[r][1] = run[1] - run[1][:, 0].reshape((-1, 1))


    print("\n\n\t\t -- final r_y -- \n")
    r_y_stats = analyse_parameter(r_runs, parameter_index=1, parameter_values=[-1], x_label="v_0", y_label="r_y", print_individual_stats=True)


    '''
        # Influence of deviation compared to total runoutlength
    # Fetch runoutlengths for all runs (only v0)
    runoutlengths = []
    for runs in traj_analysis_runs:
        # 0: runout; Only look at v0=0, last entry in runoutlength
        runoutlengths.append(runs[0][0][-1])
            
    # means-means, v0_j=0 ; divide by min runoutlength -> max influence
    print(r_y_stats[0][0][0]["mean"][0]/np.amin(runoutlengths))
    #'''

    #"""




def all_metrics(r_runs, v_runs, omega_runs):


    # Analyse all metrics

    #"""

        # Analyse r

    print("\n\n\t\t -- r -- \n")
    analyse_parameter(r_runs, parameter_index=3, x_label="v_0", y_label="r", print_individual_stats=False)

        # Analyse v

    print("\n\n\t\t -- v -- \n")
    analyse_parameter(v_runs, parameter_index=3, x_label="v_0", y_label="v", print_individual_stats=False)


        # Analyse omega

    print("\n\n\t\t -- omega -- \n")
    analyse_parameter(omega_runs, parameter_index=3, x_label="v_0", y_label="\omega", print_individual_stats=False)

    #"""

        # Analyse Ekin
         
    Ekin = []
    for j, run in enumerate(r_runs):

            # First: get rid of offset, i.e. normalize
            ## and ignore the vector as well as the 0 component :)
        r_runs[j] = [run[0]-run[0][0, 0], run[1]-run[1][0, 0], run[2]-run[2][0, 0]]
        v_runs[j] = [v_runs[j][0], v_runs[j][1], v_runs[j][2]]
        omega_runs[j] = [omega_runs[j][0], omega_runs[j][1], omega_runs[j][2]]
        
        
        # Calculate norms
        r_runs[j] = np.sqrt(r_runs[j][0]*r_runs[j][0] + r_runs[j][1]*r_runs[j][1] + r_runs[j][2]*r_runs[j][2])
        v_runs[j] = np.sqrt(v_runs[j][0]*v_runs[j][0] + v_runs[j][1]*v_runs[j][1] + v_runs[j][2]*v_runs[j][2])
        omega_runs[j]  = np.sqrt(omega_runs[j][0]*omega_runs[j][0] + omega_runs[j][1]*omega_runs[j][1] + omega_runs[j][2]*omega_runs[j][2])
        
            
            # Append Ekin    
        Ekin.append([hf.kinetic_energy(v_runs[j], omega_runs[j])])    
        

    print("\n\n\t\t -- Ekin -- \n")
    analyse_parameter(Ekin, parameter_index=0, x_label="v_0", y_label="E_{kin}", print_individual_stats=False)


    #"""



def traj_differences(r_runs, v_runs, omega_runs):


    # Analyse the trajectory differences (r, v) between runs
    #"""

        # Get differences in r, v between all runs
    r_diff = []
    v_diff = []
    omega_diff = []

    for r1, run1 in enumerate(r_runs):
        r_diff.append([])
        v_diff.append([])
        omega_diff.append([])
        
        for r2, run2 in enumerate(r_runs):
            #r_diff[r1].append([abs(run1[0]-run2[0]), abs(run1[1]-run2[1]), abs(run1[2]-run2[2])])
            #v_diff[r1].append([abs(v_runs[r1][0]-v_runs[r2][0]), abs(v_runs[r1][1]-v_runs[r2][1]), abs(v_runs[r1][2]-v_runs[r2][2])])
            
            r_diff[r1].append([abs(np.amax(run1[0]-run2[0])), abs(np.amax(run1[1]-run2[1])), abs(np.amax(run1[2]-run2[2]))])
            v_diff[r1].append([abs(np.amax(v_runs[r1][0]-v_runs[r2][0])), abs(np.amax(v_runs[r1][1]-v_runs[r2][1])), abs(np.amax(v_runs[r1][2]-v_runs[r2][2]))])
            omega_diff[r1].append([abs(np.amax(omega_runs[r1][0]-omega_runs[r2][0])), abs(np.amax(omega_runs[r1][1]-omega_runs[r2][1])), abs(np.amax(omega_runs[r1][2]-omega_runs[r2][2]))])

    # ry
    print("\n\n\t\t -- difference r, v, omega -- \n")
    print(np.amax(r_diff))
    print(np.amax(v_diff))
    print(np.amax(omega_diff))

    #analyse_parameter(r_diff, parameter_index=1, parameter_values=..., x_label="diff", y_label="diff_r_y", print_individual_stats=True)

    #"""



        
###################################################################################################
  
def soil_search():
    
    # Soil search @ alpha=20
    hdf5_files = []
    soil_files = []

    inclination = 20 

    # 10, 13, 32, 6 look good!

    #"""   
    for j in range(38):
        hdf5_files.append(f"incline/data/LHS38_soil/simulation_level_{j}.hdf5")
        soil_files.append(f"incline/data/LHS38_soil/soil_params_simulation_level_{j}.csv")
    #"""  
    
    
        # level10
    #hdf5_files.append(f"incline/data/LHS38_soil/simulation_level_10.hdf5")
    #soil_files.append(f"incline/data/LHS38_soil/soil_params_simulation_level_10.csv")

    r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, params = get_data(hdf5_files, soil_files)
     
     
    analytical = []
    
    t = t_runs[0]

    
    # Soil search @ alpha=20
    #'''
    for j in range(38):
        analytical.append(hf.analytic_solution(t, r_v0=np.array(v0), alpha=inclination*np.pi/180))
    #'''
     
     
    analytical_comparison(r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, analytical, params, soil_search=True) 

    

def inclination():
    

    # Inclination
    #"""   
    hdf5_files = []
    soil_files = []

    inclination = [0, 10, 20, 30, 40, 50, 60]

    for alpha in inclination:
        hdf5_files.append(f"incline/data/level10/inclination/incl_{alpha}.hdf5")
        #soil_files.append(f"incline/data/incl_{alpha}.csv")
    #"""  
    
    
    r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, params = get_data(hdf5_files, soil_files)
     
     
    analytical = []
    t = t_runs[0]
   
    # Inclination
    #'''
    for incl in inclination:
        analytical.append(hf.analytic_solution(t, r_v0=np.array(v0), alpha=incl*np.pi/180))    
    #''' 
     
     
    analytical_comparison(r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, analytical, params, soil_search=False) 
    
    
    
    
    
def vy_overview():
          
      
    # v_y overview ; v_y <-> 't'/robustness
    
    hdf5_files = []
    soil_files = []

    #"""
    for j in range(100):
        hdf5_files.append(f"incline/data/level10/robustness/simulation_level_{j}.hdf5")
        soil_files.append(f"incline/data/level10/robustness/soil_params_simulation_level_{j}.csv")
        
        #hdf5_files.append(f"incline/data/level10/v_y/simulation_level_{j}.hdf5")
        #soil_files.append(f"incline/data/level10/v_y/soil_params_simulation_level_{j}.csv")
    #"""    
    
    
    r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, params = get_data(hdf5_files, soil_files)
     
    analyse_y(r_runs, v_runs)
    analyse_runoutlength(traj_analysis_runs)
    
    
    

def vy_inclination():
    

    # Inclination
    #"""   
    hdf5_files = []
    soil_files = []

    inclination = [0, 10, 20, 30, 40, 50, 60]

    for alpha in inclination:
        hdf5_files.append(f"incline/data/level10/inclination/incl_{alpha}.hdf5")
        #soil_files.append(f"incline/data/incl_{alpha}.csv")
    #"""  
    
    
    r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, params = get_data(hdf5_files, soil_files)
     
     
    analyse_y(r_runs, v_runs)
    
    
    
def vy_res():
    
        
    # v_y <-> res (t_step=1e-3) 
    hdf5_files = []
    soil_files = []

    """  
    for j in range(1, 9):
        hdf5_files.append(f"incline/data/level10/resolution/res_{j}.hdf5")
        #soil_files.append(f"incline/data/level10/v_y/res,step/soil_params_res{j}.csv")
    
    """    
    for res in np.arange(4.1, 6.1, 0.1, dtype=float):
        res = res.round(1)
        if not res in [5.0, 6.0]: 
            hdf5_files.append(f"incline/data/level10/resolution/res_{res}.hdf5")
    #"""  
    
    r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, params = get_data(hdf5_files, soil_files)
          
    analyse_y(r_runs, v_runs)
     
    
        
        
def vy_timestep():
    
     
    # v_y <-> t_step (res=5)
    #"""
    hdf5_files = []
    soil_files = []

    for j in range(1, 6):
        hdf5_files.append(f"incline/data/level10/timestep/siconos_sphere_res_1e-{j}.hdf5")
        #soil_files.append(f"incline/data/level10/v_y/res,step/soil_params_1e-{j}.csv")
    #"""

    
    r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, params = get_data(hdf5_files, soil_files)
          
    analyse_y(r_runs, v_runs)
     
    
    
  
  
def robustness():

    # Soil_manually @ alpha=20
    hdf5_files = []
    soil_files = []

        # 0,0.3,0
    for j in range(100):
        hdf5_files.append(f"incline/data/0,0.3,0/v_y/simulation_level_{j}.hdf5")
        soil_files.append(f"incline/data/0,0.3,0/v_y/soil_params_simulation_level_{j}.csv")


        # level10
    """   
    for j in range(100):
        hdf5_files.append(f"incline/data/level10/robustness/simulation_level_{j}.hdf5")
        soil_files.append(f"incline/data/level10/robustness/soil_params_simulation_level_{j}.csv")
        
    #"""  
        
    ## better: 38.032 ....
    #e, mu, mu_r
    #0., 0.1, 0.9


    r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, params = get_data(hdf5_files, soil_files)
    
    
    t = t_runs[0]
    inclination = 20 
    analytical = []
    
    analytical.append(hf.analytic_solution(t, r_v0=np.array(v0), alpha=inclination*np.pi/180))
    
    
    
    #analytical_comparison(r_runs, v_runs, omega_runs, traj_analysis_runs, t_runs, v0, analytical, params,     soil_search=False)
    all_metrics(r_runs, v_runs, omega_runs)
    #traj_differences(r_runs, v_runs, omega_runs)
    
    
    
def multi_analysis_bouncing():
    import subprocess 
    
    for j, e in enumerate(np.arange(0, 1.1, 0.1)):
        e = e.round(1)
        ### https://docs.python.org/3/library/subprocess.html#module-subprocess
        simulation = subprocess.run(["python3", "incline/3_analysis.py", f"-t incline/data/bouncing/e_{e}.hdf5", f"-n incline/data/bouncing/soil_params_simulation_level_{j}.csv"], capture_output=True)
        print(simulation.stderr)
        print(simulation.stdout.decode('utf-8'))
    
  
  
#########################################
################ MAIN ###################
######################################### 


# General multi-file 
"""
filenames = ["crater/data/e_0.25_1", "crater/data/mu_0_0.75"]

hdf5_files = []
soil_files = []

for name in filenames:
    hdf5_files.append(f"{name}.hdf5")
    soil_files.append(f"{name}.csv")
#"""   
      
    






#############################################################################################



soil_search()
#inclination()
#vy_res()
#vy_overview()
#vy_inclination()
#vy_timestep()

#robustness()


#multi_analysis_bouncing()


    







    
    
    
"""
    # Plot analysed results
print(f"Plotting {len(parameters)} simulations")
for parameters, traj, vel in zip(parameters, trajectories, velocities):
    hf.plot_results((None, parameters), blockAttr=(traj, vel))
#"""




  
  
  


