########
# TODO:
#   - evaluation function? or just statistics return?
#
##########



#
# Simple script to run all files/code necessary for preprocessing, simulation and analysis of rockfalls in one crater
#


import numpy as np

import sys, os

# Only needed if helperfunctions in different directory
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))
import helperfunctions as hf

import json
import copy


######################################################################

       
def load_statistics(filePath):
    """
    #TODO:
        - change for Validation!?
    """

    ####
    # Read and return selected content of a .JSON file containing the statistic over all analysis parameters
    
    
    # -IN- #    
    # filePath; <str>:  PATH to .JSON file containing all statistic findings
    
    
    # -OUT- #
    # analysis; <np.array(<float>)>:    Statistics for each parameter (mean/median and sample_variance) ordered as in parameters
    # parameters; <list(<str>)>:        All parameters labels responsible for two consecutive items in analysis
    ####
    
    # Load file contents
    with open(filePath) as f:
        content = json.load(f)
    
    statistics = {}
        
    # Load all relevant parameters for each site
    for p, parameter in enumerate(content[0].keys()):
        
        parameter_statistic = content[0][parameter]
        
            # centroid_distance
        if parameter == 'centroid_distance':
            # mean: we care about outliers!, bad (absolute) simulations - even only sometimes - should be treated as such
            statistics[parameter] = [parameter_statistic['mean'], parameter_statistic['sample_variance']]#, parameter_statistic['max']]
            
            # percentile_location
        elif parameter == 'percentile_location_lower':
            # median: we don't care about outliers as much! this is a relativ metric comparing/ranking the observed location relativ to the cluster of simulation positions ; outliers should not skew the overall relativ impression too much as those depend much on the variance/spread of the simulation without saying anything absolute about exactly those
            statistics[parameter] = [parameter_statistic['median'], parameter_statistic['sample_variance']]
        elif parameter == 'percentile_location_upper':
            statistics[parameter] = [parameter_statistic['median'], parameter_statistic['sample_variance']]
            
            # individual_distance_statistics['mean'] 
            ## not needed for distribution perspective ; extra metric  
        elif parameter == 'individual_distance_statistics':
            # mean: we care about outliers!, bad (absolute) simulations - even only sometimes - should be treated as such
            statistics[parameter] = [parameter_statistic['mean'], parameter_statistic['sample_variance']]
            
      
    
        # Assemble values for easy access (and keep them ordered!)
        ## switch from name selection to index selection
    parameters = ['centroid_distance', 'percentile_location_lower', 'percentile_location_upper', 'individual_distance_statistics']
    values = []
        
    for parameter in parameters:
        values.append(statistics[parameter])
           
    # Flatten nested list
    values = [value for statistic in values for value in statistic]
    
    return values, parameters
    
        
    

######################################################################
############################ MAIN ####################################
######################################################################


if __name__ == "__main__":

        # Paths and input name
    crater_name = 'Atlas_East'
    heightmap_path = f'data/{crater_name}/0_raw_data/NAC_DTM_ATLAS1_E473N0448.IMG'
    LBL_path = f'data/{crater_name}/0_raw_data/NAC_DTM_ATLAS1.LBL'
    features_path =  f'data/{crater_name}/validation_features.geojson' 
    
    rocks_path = None
    soil_path = f'data/{crater_name}/soil_params.csv'
    LHS_path = f'data/{crater_name}/LHS_soil.csv'
    calibration_ranking_path = 'Atlas_East_calibration_ranking.csv'
    
    
    
        # Get best level
    ranking = hf.read_CSV(calibration_ranking_path)
    best_level_ID = int(ranking[0]['ID'].rsplit('_')[-1])
           
        # Get corresponding soil parameters
    LHS = hf.read_CSV(LHS_path)
    best_level_parameters = LHS[best_level_ID]
              
          # Execute scripts
          
        # Write soil parameters to soil_params.csv
    parameters = best_level_parameters.keys()
    values = [[float(best_level_parameters[parameter]) for parameter in parameters]]
            
    hf.write_CSV(parameters, values, f'data/{crater_name}/soil_params.csv')

    # _WITHOUT_ preprocessing 
    ## We assume this has already been done during Calibration
    ## WATCHOUT! We need to rebuild rocks_params.csv with new validation features !!!! --> run preprocessing again (should be robust!)
    hf.run_scripts(crater_name, heightmap_path, LBL_path, features_path, soil_path, rocks_parameters=rocks_path, run_preprocessing=True, simulation_name=f'evaluation')
    
        
            
    statistics, parameters = load_statistics(f'data/{crater_name}/4_statistics/evaluation_statistics.json')
    
    print(statistics, parameters)
    
    
    
    
    
    
