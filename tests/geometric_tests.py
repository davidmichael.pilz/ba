########
# TODO:
#
#
##########


##
##  (Rudimantary) Tests for various geometric functions used in other scripts
##



import numpy as np

import h5py

import matplotlib.pyplot as plt
import matplotlib as mpl


from matplotlib import cm, colors   
#from matplotlib.colors import ListedColormap


import sys, os

# only needed if helperfunctions one directory above this file
sys.path.append(os.path.join(os.path.dirname(__file__), '../shared_code'))
import helperfunctions as hf

#np.set_printoptions(threshold=sys.maxsize)

from osgeo import ogr


#################################################################################################


## https://stackoverflow.com/questions/55522395/how-do-i-plot-shapely-polygons-and-objects-using-matplotlib
    
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

# Plots a Polygon to pyplot `ax`


#################################################################################################


def get_closest_triangle_test():
    
    print('\nget_closest_triangle_test():\n')
        
    
    print('\nSimple point [[0, 0], [1, 0], [1, 1]]:')
    
    # test point
    point = [0.5, 0.4]

    print(hf.get_closest_triangle(point, 0, 0, 1))
    
    
    print('\nPoint on line between two vertices [[0, 0], [0, 1], [1, 1]]:')
    
    # test point
    point = [0.5, 0.5]

    print(hf.get_closest_triangle(point, 0, 0, 1))
        
    
    print('\nPoint on rasterline between two vertizes [[0, 0], [1, 0], [1, 1]]:')
    
    # test point
    point = [0, 0.5]

    print(hf.get_closest_triangle(point, 0, 0, 1))
    
    
    print('\nPoint on vertex [[1, 1], [1, 2], [2, 2]]:')
    
    # test point
    point = [1, 1]

    print(hf.get_closest_triangle(point, 0, 0, 1))
    
    
    
    return 0
    
    

def get_interpolated_height_test():
    
    print('\nget_interpolated_height_test():\n')
    
    
    print('\nSimple plane - horizontally [0]:')
    # 3D height grid ; tl corner = 0, 0 ; scale = 1
    grid = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
    
    # test point
    point = [0.5, 0.5]

    print(hf.get_interpolated_height(point, grid, 0, 0, 1))
    
    
    print('\nComplex plane [0.5]:')
    # 3D height grid ; tl corner = 0, 0 ; scale = 1
    grid = np.array([[0, 1, 2], [1, 1, 2],[2, 2, 2]])
    
    # test point
    point = [0.5, 0.5]

    print(hf.get_interpolated_height(point, grid, 0, 0, 1))
    
    
    return 0
       
    
def intersection_line_triangle_test():

    print('\nintersection_line_triangle_test():\n')
    
    print('\nSimple plane - horizontal [0.5, 0.5, 1]:')
    
    # 3D triangle naive vertices
    triangle_vertices = np.array([[0, 0, 1], [1, 1, 1],[0, 1, 1]])
    
    print('Intersection: ', hf.intersection_line_triangle(np.array([0.5, 0.5, 0]), np.array([0.5, 0.5, 1]), triangle_vertices))
    
    
    
    print('\nSimple plane - vertical [0, 0.5, 0.5]:')
    
    # 3D triangle naive vertices
    triangle_vertices = np.array([[0, 0, 0], [0, 0.5, 1],[0, 1, 0]])
    
    print('Intersection: ', hf.intersection_line_triangle(np.array([-0.5, 0.5, 0.5]), np.array([0.5, 0.5, 0.5]), triangle_vertices))

    
    
    print('\nComplex [0.5, 0.5, 0.5]:')
    
    # 3D triangle naive vertices
    triangle_vertices = np.array([[0, 0, 0], [1, 1, 1],[0, 1, 1]])

    print('Intersection: ', hf.intersection_line_triangle(np.array([0.5, 0.5, 0]), np.array([0.5, 0.5, 1]), triangle_vertices))

    
    
    print('\nParallel [None]:')
    
    # 3D triangle naive vertices
    triangle_vertices = np.array([[0, 0, 0], [0, 0.5, 1],[0, 1, 0]])

    print('Intersection: ', hf.intersection_line_triangle(np.array([0.5, 0, 0]), np.array([0.5, 0, 1]), triangle_vertices))


    
    print('\nInside [None]:')
    
    # 3D triangle naive vertices
    triangle_vertices = np.array([[0, 0, 0], [0, 0.5, 1],[0, 1, 0]])

    print('Intersection: ', hf.intersection_line_triangle(np.array([0, 0, 0]), np.array([0, 0.5, 1]), triangle_vertices))


    
    print('\nPoint Touching [None]:')
    
    # 3D triangle naive vertices
    triangle_vertices = np.array([[0, 0, 0], [0, 0.5, 1],[0, 1, 0]])

    print('Intersection: ', hf.intersection_line_triangle(np.array([0, 0, 1]), np.array([0, 1, 1]), triangle_vertices))
    
    
    return 0
    
    
    
    
    # ogr - GEOS : only 2D operations (even with 3D objects.........) BUT no error, returns some fantasy values (mean of z between objects of some sort..)
###########################################################
def ogr_intersection_test():

    print('\nogr_intersection_test():\n')

    # https://pcjericks.github.io/py-gdalogr-cookbook/geometry.html#calculate-intersection-between-two-geometries
    
    wkt1 = "POLYGON ((1208064.271243039 624154.6783778917, 1208064.271243039 601260.9785661874, 1231345.9998651114 601260.9785661874, 1231345.9998651114 624154.6783778917, 1208064.271243039 624154.6783778917))"
    wkt2 = "POLYGON ((1199915.6662253144 633079.3410163528, 1199915.6662253144 614453.958118695, 1219317.1067437078 614453.958118695, 1219317.1067437078 633079.3410163528, 1199915.6662253144 633079.3410163528))"

    poly1 = ogr.CreateGeometryFromWkt(wkt1)
    poly2 = ogr.CreateGeometryFromWkt(wkt2)

    intersection = poly1.Intersection(poly2)


    print('Intersects: ', poly1.Intersects(poly2))
    print('Intersection: ', intersection.ExportToWkt())
    
    return 0
    
    
    
def polygon_line_intersection():
    
    print('\npolygon_line_intersection():\n')

    line1 = ogr.Geometry(ogr.wkbLineString)
    line1.AddPoint(0, -1)
    #line1.AddPoint(0, 0.5)#(0, 0) #passes Intersects and Crosses
    line1.AddPoint(0, 1)
    
    line2 = ogr.Geometry(ogr.wkbLineString)
    line2.AddPoint(1, 0)
    #line2.AddPoint(0.5, 0)#(0, 0) #passes Intersects and Crosses
    line2.AddPoint(1, 0)
    
    print('Intersects: ', line1.Intersects(line2))
    print('Contains: ', line1.Contains(line2))
    print('Crosses: ', line1.Crosses(line2))
    print('Intersection: ', line1.Intersection(line2))
    
    
    print('\nCurveGeometry\n')
    
    line1 = line1.GetCurveGeometry()
    line2 = line2.GetCurveGeometry()
    
    
    print('Intersects: ', line1.Intersects(line2))
    print('Contains: ', line1.Contains(line2))
    print('Crosses: ', line1.Crosses(line2))
    print('Intersection: ', line1.Intersection(line2))
    
    
    return 0
    
    
    
def polygon_linearring_intersection():

    print('\npolygon_linearring_intersection():\n')    
    
    ring1 = ogr.Geometry(ogr.wkbLinearRing)
    ring1.AddPoint(0, -1)
    ring1.AddPoint(0, 0)#(0, 0) #passes
    ring1.AddPoint(0, 1)
    
    ring2 = ogr.Geometry(ogr.wkbLinearRing)
    ring2.AddPoint(1, 0)
    ring2.AddPoint(0, 0)#(0, 0) #passes
    ring2.AddPoint(1, 0)
    
    print('Intersects: ', ring1.Intersects(ring2))
    print('Contains: ', ring1.Contains(ring2))
    print('Intersection: ', ring1.Intersection(ring2))
    
    return 0
###########################################################
    
    
def convex_distribution_test():
    
    print('\nconvex_distribution_test():\n')   
        
        
    print('\nSimple triangle (in xy) [0.3333333, 0.333333]:')
    
    points = np.array([[0, 0, 0], [0, 1, 0], [1, 0, 0]])

    convex_dist = hf.convex_distribution(points)
    
    print('Centroid: ', convex_dist['centroid'].xy)
 
    ax, fig = hf.convex_distribution_plot(convex_dist['distribution'], convex_dist['centroid'], points)
    
    
    print('\nSimple square (in xy) [0.5, 0.5]:')
    
    points = np.array([[0, 0, 0], [0, 1, 0], [1, 0, 0], [1, 1, 0]])

    convex_dist = hf.convex_distribution(points)
    
    print('Centroid: ', convex_dist['centroid'].xy)
 
    ax, fig = hf.convex_distribution_plot(convex_dist['distribution'], convex_dist['centroid'], points)
    
    
    print('\nSimple poylgon (in xy) [near 0.5, 0.5]:')
    
    points = np.array([[0, 0, 0], [0, 0.5, 0], [0.5, 0.75, 0], [1, 1, 0], [0.5, 0, 0], [0.75, 0.5, 0]])

    convex_dist = hf.convex_distribution(points)
    
    print('Centroid: ', convex_dist['centroid'].xy)
 
    ax, fig = hf.convex_distribution_plot(convex_dist['distribution'], convex_dist['centroid'], points)
    
    
    print('\nComplex poylgon [near 0.5, 0.5]:')
    
    points = np.array([[0, 0, 1], [0, 0.5, 100], [0.5, 0.75, -50], [1, 1, 4], [0.5, 0, 0], [0.75, 0.5, 0]])
    
    convex_dist = hf.convex_distribution(points)
    
    print('Centroid: ', convex_dist['centroid'].xy)
 
    ax, fig = hf.convex_distribution_plot(convex_dist['distribution'], convex_dist['centroid'], points)
    
    
    

    print('\nMultivariate normal (2D) distributed points [close to 0, 0]:')
    
    seed = 11235813213455
    rng = np.random.Generator(np.random.PCG64(seed))

    points = rng.multivariate_normal([0, 0], [[1, 0], [0, 1]], size=100)
    points = np.concatenate((points, np.zeros((points.shape[0], 1))), axis=1)
    
    convex_dist = hf.convex_distribution(points)
    
    print('Centroid: ', convex_dist['centroid'].xy)

    #print(points)
   
    ax, fig = hf.convex_distribution_plot(convex_dist['distribution'], convex_dist['centroid'], points, dimension=2)
        
        

    print('\nUniform random points [close to 0.5, 0.5]:')
    
    points = rng.random((113, 4))
    
    convex_dist = hf.convex_distribution(points)
    
    print('Centroid: ', convex_dist['centroid'].xy)

    #print(points)
   
    ax, fig = hf.convex_distribution_plot(convex_dist['distribution'], convex_dist['centroid'], points, dimension=3)
        
    plt.show()
            
            
    

    print('\nLine [0.5, 0.5]:')
    
    points = np.array([[0, 0, 0], [1, 1, 0]])

    print('Centroid: ', hf.convex_distribution(points)['centroid'])



    print('\nSingle Point [0, 0]:')
    
    points = np.array([[0, 0, 0]])

    convex_dist = hf.convex_distribution(points)

    print('Centroid: ', convex_dist['centroid'])
    

    return 0
    
    
    
    
def coordinates_calculate_normal_test():
    
    print('\n coordinates_calculate_normal_test():\n')   
        
        
    print('\nFlat surface [0, 0, 1]:')
    
    grid = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
    
    coordinate = [0.5, 0.5]

    normal = hf.coordinates_calculate_normal(grid, coordinate, 0, 0, 1)
    
    print('Normal: ', normal)
 
    
        
    print('\nFlat surface, point on vertex [0, 0, 1]:')
    
    grid = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
    
    coordinate = [1, 1]

    normal = hf.coordinates_calculate_normal(grid, coordinate, 0, 0, 1)
    
    print('Normal: ', normal)
    
            
            
    print('\nTilted surface increasing in column direction [0, a, a]:')
    
    grid = np.array([[0, 1, 2], [0, 1, 2], [0, 1, 2]])
    
    coordinate = [0.5, 0.5]

    normal = hf.coordinates_calculate_normal(grid, coordinate, 0, 0, 1)
    
    print('Normal: ', normal)
    
            
            
    print('\nTilted surface increasing in column direction, point on raster line [0, a, a]:')
    
    grid = np.array([[0, 1, 2], [0, 1, 2], [0, 1, 2]])
    
    coordinate = [0, 0.5]

    normal = hf.coordinates_calculate_normal(grid, coordinate, 0, 0, 1)
    
    print('Normal: ', normal)
    
    
    
    return 0
    
    
    
    
def composite_rotation_quaternions_test():
    
    print('\n composite_rotation_quaternions_test():\n')   
        
        
    print('\n0, x(pi) [0, 1, 0, 0]:')
    
    rot1 = [1, 0, 0, 0]
    rot2 = [0, 1, 0, 0]

    result = hf.composite_rotation_quaternions(rot1, rot2)
    
    print('Resulting quaternion: ', result)
 
    
        
    print('\ny(pi), x(pi) [0, 0, 0, 1]:')
    
    rot1 = [0, 0, 1, 0]
    rot2 = [0, 1, 0, 0]

    result = hf.composite_rotation_quaternions(rot1, rot2)
    
    print('Resulting quaternion: ', result)

        
    print('\n(1, 1, 1)(pi), z(pi) [-1, -1, 1, 0]:')
    
    rot1 = [0, 1, 1, 1]#/np.sqrt(3)
    rot2 = [0, 0, 0, 1]

    result = hf.composite_rotation_quaternions(rot1, rot2)
    
    print('Resulting quaternion: ', result)
    
    
    return 0

#######################################
 
    # Simple geometry
#get_closest_triangle_test() ## Passed
#intersection_line_triangle_test() ## Passed
#get_interpolated_height_test() ## Passed
    
    # Convex Distribution
convex_distribution_test() ## Passed

#coordinates_calculate_normal_test() ## Passed
#composite_rotation_quaternions_test() ## Passed










